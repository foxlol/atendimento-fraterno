Para construir:

client/npm run build
server/npm run webpack

Para o funcionamento da API é necessário um arquivo de configuração em configurations/app.json:

{
  "test": {
    "PORT": 3001,
    "MONGODB_URI": "mongodb://localhost:27017/AtendimentoFraternoTest",
    "JWT_SECRET": "hajasdnajsadhjajaj23ashdjahj23"
  },
  "development": {
    "PORT": 3001,
    "MONGODB_URI": "mongodb://localhost:27017/AtendimentoFraterno",
    "JWT_SECRET": "hajasdnajsadhjajaj23ashdjahj23"
  },
  "production": {
    "PORT": 3001,
    "MONGODB_URI": "mongodb://192.168.1.50:27017/AtendimentoFraterno",
    "JWT_SECRET": "hajasdnajsadhjajaj23ashdjahj23"
  }
}

Para rodar o projeto há configurações do PM2 (ecosystem.config.js) e Docker.