module.exports = {

  apps : [

    {
      name      : 'af-server',
      script    : './server/build/bundle.js',
      env: {
        NODE_ENV: 'production'
      }
    },

    {
      name      : 'af-client',
      script    : './client/server.js',
      env: {
        NODE_ENV: 'production'
      }
    }
  ]
};
