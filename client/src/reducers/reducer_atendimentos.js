import {
  CONSULTAR_ATENDIMENTOS
} from '../actions/atendimento';

const initialState = {
  resultado: []
};

export default function(state = initialState, action) {

  switch (action.type) {
    case CONSULTAR_ATENDIMENTOS:
      return Object.assign({}, state, { resultado: action.payload });
    default:
      return state;
  }
};
