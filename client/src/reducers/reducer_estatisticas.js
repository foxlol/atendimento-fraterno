import {
  ATUALIZAR_ESTATISTICAS
} from '../actions/estatisticas';

const initialState = {
  atendimentos: 0,
  tratamentosEspirituais: 0,
  tratamentosSaude: 0,
  tratamentosDistancia: 0,
  tratamentosDistanciaSaude: 0,
};

export default function(state = initialState, action) {

  switch (action.type) {
    case ATUALIZAR_ESTATISTICAS:
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
};
