import {
  LOGIN, LOGOUT
} from '../actions/autenticacao';

const initialState = {
  usuario: undefined,
  isAutenticado: false,
  isAtendente: false,
  roles: []
};

const loadInitialState = () => {
  const userLoginData = localStorage.getItem('user-login-data');
  let parsedUserLoginData;

  if (typeof userLoginData === "string") {
    parsedUserLoginData = JSON.parse(userLoginData);
  }

  return parsedUserLoginData ? parsedUserLoginData : initialState;
}

export default function(state = loadInitialState(), action) {
  switch (action.type) {
    case LOGIN:
      return Object.assign({}, state, action.payload);
    case LOGOUT:
      return Object.assign({}, state, action.payload);
    default:
      return state;
  }
};
