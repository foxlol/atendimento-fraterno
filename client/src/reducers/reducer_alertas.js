import {
  ALERTAR, ALERTAR_ERRO, ALERTAR_SUCESSO,
  REMOVER_TODOS, REMOVER_ERRO, REMOVER_SUCESSO, REMOVER_ALERTA
} from '../actions/alerta';

const initialState = {
  erro: undefined,
  sucesso: undefined,
  alerta: undefined
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ALERTAR_ERRO:
      return Object.assign({}, state, {
        erro: action.mensagem,
      });
    case ALERTAR_SUCESSO:
      return Object.assign({}, state, {
        sucesso: action.mensagem,
      });
    case ALERTAR:
      return Object.assign({}, state, {
        alerta: action.mensagem,
      });
    case REMOVER_TODOS:
      return Object.assign({}, state, initialState);
    case REMOVER_ERRO:
      return Object.assign({}, state, {
        erro: undefined,
      });
    case REMOVER_SUCESSO:
      return Object.assign({}, state, {
        sucesso: undefined,
      });
    case REMOVER_ALERTA:
      return Object.assign({}, state, {
        alerta: undefined,
      });
    default:
      return state;
  }
};
