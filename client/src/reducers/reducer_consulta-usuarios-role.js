import _ from "lodash";

import {
  CONSULTAR_USUARIOS_POR_ROLE,
} from "../actions/usuario";

const initialState = {
  usuarios: [],
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CONSULTAR_USUARIOS_POR_ROLE:
      let usuarios = action.payload ? action.payload.usuarios : [];
      return Object.assign({}, state, { usuarios });
    default:
      return state;
  }
}
