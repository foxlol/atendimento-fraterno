import { combineReducers } from 'redux';

import AutenticacaoReducer from './reducer_autenticacao';
import AlertasReducer from './reducer_alertas';
import ConsultaUsuariosReducer from './reducer_consulta-usuarios';
import ConsultaUsuariosPorRoleReducer from './reducer_consulta-usuarios-role';
import PesquisaReducer from './reducer_pesquisa';
import AtendimentosReducer from './reducer_atendimentos';
import TratamentosReducer from './reducer_tratamentos';
import TratamentosProcessoReducer from './reducer_tratamentos-processo';
import EstatisticasReducer from './reducer_estatisticas';
import LoaderReducer from './reducer_loader';

const rootReducer = combineReducers({
  autenticacao: AutenticacaoReducer,
  alerta: AlertasReducer,
  consultaUsuarios: ConsultaUsuariosReducer,
  usuariosPorRole: ConsultaUsuariosPorRoleReducer,
  pesquisa: PesquisaReducer,
  atendimentos: AtendimentosReducer,
  tratamentos: TratamentosReducer,
  tratamentosProcesso: TratamentosProcessoReducer,
  estatisticas: EstatisticasReducer,
  loader: LoaderReducer
});

export default rootReducer;
