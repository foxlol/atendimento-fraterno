import {
  CONSULTAR_TRATAMENTOS_PROCESSO
} from '../actions/tratamento';

const initialState = {
  listagem: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CONSULTAR_TRATAMENTOS_PROCESSO:
      const listagem = state.listagem;
      listagem[action.payload.tipo] = action.payload.listagem;
      return Object.assign({}, state, { listagem });
    default:
      return state;
  }
};
