import _ from "lodash";

import {
  CONSULTAR_USUARIOS_POR_NOME,
  CARREGAR_DADOS_ADICIONAIS_USUARIO,
  ATUALIZAR_DADOS_USUARIO,
  NOVO_USUARIO,
  CADASTRAR_USUARIO,
  SELECIONAR_USUARIO,
  CONSULTAR_USUARIO_POR_ID,
  EXCLUIR_USUARIO
} from "../actions/usuario";

const initialState = {
  selecionado: undefined,
  dadosAdicionais: undefined,
  resultado: [],
  resultadoParaExibicao: [],
  nomeNovoUsuario: undefined
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CONSULTAR_USUARIOS_POR_NOME:
      let resultadoParaExibicao;
      let resultado = action.payload ? action.payload.usuarios : [];

      if (action.payload) {
        resultadoParaExibicao = _.map(resultado, usuario => {
          return {
            key: usuario._id,
            title: usuario.nome
          };
        });
      }

      return Object.assign({}, state, { resultado, resultadoParaExibicao });
    case SELECIONAR_USUARIO:
      return Object.assign({}, state, {
        selecionado: _.find(state.resultado, usuario => {
          return usuario._id === action.idSelecionado;
        })
      });
    case CONSULTAR_USUARIO_POR_ID:
      return Object.assign({}, state, { selecionado: action.payload.usuario });
    case CARREGAR_DADOS_ADICIONAIS_USUARIO:
      return Object.assign({}, state, {
        dadosAdicionais: action.payload.dadosAdicionais
      });
    case ATUALIZAR_DADOS_USUARIO:
      return Object.assign({}, state, {
        selecionado: action.payload.selecionado
      });
    case CADASTRAR_USUARIO:
      return Object.assign({}, state, {
        selecionado: action.payload.selecionado
      });
    case NOVO_USUARIO:
      return Object.assign({}, state, {
        ...initialState,
        nomeNovoUsuario: action.payload.nome
      });
    case EXCLUIR_USUARIO:
      return Object.assign({}, state, initialState);
    default:
      return state;
  }
}
