import _ from 'lodash';

import {
  PESQUISA
} from '../actions/pesquisa';

const initialState = {
  inputs: {}
};

export default function(state = initialState, action) {

  switch (action.type) {
    case PESQUISA:
      return Object.assign({}, state, {
        inputs: _.set(state.inputs, action.payload.inputName, {
          results: action.payload.results
        })
      });
    default:
      return state;
  }
};
