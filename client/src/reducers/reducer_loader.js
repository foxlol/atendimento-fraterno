import {
  STARTED_LOADING, FINISHED_LOADING
} from '../actions/loader';

const initialState = {
  isLoading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case STARTED_LOADING:
      return Object.assign({}, state, {
        isLoading: true
      });
    case FINISHED_LOADING:
      return Object.assign({}, state, {
        isLoading: false
      });
    default:
      return state;
  }
};
