import {
  REQUISITAR_ACAO, ENCERRAR_REQUISICAO_ACAO, CONSULTAR_TODOS_TRATAMENTOS,
  CONSULTAR_TRATAMENTOS, CONSULTAR_TRATAMENTOS_PREVISTOS_DIA, CONSULTAR_CONTAGEM_TRATAMENTOS_PREVISTOS_DIA
} from '../actions/tratamento';

const initialState = {
  mensagem: undefined,
  id: undefined,
  listagem: [],
  previstos: undefined,
  agendamentosNecessarios: undefined
};

export default function(state = initialState, action) {
  switch (action.type) {
    case REQUISITAR_ACAO:
      return Object.assign({}, state, {
        mensagem: action.mensagem,
        id: action.idTratamento,
        agendamentosNecessarios: action.data
      });
    case ENCERRAR_REQUISICAO_ACAO:
      return Object.assign({}, state, {
        mensagem: undefined,
        id: undefined,
        agendamentosNecessarios: undefined
      });
    case CONSULTAR_TRATAMENTOS:
      return Object.assign({}, state, { listagem: action.payload });
    case CONSULTAR_TODOS_TRATAMENTOS:
      return Object.assign({}, state, { listagem: action.payload });
    case CONSULTAR_CONTAGEM_TRATAMENTOS_PREVISTOS_DIA:
      return Object.assign({}, state, { contagemPrevistos: action.payload });
    case CONSULTAR_TRATAMENTOS_PREVISTOS_DIA:
      return Object.assign({}, state, { previstos: action.payload });
    default:
      return state;
  }
};
