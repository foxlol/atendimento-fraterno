import axios from 'axios';
import { alertarErro } from '../actions/alerta';
import { startLoading, finishLoading } from '../actions/loader';

function callApi(method, endpoint, data, authenticated) {

  let config = {
    url: `${process.env.REACT_APP_API_URL}${endpoint}`,
    method,
    data
  };

  if (authenticated) {
    let token = localStorage.getItem('auth-token');
    config.headers = {'x-auth': token};
  }

  return axios(config).then((response) => {
    return response;
  });
}

function handleError(error) {
  let mensagem =
    error.response ?
      error.response.data.error ?
        error.response.data.error.message ? error.response.data.error.message :
        error.response.data.error : `Erro inesperado: ${error.response.statusText}`
        : `Erro inesperado: ${error.message}` || 'Erro inesperado';

  let statusCode =
    error ?
      error.status ? error.status : error.response
        ? error.response.status : null : null;

  if (statusCode === 401) {
    mensagem = 'Acesso não autorizado';
  }

  return alertarErro(mensagem);
}

export const CALL_API = Symbol('Call API');

export default store => next => action => {

  const callAPIAction = action[CALL_API];

  if (callAPIAction === undefined) {
    return next(action);
  }

  let {
    endpoint, type, method, data,
    authenticated, callback,
    handleErrorOnCallback = false,
    onFinish, showLoader
  } = callAPIAction;

  if (showLoader) {
    store.dispatch(startLoading());
  }

  return callApi(method, endpoint, data, authenticated).then((response) => {
    let payload = callback(response, store.dispatch);
    if (type) {
      next({ type, payload });
    }
  }, (error) => {
    if (handleErrorOnCallback) {
      callback(error.response, store.dispatch);
    } else {
      next(handleError(error));
    }
  }).then(() => {
    if (onFinish) {
      onFinish();
    }
    
    if (showLoader) {
      store.dispatch(finishLoading());
    }
  });
};
