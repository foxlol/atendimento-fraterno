import { CALL_API } from "../middlewares/api";

import { alertarErro, alertarSucesso } from "../actions/alerta";

export const CONSULTAR_USUARIOS_POR_NOME = "consultar-usuarios-por-nome";
export const CONSULTAR_USUARIOS_POR_ROLE = "consultar-usuarios-por-role";
export const CONSULTAR_USUARIO_POR_ID = "consultar-usuario-por-id";
export const CARREGAR_DADOS_ADICIONAIS_USUARIO =
  "carregar-dados-adicionais-usuario";
export const ATUALIZAR_DADOS_USUARIO = "atualizar-dados-usuario";
export const NOVO_USUARIO = "novo-usuario";
export const CADASTRAR_USUARIO = "cadastrar-usuario";
export const SELECIONAR_USUARIO = "selecionar-usuario";
export const EXCLUIR_USUARIO = "excluir-usuario";

export function consultarUsuarios(nome, callback) {
  return {
    [CALL_API]: {
      type: CONSULTAR_USUARIOS_POR_NOME,
      method: "GET",
      endpoint: `/usuarios/?nome=${nome}`,
      authenticated: true,
      handleErrorOnCallback: true,
      callback: (response, dispatch) => {
        if (callback) {
          callback();
        }

        if (response.status !== 200) {
          if (response.status === 404) {
            return dispatch({
              type: CONSULTAR_USUARIOS_POR_NOME,
              usuarios: []
            });
          } else if (response.status === 401) {
            return dispatch(alertarErro("Acesso não autorizado"));
          } else {
            return dispatch(
              alertarErro("Erro inesperado ao pesquisar pacientes")
            );
          }
        }

        return {
          usuarios: response.data.usuarios
        };
      }
    }
  };
}

export function consultarUsuariosPorRole(role, callback) {
  return {
    [CALL_API]: {
      type: CONSULTAR_USUARIOS_POR_ROLE,
      method: "GET",
      endpoint: `/usuarios/?role=${role}`,
      authenticated: true,
      handleErrorOnCallback: true,
      callback: (response, dispatch) => {
        if (callback) {
          callback();
        }

        if (response.status !== 200) {
          if (response.status === 404) {
            return dispatch({
              type: CONSULTAR_USUARIOS_POR_ROLE,
              usuarios: []
            });
          } else if (response.status === 401) {
            return dispatch(alertarErro("Acesso não autorizado"));
          } else {
            return dispatch(
              alertarErro("Erro inesperado ao pesquisar usuários")
            );
          }
        }

        return {
          usuarios: response.data.usuarios
        };
      }
    }
  };
}

export function consultarUsuario(id, callback) {
  return {
    [CALL_API]: {
      type: CONSULTAR_USUARIO_POR_ID,
      method: "GET",
      endpoint: `/usuarios/${id}`,
      authenticated: true,
      handleErrorOnCallback: false,
      showLoader: true,
      callback: (response, dispatch) => {
        dispatch(carregarDadosAdicionais(id));
        callback();
        return {
          usuario: response.data.usuario
        };
      }
    }
  };
}

export function carregarDadosAdicionais(id) {
  return {
    [CALL_API]: {
      type: CARREGAR_DADOS_ADICIONAIS_USUARIO,
      method: "GET",
      endpoint: `/usuarios/${id}/dados-adicionais`,
      authenticated: true,
      handleErrorOnCallback: false,
      callback: (response, dispatch) => {
        return {
          idSelecionado: id,
          dadosAdicionais: response.data.dadosAdicionais
        };
      }
    }
  };
}

export function selecionarUsuario(idSelecionado) {
  return {
    type: SELECIONAR_USUARIO,
    idSelecionado
  };
}

export function novoUsuario(callback, nome) {
  callback();

  return {
    type: NOVO_USUARIO,
    payload: {
      nome
    }
  };
}

export function cadastrarUsuario(dados) {
  return {
    [CALL_API]: {
      type: CADASTRAR_USUARIO,
      method: "POST",
      endpoint: `/usuarios`,
      data: dados,
      authenticated: false,
      handleErrorOnCallback: false,
      showLoader: true,
      callback: (response, dispatch) => {
        dispatch(alertarSucesso("Usuário cadastrado com sucesso"));
        return {
          selecionado: response.data.usuario
        };
      }
    }
  };
}

export function atualizarDadosUsuario(dados) {
  return {
    [CALL_API]: {
      type: ATUALIZAR_DADOS_USUARIO,
      method: "PATCH",
      endpoint: `/usuarios/${dados._id}`,
      data: dados,
      authenticated: true,
      handleErrorOnCallback: false,
      showLoader: true,
      callback: (response, dispatch) => {
        dispatch(alertarSucesso("Dados do usuário atualizados com sucesso"));
        return {
          selecionado: response.data.usuario
        };
      }
    }
  };
}

export function removerUsuario(dados, callback) {
  return {
    [CALL_API]: {
      type: EXCLUIR_USUARIO,
      method: "DELETE",
      endpoint: `/usuarios/${dados._id}`,
      authenticated: true,
      handleErrorOnCallback: false,
      showLoader: true,
      onFinish: callback,
      callback: (response, dispatch) => {
        dispatch(alertarSucesso("Usuário excluído com sucesso"));
      }
    }
  };
}
