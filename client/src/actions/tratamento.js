import { CALL_API } from '../middlewares/api';
import { alertarSucesso, alertarErro } from '../actions/alerta';
import { carregarDadosAdicionais } from '../actions/usuario';

export const REQUISITAR_ACAO = 'requisitar-acao';
export const ENCERRAR_REQUISICAO_ACAO = 'encerrar-requisicao-acao';
export const CONSULTAR_TRATAMENTOS = 'consultar-tratamentos';
export const CONSULTAR_TODOS_TRATAMENTOS = 'consultar-todos-tratamentos';
export const CONSULTAR_CONTAGEM_TRATAMENTOS_PREVISTOS_DIA = 'consultar-contagem-tratamentos-previstos-dia';
export const CONSULTAR_TRATAMENTOS_PREVISTOS_DIA = 'consultar-tratamentos-previstos-dia';
export const CONSULTAR_TRATAMENTOS_PROCESSO = 'consultar-tratamentos-processo';

export function cadastrarTratamento(
  idUsuario, tipo, datasAgendamentos, idAtendente, motivo, intercessor, callback) {
    return {
      [CALL_API]: {
        method: 'POST',
        endpoint: `/tratamentos/`,
        data: {idUsuario, tipo, datasAgendamentos, idAtendente, motivo, intercessor},
        authenticated: true,
        handleErrorOnCallback: false,
        onFinish: callback,
        showLoader: true,
        callback: (response, dispatch) => {
          dispatch(carregarDadosAdicionais(idUsuario));
          return dispatch(alertarSucesso('Tratamento cadastrado com sucesso'));
        }
      }
    }
}

export function checkinTratamento(id, callback) {
  return {
    [CALL_API]: {
      method: 'PATCH',
      endpoint: `/tratamentos/${id}/checkin`,
      authenticated: true,
      handleErrorOnCallback: true,
      showLoader: true,
      onFinish: callback,
      callback: (response, dispatch) => {
        const { status, data } = response;

        if (status === 200) {
          dispatch(carregarDadosAdicionais(data.tratamento.paciente));
          dispatch(consultarTratamentosPrevistosDia());
          return dispatch(alertarSucesso('Check-in efetuado com sucesso'));
        } else if (status === 403 && data.requireAction) {
          return dispatch(requisitarAcao(id, data.error, data.agendamentosNecessarios));
        } else if (status === 401) {
          return dispatch(alertarErro('Acesso não autorizado'));
        } else {
          return dispatch(alertarErro(data.error));
        }
      }
    }
  }
};

export function checkoutTratamento(id, callback) {
  return {
    [CALL_API]: {
      method: 'PATCH',
      endpoint: `/tratamentos/${id}/checkout`,
      authenticated: true,
      handleErrorOnCallback: true,
      onFinish: callback,
      showLoader: true,
      callback: (response, dispatch) => {
        const { status, data } = response;

        if (status === 200) {
          dispatch(carregarDadosAdicionais(data.tratamento.paciente));
          return dispatch(alertarSucesso('Check-out efetuado com sucesso'));
        } else if (status === 403 && data.requireAction) {
          return dispatch(requisitarAcao(id, data.error));
        } else if (status === 401) {
          return dispatch(alertarErro('Acesso não autorizado'));
        } else {
          return dispatch(alertarErro(data.error));
        }
      }
    }
  }
};

export function encerrarTratamento(id, callback) {
  return {
    [CALL_API]: {
      method: 'PATCH',
      endpoint: `/tratamentos/${id}/encerrar`,
      authenticated: true,
      handleErrorOnCallback: false,
      onFinish: callback,
      showLoader: true,
      callback: (response, dispatch) => {
        const { data } = response;
        dispatch(carregarDadosAdicionais(data.tratamento.paciente));
        return dispatch(alertarSucesso('Tratamento encerrado com sucesso'));
      }
    }
  }
};

export function estenderTratamento(id, semanas, callback) {
  return {
    [CALL_API]: {
      method: 'PATCH',
      endpoint: `/tratamentos/${id}/estender/semanas/${semanas}`,
      authenticated: true,
      handleErrorOnCallback: false,
      onFinish: callback,
      showLoader: true,
      callback: (response, dispatch) => {
        const { data } = response;
        dispatch(consultarTratamentosPrevistosDia());
        dispatch(carregarDadosAdicionais(data.tratamento.paciente));
        return dispatch(alertarSucesso('Tratamento estendido com sucesso'));
      }
    }
  }
};

export function adicionarObservacaoMediunica(id, observacaoMediunica, callback) {
  return {
    [CALL_API]: {
      method: 'PATCH',
      endpoint: `/tratamentos/${id}/observacao-mediunica`,
      data: { observacaoMediunica },
      authenticated: true,
      handleErrorOnCallback: false,
      onFinish: callback,
      showLoader: true,
      callback: (response, dispatch) => {
        const { data } = response;
        dispatch(carregarDadosAdicionais(data.tratamento.paciente));
        return dispatch(alertarSucesso('Observação mediúnica cadastrada com sucesso'));
      }
    }
  }
};

export function consultarTratamentosVerificados(tipo, callback) {
  return {
    [CALL_API]: {
      type: CONSULTAR_TRATAMENTOS,
      method: 'GET',
      endpoint: `/tratamentos/${tipo}/verificados`,
      authenticated: true,
      handleErrorOnCallback: false,
      onFinish: callback,
      callback: (response, dispatch) => {
        return response.data.tratamentos;
      }
    }
  }
};

export function consultarTratamentosVerificadosProcesso(tipo, callback) {
  return {
    [CALL_API]: {
      type: CONSULTAR_TRATAMENTOS_PROCESSO,
      method: 'GET',
      endpoint: `/tratamentos/${tipo}/verificados`,
      authenticated: true,
      handleErrorOnCallback: false,
      onFinish: callback,
      callback: (response, dispatch) => {
        return {
          tipo,
          listagem: response.data.tratamentos
        };
      }
    }
  }
};

export function consultarContagemTratamentosPrevistosDia(callback) {
  return {
    [CALL_API]: {
      type: CONSULTAR_CONTAGEM_TRATAMENTOS_PREVISTOS_DIA,
      method: 'GET',
      endpoint: `/tratamentos/contagem/previstos/dia`,
      authenticated: true,
      handleErrorOnCallback: false,
      onFinish: callback,
      callback: (response, dispatch) => {
        return response.data.previstos;
      }
    }
  }
};

export function consultarTratamentosPrevistosDia(callback) {
  return {
    [CALL_API]: {
      type: CONSULTAR_TRATAMENTOS_PREVISTOS_DIA,
      method: 'GET',
      endpoint: `/tratamentos/previstos/dia`,
      authenticated: true,
      handleErrorOnCallback: false,
      onFinish: callback,
      callback: (response, dispatch) => {
        return response.data;
      }
    }
  }
};

export function consultarTodosTratamentos(filtro, callback) {
  return {
    [CALL_API]: {
      type: CONSULTAR_TODOS_TRATAMENTOS,
      method: 'POST',
      endpoint: `/tratamentos/todos`,
      data: filtro,
      authenticated: true,
      handleErrorOnCallback: false,
      showLoader: true,
      onFinish: callback,
      callback: (response, dispatch) => {
        return response.data.tratamentos;
      }
    }
  }
};

export function requisitarAcao(idTratamento, mensagem, data) {
  return { type: REQUISITAR_ACAO, mensagem, idTratamento, data };
}

export function encerrarRequisicaoAcao() {
  return { type: ENCERRAR_REQUISICAO_ACAO };
}

export function converterTratamentoParaDistancia(id, callback) {
    return {
      [CALL_API]: {
        method: 'PATCH',
        endpoint: `/tratamentos/${id}/converter-distancia`,
        authenticated: true,
        handleErrorOnCallback: false,
        onFinish: callback,
        showLoader: true,
        callback: (response, dispatch) => {
          const { data } = response;
          dispatch(carregarDadosAdicionais(data.tratamento.paciente));
          return dispatch(alertarSucesso('Tratamento convertido com sucesso'));
        }
      }
    }
};

export function converterTratamentoParaPresencial(id, callback) {
    return {
      [CALL_API]: {
        method: 'PATCH',
        endpoint: `/tratamentos/${id}/converter-presencial`,
        authenticated: true,
        handleErrorOnCallback: false,
        onFinish: callback,
        showLoader: true,
        callback: (response, dispatch) => {
          const { data } = response;
          dispatch(carregarDadosAdicionais(data.tratamento.paciente));
          return dispatch(alertarSucesso('Tratamento convertido com sucesso'));
        }
      }
    }
};
