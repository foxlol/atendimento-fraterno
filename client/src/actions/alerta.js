export const ALERTAR = 'alertar';
export const ALERTAR_ERRO = 'alertar_erro';
export const ALERTAR_SUCESSO = 'alertar_sucesso';
export const REMOVER_TODOS = 'remover_todos';
export const REMOVER_ERRO = 'remover_erro';
export const REMOVER_SUCESSO = 'remover_sucesso';
export const REMOVER_ALERTA = 'remover_alerta';

export function alertarErro(mensagem) {
  return  {
    type: ALERTAR_ERRO,
    mensagem
  };
}

export function alertarSucesso(mensagem) {
  return  {
    type: ALERTAR_SUCESSO,
    mensagem
  };
}

export function alertar(mensagem) {
  return  {
    type: ALERTAR,
    mensagem
  };
}

export function removerTodosAlertas() {
  return  {
    type: REMOVER_TODOS
  };
}

export function removerErro() {
  return  {
    type: REMOVER_ERRO
  };
}

export function removerSucesso() {
  return  {
    type: REMOVER_SUCESSO
  };
}

export function removerAlerta() {
  return  {
    type: REMOVER_ALERTA
  };
}
