export const STARTED_LOADING = 'started_loading';
export const FINISHED_LOADING = 'finished_loading';

export function startLoading() {
  return  {
    type: STARTED_LOADING
  };
}

export function finishLoading() {
  return  {
    type: FINISHED_LOADING
  };
}
