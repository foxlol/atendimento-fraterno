import _ from 'lodash';
import jwtDecode from 'jwt-decode';

import { CALL_API } from '../middlewares/api';
import { alertarSucesso } from '../actions/alerta';

export const LOGIN = 'login';
export const LOGOUT = 'logout';

export function logarUsuario(credenciais) {
  return {
    [CALL_API]: {
      type: LOGIN,
      method: 'POST',
      endpoint: '/usuarios/login',
      data: credenciais,
      authenticated: false,
      showLoader: true,
      callback: (response, dispatch) => {
        const token = response.headers['x-auth'];
        const decoded = jwtDecode(token);

        const userLoginData = {
          usuario: response.data.usuario,
          isAutenticado: true,
          isAtendente: isAtendente(decoded.roles),
          roles: decoded.roles
        };

        localStorage.setItem('user-login-data', JSON.stringify(userLoginData));
        localStorage.setItem('auth-token', token);

        dispatch(alertarSucesso('Login efetuado com sucesso'));

        return userLoginData;
      }
    }
  }
};

export function deslogarUsuario(callback) {
  return {
    [CALL_API]: {
      type: LOGOUT,
      method: 'DELETE',
      endpoint: '/usuarios/token',
      authenticated: true,
      showLoader: true,
      handleErrorOnCallback: true,
      onFinish: callback,
      callback: (response, dispatch) => {
        localStorage.removeItem('auth-token');
        localStorage.removeItem('user-login-data');
        
        return {
          usuario: undefined,
          isAutenticado: false,
          roles: []
        }
      }
    }
  }
};

function isAtendente(roles) {
  return _.includes(roles, 'ATENDIMENTO');
}
