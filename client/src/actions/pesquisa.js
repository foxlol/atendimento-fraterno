import _ from 'lodash';

import { CALL_API } from '../middlewares/api';

import { alertarErro } from '../actions/alerta';

export const PESQUISA = 'pesquisa';

export function pesquisarCidades(nome, inputName, callback) {
  return {
    [CALL_API]: {
      type: PESQUISA,
      method: 'GET',
      endpoint: `/localidades/cidades?nome=${nome}`,
      authenticated: false,
      handleErrorOnCallback: false,
      callback: (response, dispatch) => {
        callback();

        const results = _.map(response.data.cidades, (cidade) => {
          return {
            key: `${cidade.nome} - ${cidade.estado}`,
            title: `${cidade.nome} - ${cidade.estado}`
          };
        });

        return {
          inputName,
          results
        }
      }
    }
  }
};

export function pesquisarUsuarios(nome, inputName, callback) {
  return {
    [CALL_API]: {
      type: PESQUISA,
      method: 'GET',
      endpoint: `/usuarios/?nome=${nome}`,
      authenticated: true,
      handleErrorOnCallback: true,
      callback: (response, dispatch) => {
        callback();

        if (response.status !== 200) {
          if (response.status === 404) {
            return {
              inputName,
              results: []
            }
          } else if (response.status === 401) {
            return dispatch(alertarErro('Acesso não autorizado'));
          } else {
            return dispatch(alertarErro('Erro inesperado ao pesquisar pacientes'));
          }
        }

        const results = _.map(response.data.usuarios, (usuario) => {
          return {
            key: `${usuario._id}`,
            title: `${usuario.nome}`
          };
        });

        return {
          inputName,
          results
        }
      }
    }
  }
};
