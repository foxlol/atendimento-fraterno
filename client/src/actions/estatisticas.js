import { CALL_API } from '../middlewares/api';

export const ATUALIZAR_ESTATISTICAS = 'atualizar-estatisticas';

export function atualizarEstatisticas() {
  return {
    [CALL_API]: {
      method: 'GET',
      type: ATUALIZAR_ESTATISTICAS,
      endpoint: `/estatisticas`,
      authenticated: true,
      handleErrorOnCallback: false,
      callback: (response, dispatch) => {
        return response.data.estatisticas;
      }
    }
  }
};
