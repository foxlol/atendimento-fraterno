import { CALL_API } from '../middlewares/api';
import { alertarSucesso } from '../actions/alerta';
import { carregarDadosAdicionais } from '../actions/usuario';
import { consultarTratamentosPrevistosDia } from '../actions/tratamento';

export const CONSULTAR_ATENDIMENTOS = 'consultar-atendimentos';

export function agendarAtendimento(idUsuario, isRetorno, atendentePreferencial, callback) {
  return {
    [CALL_API]: {
      method: 'POST',
      endpoint: `/atendimentos`,
      data: {idUsuario, isRetorno, atendentePreferencial},
      authenticated: true,
      handleErrorOnCallback: false,
      onFinish: callback,
      callback: (response, dispatch) => {
        dispatch(carregarDadosAdicionais(idUsuario));
        dispatch(consultarTratamentosPrevistosDia());
        dispatch(alertarSucesso('Atendimento agendado com sucesso'));
      }
    }
  }
};

export function consultarAtendimentos(callback) {
  return {
    [CALL_API]: {
      type: CONSULTAR_ATENDIMENTOS,
      method: 'GET',
      endpoint: `/atendimentos`,
      authenticated: true,
      handleErrorOnCallback: false,
      onFinish: callback,
      callback: (response, dispatch) => {
        return response.data.atendimentos;
      }
    }
  }
};

export function removerAtendimento(id, callback) {
  return {
    [CALL_API]: {
      method: 'DELETE',
      endpoint: `/atendimentos/${id}`,
      authenticated: true,
      handleErrorOnCallback: false,
      onFinish: callback,
      callback: (response, dispatch) => {
        dispatch(consultarAtendimentos());
        dispatch(alertarSucesso('Atendimento concluído com sucesso'));
      }
    }
  }
};

export function iniciarAtendimento(idAtendimento, idAtendente, callback) {
  return {
    [CALL_API]: {
      method: 'PATCH',
      endpoint: `/atendimentos/${idAtendimento}/iniciar`,
      data: {idAtendente},
      authenticated: true,
      handleErrorOnCallback: false,
      callback: (response, dispatch) => {
        callback();
        dispatch(alertarSucesso('Atendimento iniciado com sucesso'));
      }
    }
  }
};
