import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Search } from 'semantic-ui-react';

class SearchInput extends Component {

  componentWillMount() {
    this.setState({isLoading: false, value: this.props.initialValue});
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.initialValue === nextProps.initialValue) {
      return;
    }

    this.setState({value: nextProps.initialValue});
  }

  resetComponent() {
    this.setState({ isLoading: false, value: undefined });
    this.props.onSelect(undefined);
  }

  handleResultSelect = (e, { result }) => {
    this.setState({value: result.title});
    this.props.onSelect(result);
  }

  handleSearchChange = (e, { value }) => {
    this.setState({ value });
    if (value.length < 1) return this.resetComponent();
    this.search(value);
  }

  search = _.debounce((value) => {
    this.setState({ isLoading: true });

    this.props.searchAction(value, this.props.name, () => {
      this.setState({ isLoading: false });
    });
  }, 500);

  render() {
    const { isLoading, value } = this.state;
    const { inputs } = this.props.pesquisa;
    const input = _.get(inputs, this.props.name);
    const results = input ? input.results : [];

    return (
      <Search
        loading={isLoading}
        onResultSelect={this.handleResultSelect}
        onSearchChange={this.handleSearchChange}
        placeholder={this.props.placeholder}
        results={results}
        value={value}
        noResultsMessage="Nenhum resultado" />
    );
  }
}

function mapStateToProps(state) {
  return { pesquisa: state.pesquisa };
}

export default connect(mapStateToProps)(SearchInput);
