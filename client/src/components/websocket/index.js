import React, { Component } from 'react';
import { compose } from "redux";
import { connect } from 'react-redux';
import io from 'socket.io-client';
import { alertarErro } from '../../actions/alerta';

const withWSConnection = ConnectedComponent => 

  class extends Component {

    state = {
      estatisticas: {}
    };

    componentDidMount() {
      this.configureSocket();
    }

    componentWillUnmount() {
      if (this.socket) {
        this.socket.disconnect();
      }
    }

    configureSocket = () => {
      if (this.socket) {
        this.socket.disconnect();
      }

      this.socket = io(
        `${process.env.REACT_APP_API_WS_URL}/estatisticas`, {
          query: { auth_token: localStorage.getItem('auth-token') },
          'force new connection': true
      });

      this.socket.on('exception', (exception) => {
        this.props.alertarErro(exception.message);
      });

      this.socket.on('connect_error', (error) => {
        this.props.alertarErro('Erro ao se conectar ao servidor', error);
      });   
      
      this.socket.on('sincronizarEstatisticas', estatisticas => {
        this.setState({ estatisticas });
      });
    }

    render() {
      return <ConnectedComponent {...this.props} socket={this.socket} estatisticas={this.state.estatisticas} />;
    }
  };

const mapStateToProps = state => {
  return {};
};

const composedHoc = compose(
  connect(mapStateToProps, { alertarErro }),
  withWSConnection
);

export default composedHoc;