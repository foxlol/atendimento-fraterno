import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Search } from "semantic-ui-react";

import Security from "../security/component";

import {
  consultarUsuarios,
  selecionarUsuario,
  carregarDadosAdicionais,
  novoUsuario
} from "../../actions/usuario";

class ConsultaUsuario extends Component {
  componentWillMount() {
    this.resetComponent();
  }

  resetComponent = () => {
    this.setState({ isLoading: false, nome: "" });
  };

  handleResultSelect = (e, { result }) => {
    this.setState({ nome: result.title });
    this.props.selecionarUsuario(result.key);
    this.props.carregarDadosAdicionais(result.key);
    this.context.router.history.push("/usuarios/cadastro");
    this.resetComponent();
  };

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, nome: value });
    this.consultarUsuarios(value);
  };

  adicionarNovoUsuario = nome => {
    this.props.novoUsuario(() => {
      this.resetComponent();
      this.context.router.history.push("/usuarios/cadastro");
    }, nome);
  };

  consultarUsuarios = _.debounce(nome => {
    this.props.consultarUsuarios(nome, () => {
      this.setState({
        isLoading: false
      });
    });
  }, 500);

  render() {
    const { isLoading, nome } = this.state;

    return (
      <Search
        fluid
        loading={isLoading}
        onResultSelect={this.handleResultSelect}
        onSearchChange={this.handleSearchChange}
        results={this.props.consultaUsuarios.resultadoParaExibicao}
        minCharacters={3}
        value={nome}
        placeholder="Pesquisar pacientes"
        noResultsMessage={
          <React.Fragment>
            <Security allowedRoles="ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO">
              <span
                style={{ display: "block" }}
                onClick={() => this.adicionarNovoUsuario(nome)}
              >
                Adicionar {nome}
              </span>
            </Security>
            <Security notAuthenticated>
              <span>Nenhum resultado encontrado</span>
            </Security>
          </React.Fragment>
        }
      />
    );
  }
}

ConsultaUsuario.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired
  })
};

function mapStateToProps(state) {
  return { consultaUsuarios: state.consultaUsuarios };
}

export default connect(
  mapStateToProps,
  { carregarDadosAdicionais, selecionarUsuario, consultarUsuarios, novoUsuario }
)(ConsultaUsuario);
