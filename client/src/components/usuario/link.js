import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { Icon} from 'semantic-ui-react';

import { consultarUsuario } from '../../actions/usuario';

import './style.css';

class LinkUsuario extends Component {

  navegarParaCadastroUsuario(idUsuario) {
    this.props.consultarUsuario(idUsuario, () => {
      this.context.router.history.push('/usuarios/cadastro');
    });
  }

  render() {
    const { usuario } = this.props;

    if (!usuario) {
      return null;
    }

    const { _id: id, nome } = usuario;

    return (
      <button className='ButtonLink' onClick={() => this.navegarParaCadastroUsuario(id)}>
        <Icon name='user' /> {nome}
      </button>
    );
  }
}

LinkUsuario.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  })
};

export default connect(null, { consultarUsuario })(LinkUsuario);
