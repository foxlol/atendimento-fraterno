import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Form, Button, Icon } from 'semantic-ui-react';

import { removerTodosAlertas } from '../../actions/alerta';
import { logarUsuario } from '../../actions/autenticacao';

class Login extends Component {

  constructor(props) {
    super(props);

    this.abrir = this.abrir.bind(this);
    this.fechar = this.fechar.bind(this);

    this.state = { username: '', senha: '' };
  }

  handleChange = (e, { name, value }) => {
    this.setState({ [name]: value.toLowerCase() });
  }

  handleSubmit = e => {
    const { username, senha } = this.state;

    this.props.logarUsuario({ username, senha });

    this.fechar();
  }

  abrir() {
    this.setState({ aberto: true });
    this.props.removerTodosAlertas();
  }

  fechar() {
    this.setState({
      username: '',
      senha: '',
      aberto: false
    });
  }

  render() {
    const { username, senha, aberto } = this.state;

    return (
      <Modal trigger={
            <Button onClick={this.abrir} fluid basic inverted>
              <Icon name='sign in' /> Login
            </Button>}
             open={aberto}
             onClose={this.fechar}
             closeIcon='close'>
        <Modal.Header>Login</Modal.Header>
        <Modal.Content>
          <Form onSubmit={this.handleSubmit}>
            <Form.Input placeholder='Nome de usuário'
                        name='username'
                        value={username}
                        autoFocus
                        onChange={this.handleChange} />

            <Form.Input placeholder='Senha'
                        name='senha'
                        type='password'
                        value={senha}
                        onChange={this.handleChange} />

            <Form.Button primary content='Entrar' />
          </Form>
        </Modal.Content>
      </Modal>
    );
  }
}

export default connect(null, { logarUsuario, removerTodosAlertas })(Login);
