import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import asyncPoll from 'react-async-poll';

import {
  Card, Icon, Grid, Tab, Menu
} from 'semantic-ui-react';

import { agendarAtendimento, removerAtendimento } from '../../actions/atendimento';
import { carregarDadosAdicionais } from '../../actions/usuario';

import HistoricoTratamentos from '../tratamento/historico';
import TratamentoAtual from '../tratamento/atual';
import ModalAcoesTratamento from '../tratamento/modal-acoes';
import ModalNovoTratamento from '../tratamento/modal-novo';
import ModalResumoTratamento from '../tratamento/modal-resumo';
import DadosUsuario from './dados';

import { isUserAllowed } from '../security/helper';

class CadastroUsuario extends Component {

  state = { activeIndex: 0 };

  constructor(props) {
    super();
    this.renderTabTratamentos = this.renderTabTratamentos.bind(this);
    this.renderTabDados = this.renderTabDados.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { selecionado: usuario } = nextProps.consultaUsuarios;

    if (!usuario) {
      this.setState({ activeIndex: 0 });
    }
  }

  handleTabChange = (e, { activeIndex }) => this.setState({ activeIndex });

  renderTabDados(props) {
    return (
      <Tab.Pane>
        <DadosUsuario socket={props.socket} />
      </Tab.Pane>
    );
  }

  renderTabTratamentos(props) {
    return (
      <Tab.Pane>
        <TratamentoAtual socket={props.socket} />
      </Tab.Pane>
    );
  }

  renderTabHistoricoTratamentos() {
    return (
      <Tab.Pane>
        <HistoricoTratamentos />
      </Tab.Pane>
    );
  }

  newUserPanes = [
      { menuItem: 'Dados', render: () => this.renderTabDados(this.props) }
    ];

  essentialPanes = [
      { menuItem: 'Dados', render: () => this.renderTabDados(this.props) },
      { menuItem: 'Tratamento atual', render: () => this.renderTabTratamentos(this.props) }
    ];

  allPanes = [
      { menuItem: 'Dados', render: () => this.renderTabDados(this.props) },
      { menuItem: 'Tratamento atual', render: () => this.renderTabTratamentos(this.props) },
      { menuItem: 'Histórico', render: this.renderTabHistoricoTratamentos }
    ];

  agendarAtendimento = () => {
    this.props.agendarAtendimento(this.props.consultaUsuarios.selecionado._id, false, null, () => {
      this.props.socket.emit('atualizarEstatisticasAtendimento');
    });
  }

  concluirAtendimento = (id) => {
    const { selecionado: usuarioSelecionado } = this.props.consultaUsuarios;
    const { isAtendente } = this.props.autenticacao;
    this.props.removerAtendimento(id, () => {
      this.props.socket.emit('atualizarEstatisticasAtendimento');
      
      if (isAtendente) {
        this.context.router.history.push('/atendimentos');
      } else {
        this.props.carregarDadosAdicionais(usuarioSelecionado._id);
      }
    });
  }

  renderAcoesRapidas() {
    const { selecionado: usuario, dadosAdicionais } = this.props.consultaUsuarios;

    if (!usuario || !dadosAdicionais) {
      return null;
    }

    return (
      <Menu pointing fluid size={'small'} stackable inverted color={'grey'}>
            {
              dadosAdicionais.atendimentoAtual ?
                <Menu.Item
                  onClick={
                    () => this.concluirAtendimento(
                      dadosAdicionais.atendimentoAtual._id
                    )
                  }>
                  <Icon name='comments' />
                  Concluir Atendimento
                </Menu.Item>
                :
                <Menu.Item onClick={this.agendarAtendimento}>
                  <Icon name='comments' />
                  Agendar Atendimento
                </Menu.Item>
            }
            {
              dadosAdicionais.tratamentoAtual ? null :
                <ModalNovoTratamento
                  trigger={
                    <Menu.Item>
                      <Icon name='treatment' />
                      Novo Tratamento
                    </Menu.Item>
                  }
                  socket={this.props.socket}
                  usuario={usuario} />
            }
      </Menu>
    );
  }

  render() {
    const { selecionado: usuario, dadosAdicionais } = this.props.consultaUsuarios;
    const { roles } = this.props.autenticacao;
    const { activeIndex } = this.state;

    const isUsuarioAtendimento =
      isUserAllowed(roles, 'ADMIN,ATENDIMENTO');

    const exibirResumo = dadosAdicionais
      && dadosAdicionais.tratamentoAtual
        && isUsuarioAtendimento;

    let panes;

    if (usuario) {
      if (isUsuarioAtendimento) {
        panes = this.allPanes;
      } else {
        panes = this.essentialPanes;
      }
    } else {
      panes = this.newUserPanes;
    }

    return (
      <Grid padded>
        <ModalAcoesTratamento 
            socket={this.props.socket} 
            tratamentoAtual={dadosAdicionais ? dadosAdicionais.tratamentoAtual : null} 
        />
        <Grid.Column>
          <Card fluid color='blue'>
            <Card.Content>
              <Card.Header>
                <Icon name='address book' />
                {usuario ? usuario.nome : 'Novo usuário'}
                {this.renderAcoesRapidas()}
              </Card.Header>
              <Card.Description>
                <Tab activeIndex={activeIndex}
                     onTabChange={this.handleTabChange}
                     menu={{ pointing: true }}
                     panes={panes} />
              </Card.Description>
            </Card.Content>
          </Card>

          {
            exibirResumo ?
              <ModalResumoTratamento
                tratamentoAtual={dadosAdicionais.tratamentoAtual}
                paciente={usuario.nome}
              />
              : null
          }
        </Grid.Column>
      </Grid>
    );
  }
};

CadastroUsuario.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  })
};

const onPollInterval = (props) => {
  const { selecionado: usuarioSelecionado } = props.consultaUsuarios;

  if (!usuarioSelecionado) {
    return;
  }

  props.carregarDadosAdicionais(usuarioSelecionado._id);
};

function mapStateToProps(state) {
  return {
    consultaUsuarios: state.consultaUsuarios,
    autenticacao: state.autenticacao
   };
}

export default connect(mapStateToProps, { carregarDadosAdicionais, agendarAtendimento, removerAtendimento })(asyncPoll(10 * 1000, onPollInterval)(CadastroUsuario));
