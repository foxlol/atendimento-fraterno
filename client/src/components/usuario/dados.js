import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import InputMask from "react-input-mask";
import moment from "moment";

import "moment/locale/pt-br";

import { Button, Form, Checkbox, Dropdown } from "semantic-ui-react";

import Security from "../security/component";

import { alertar } from "../../actions/alerta";

import {
  atualizarDadosUsuario,
  cadastrarUsuario,
  carregarDadosAdicionais,
  removerUsuario
} from "../../actions/usuario";

import { pesquisarCidades } from "../../actions/pesquisa";

import SearchInput from "../pesquisa/input";

const generos = [
  { key: 1, text: "Masculino", value: "M" },
  { key: 2, text: "Feminino", value: "F" }
];

const roles = [
  { key: "ADMIN", text: "Administração", value: "ADMIN" },
  { key: "RECEPCAO", text: "Recepção", value: "RECEPCAO" },
  { key: "SECRETARIA", text: "Secretaria", value: "SECRETARIA" },
  { key: "ATENDIMENTO", text: "Atendimento", value: "ATENDIMENTO" },
  { key: "ORGANIZACAO", text: "Organização", value: "ORGANIZACAO" },
  { key: "ENCAMINHAMENTO", text: "Encaminhamento", value: "ENCAMINHAMENTO" },
  { key: "SUPORTE", text: "Suporte", value: "SUPORTE" },
  { key: "MEDIUM", text: "Médium", value: "MEDIUM" },
  {
    key: "SUPORTE_ESCRIVAO",
    text: "Suporte (Escrivão)",
    value: "SUPORTE_ESCRIVAO"
  },
  { key: "PACIENTE", text: "Paciente", value: "PACIENTE" }
];

class DadosUsuario extends Component {
  toggleDeficiencia = () => {
    const { usuario } = this.state;
    _.set(usuario, "deficiente", !usuario.deficiente);
    this.setState({ usuario });
  };

  handleDateChange = e => {
    const date = e.target.value;
    const { usuario } = this.state;
    _.set(usuario, "dataNascimento", date);
    this.setState({ usuario });
  };

  handlePhoneChange = e => {
    const phone = e.target.value;
    const { usuario } = this.state;
    _.set(usuario, "telefone", phone);
    this.setState({ usuario });
  };

  handleChange = (e, { name, value }) => {
    const { usuario } = this.state;
    _.set(usuario, name, value);
    this.setState({ usuario });
  };

  handleCidadeSearchSelect = selectedValue => {
    const { usuario } = this.state;

    _.set(
      usuario,
      "endereco.cidade",
      selectedValue ? selectedValue.title : undefined
    );

    this.setState({ usuario });
  };

  handleSubmit = e => {
    const { usuario } = this.state;

    if (_.isEmpty(_.trim(usuario.email))) {
      _.unset(usuario, "email");
    }

    if (_.isEmpty(_.trim(usuario.telefone))) {
      _.unset(usuario, "telefone");
    }

    if (_.isEmpty(_.trim(usuario.dataNascimento))) {
      _.unset(usuario, "dataNascimento");
    }

    if (usuario.endereco && _.isEmpty(_.trim(usuario.endereco.logradouro))) {
      _.unset(usuario, "endereco.logradouro");
    }

    if (usuario.endereco && _.isEmpty(_.trim(usuario.endereco.numero))) {
      _.unset(usuario, "endereco.numero");
    }

    if (usuario.endereco && _.isEmpty(_.trim(usuario.endereco.bairro))) {
      _.unset(usuario, "endereco.bairro");
    }

    if (usuario.endereco && _.isEmpty(_.trim(usuario.endereco.cidade))) {
      _.unset(usuario, "endereco.cidade");
    }

    if (!_.isEmpty(usuario.dataNascimento)) {
      const dataNascimento = moment(usuario.dataNascimento, "DD-MM-YYYY");

      if (!dataNascimento.isValid()) {
        return this.props.alertar("Data de Nascimento inválida");
      }

      _.set(usuario, "dataNascimento", dataNascimento);
    } else {
      _.set(usuario, "dataNascimento", null);
    }

    if (usuario._id) {
      this.props.atualizarDadosUsuario(usuario);
    } else {
      this.props.cadastrarUsuario(usuario);
    }
  };

  setupUsuario(usuarioSelecionado, nomeNovoUsuario) {
    if (!usuarioSelecionado) {
      return {
        nome: nomeNovoUsuario,
        username: "",
        genero: "M",
        dataNascimento: "",
        email: "",
        telefone: "",
        endereco: {
          logradouro: "",
          numero: "",
          bairro: "",
          cidade: "São José do Rio Pardo - SP"
        },
        roles: ["PACIENTE"],
        senha: ""
      };
    }

    let {
      _id,
      nome,
      genero,
      dataNascimento,
      email,
      telefone,
      endereco,
      deficiente,
      roles,
      senha
    } = usuarioSelecionado;

    _.set(usuarioSelecionado, "_id", !_id ? "" : _id);
    _.set(usuarioSelecionado, "nome", !nome ? "" : nome);
    _.set(usuarioSelecionado, "senha", !senha ? "" : senha);
    _.set(usuarioSelecionado, "genero", !genero ? "M" : genero);
    _.set(usuarioSelecionado, "email", !email ? "" : email);
    _.set(usuarioSelecionado, "telefone", !telefone ? "" : telefone);
    _.set(usuarioSelecionado, "roles", !roles ? ["PACIENTE"] : roles);

    if (endereco) {
      _.set(
        usuarioSelecionado,
        "endereco.logradouro",
        !endereco.logradouro ? "" : endereco.logradouro
      );
      _.set(
        usuarioSelecionado,
        "endereco.numero",
        !endereco.numero ? "" : endereco.numero
      );
      _.set(
        usuarioSelecionado,
        "endereco.bairro",
        !endereco.bairro ? "" : endereco.bairro
      );
      _.set(
        usuarioSelecionado,
        "endereco.cidade",
        !endereco.cidade ? "" : endereco.cidade
      );
    } else {
      _.set(usuarioSelecionado, "endereco.logradouro", "");
      _.set(usuarioSelecionado, "endereco.numero", "");
      _.set(usuarioSelecionado, "endereco.bairro", "");
      _.set(usuarioSelecionado, "endereco.cidade", "");
    }

    if (_.isUndefined(deficiente)) {
      _.set(usuarioSelecionado, "deficiente", false);
    }

    if (!_.isEmpty(dataNascimento)) {
      const isFormatted = _.includes(dataNascimento, "/");

      if (isFormatted) {
        _.set(usuarioSelecionado, "dataNascimento", dataNascimento);
      } else {
        _.set(
          usuarioSelecionado,
          "dataNascimento",
          moment(dataNascimento).format("DD/MM/YYYY")
        );
      }
    } else {
      _.set(usuarioSelecionado, "dataNascimento", "");
    }

    return usuarioSelecionado;
  }

  dateFormatter(date) {
    if (!date) {
      return "";
    }

    const parsed = moment(date, "DD/MM/YYYY", true);

    return parsed.isValid() ? parsed.format("DD/MM/YYYY") : date;
  }

  constructor(props) {
    super(props);
    let {
      selecionado: usuarioSelecionado,
      nomeNovoUsuario
    } = this.props.consultaUsuarios;
    this.state = {
      usuario: this.setupUsuario(usuarioSelecionado, nomeNovoUsuario)
    };
  }

  componentWillReceiveProps(nextProps) {
    let {
      selecionado: usuarioSelecionado,
      nomeNovoUsuario
    } = this.props.consultaUsuarios;
    let {
      selecionado: usuarioSelecionadoNovo,
      nomeNovoUsuario: nomeNovoUsuarioNovo
    } = nextProps.consultaUsuarios;

    if (
      _.isEqual(usuarioSelecionado, usuarioSelecionadoNovo) &&
      _.isEqual(nomeNovoUsuario, nomeNovoUsuarioNovo)
    ) {
      return;
    }

    if (usuarioSelecionadoNovo) {
      nextProps.carregarDadosAdicionais(usuarioSelecionadoNovo._id);
    }

    this.setState({
      usuario: this.setupUsuario(usuarioSelecionadoNovo, nomeNovoUsuarioNovo)
    });
  }

  removerUsuario(usuario) {
    this.props.removerUsuario(usuario, () => {
      this.props.socket.emit("atualizarEstatisticasTratamentos");
      this.props.socket.emit("atualizarEstatisticasAtendimento");
    });
  }

  render() {
    const { usuario } = this.state;

    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Field>
          <label>Nome</label>
          <Form.Input
            name="nome"
            value={usuario.nome}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Security allowedRoles="ADMIN">
          <Form.Field>
            <label>Nome de Usuário (para login)</label>
            <Form.Input
              name="username"
              value={usuario.username}
              placeholder="Preenchimento automático"
              onChange={this.handleChange}
            />
          </Form.Field>
        </Security>
        <Form.Select
          label="Gênero"
          name="genero"
          options={generos}
          value={usuario.genero}
          onChange={this.handleChange}
        />
        <Form.Field>
          <label>Data de Nascimento</label>
          <InputMask
            name="dataNascimento"
            value={this.dateFormatter(usuario.dataNascimento)}
            mask="99/99/9999"
            onChange={this.handleDateChange}
          />
        </Form.Field>
        <Form.Group widths="equal">
          <Form.Field>
            <label>Endereço</label>
            <Form.Input
              name="endereco.logradouro"
              value={usuario.endereco.logradouro}
              onChange={this.handleChange}
            />
          </Form.Field>
          <Form.Field>
            <label>Número</label>
            <Form.Input
              name="endereco.numero"
              value={usuario.endereco.numero}
              onChange={this.handleChange}
            />
          </Form.Field>
          <Form.Field>
            <label>Bairro</label>
            <Form.Input
              name="endereco.bairro"
              value={usuario.endereco.bairro}
              onChange={this.handleChange}
            />
          </Form.Field>
        </Form.Group>
        <Form.Field>
          <label>Cidade</label>
          <SearchInput
            name="endereco.cidade"
            searchAction={this.props.pesquisarCidades}
            onSelect={this.handleCidadeSearchSelect}
            initialValue={usuario.endereco.cidade}
          />
        </Form.Field>
        <Form.Group widths="equal">
          <Form.Field>
            <label>Telefone</label>
            <InputMask
              name="telefone"
              value={usuario.telefone}
              mask="(99) 9999-99999"
              onChange={this.handlePhoneChange}
            />
          </Form.Field>
          <Form.Field>
            <label>E-mail</label>
            <Form.Input
              name="email"
              value={usuario.email}
              onChange={this.handleChange}
            />
          </Form.Field>
        </Form.Group>
        <Security allowedRoles="ADMIN">
          <Form.Field>
            <label>Funções</label>
            <Dropdown
              name="roles"
              placeholder="Selecione ..."
              fluid
              multiple
              selection
              value={usuario.roles}
              options={roles}
              closeOnChange={true}
              onChange={this.handleChange}
            />
          </Form.Field>
        </Security>
        <Security allowedRoles="ADMIN">
          <Form.Field>
            <label>Senha</label>
            <Form.Input
              name="senha"
              value={usuario.senha}
              onChange={this.handleChange}
            />
          </Form.Field>
        </Security>
        <Form.Field>
          <Checkbox
            name="deficiente"
            toggle
            label="Atendimento prioritário?"
            checked={usuario.deficiente}
            onChange={this.toggleDeficiencia}
          />
        </Form.Field>

        <Button type="submit" positive>
          Salvar dados
        </Button>

        {usuario._id && (
          <Security allowedRoles="ADMIN">
            <Button
              type="button"
              negative
              onClick={() => this.removerUsuario(usuario)}
            >
              Excluir usuário
            </Button>
          </Security>
        )}
      </Form>
    );
  }
}

function mapStateToProps(state) {
  return {
    consultaUsuarios: state.consultaUsuarios
  };
}

DadosUsuario.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired
  })
};

export default connect(
  mapStateToProps,
  {
    carregarDadosAdicionais,
    atualizarDadosUsuario,
    cadastrarUsuario,
    removerUsuario,
    alertar,
    pesquisarCidades
  }
)(DadosUsuario);
