import React from 'react';

import Security from '../security/component';
import TratamentosPrevistos from '../tratamento/previstos';

const Home = (props) => (
  <div>
    <Security allowedRoles="ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO">
      <TratamentosPrevistos socket={props.socket} />
    </Security>
  </div>
);

export default Home;
