import React, { Component } from 'react';

import withWSConnection from '../websocket/index';
import Menu from './menu';

const WSConnectedMenu = withWSConnection(Menu);

export default class Header extends Component {
  render() {
    return (
      <div>
        <WSConnectedMenu />
      </div>
    );
  }
};
