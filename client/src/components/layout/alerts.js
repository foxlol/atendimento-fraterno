import _ from 'lodash';
import React, { Component } from 'react';
import { Message, Transition } from 'semantic-ui-react'
import { connect } from 'react-redux';

import { removerErro, removerSucesso, removerAlerta, removerTodosAlertas } from '../../actions/alerta';

class Alerts extends Component {

  render() {
    const { erro, sucesso, alerta } = this.props.alerta;
    const hasAlerta = erro || sucesso || alerta;

    if (hasAlerta) {
      window.scrollTo(0, 0);

      setTimeout(() => {
        this.props.removerTodosAlertas();
      }, 5000);
    }

    return (
      <div style={{margin: '10px'}}>
        <Transition visible={!_.isEmpty(erro)} animation='scale' duration={500}>
          <Message header='' negative content={erro}
                   onDismiss={this.props.removerErro} />
        </Transition>

        <Transition visible={!_.isEmpty(alerta)} animation='scale' duration={500}>
          <Message header='' warning content={alerta}
                   onDismiss={this.props.removerAlerta} />
        </Transition>

        <Transition visible={!_.isEmpty(sucesso)} animation='scale' duration={500}>
          <Message header='' success content={sucesso}
                   onDismiss={this.props.removerSucesso} />
        </Transition>
      </div>
    );
  }
};

function mapStateToProps(state) {
  return { alerta: state.alerta }
};

export default connect(mapStateToProps, { removerErro, removerSucesso, removerAlerta, removerTodosAlertas })(Alerts);
