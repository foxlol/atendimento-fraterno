import _ from "lodash";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { Menu, Label, Dropdown, Header, Image, Icon } from "semantic-ui-react";

import { removerTodosAlertas } from "../../actions/alerta";
import { logarUsuario, deslogarUsuario } from "../../actions/autenticacao";
import { novoUsuario } from "../../actions/usuario";
import {
  consultarTratamentosVerificados,
  consultarTratamentosVerificadosProcesso
} from "../../actions/tratamento";

import ConsultaUsuario from "../usuario/consulta";
import Security from "../security/component";
import Login from "../usuario/login";
import logo from "./logo.png";

class AppMenu extends Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
    this.state = {};
  }

  getNomeUsuario() {
    const { usuario } = this.props.autenticacao;
    return usuario ? _.words(usuario.nome)[0] : "";
  }

  getUserName() {
    const { usuario } = this.props.autenticacao;
    return usuario ? usuario.username : "";
  }

  novoPaciente(e, { name }) {
    this.props.novoUsuario(() => {
      this.handleItemClick(e, { name }, "/usuarios/cadastro");
    });
  }

  handleItemClick(e, { name }, location) {
    this.setState({ activeItem: name });

    if (location) {
      this.context.router.history.push(`${location}`);
    }
  }

  listagemTratamentos(e, { name }, tipo) {
    this.props.consultarTratamentosVerificados(tipo, () => {
      this.handleItemClick(e, { name }, `/tratamentos/${tipo}`);
    });
  }

  logout() {
    this.props.deslogarUsuario(() => {
      this.props.removerTodosAlertas();
      this.context.router.history.push("/");
      window.location.reload(true);
    });
  }

  render() {
    const { activeItem } = this.state;

    const {
      atendimentos,
      tratamentosEspirituais,
      tratamentosSaude,
      tratamentosDistancia,
      tratamentosDistanciaSaude
    } = this.props.estatisticas;

    const totalTratamentos =
      tratamentosEspirituais +
      tratamentosSaude +
      tratamentosDistancia +
      tratamentosDistanciaSaude;

    return (
      <Menu fluid size="small" stackable inverted color={"blue"}>
        <Menu.Item
          name="home"
          active={activeItem === "home"}
          onClick={(e, attr) => this.handleItemClick(e, attr, "/")}
        >
          <Header as="h4" icon textAlign="center" inverted>
            <Image fluid shape="rounded" src={logo} /> Atendimento Fraterno
          </Header>
        </Menu.Item>

        <Security allowedRoles="ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO">
          <Menu.Item>
            <ConsultaUsuario />
          </Menu.Item>
        </Security>

        {/* <Security allowedRoles="ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO">
          <Menu.Item
            name='novo-paciente' header
            active={activeItem === 'novo-paciente'}
            onClick={(e, attr) => this.novoPaciente(e, attr)}>

            <Icon name='add user' />
            Novo Paciente
          </Menu.Item>
        </Security> */}

        <Security allowedRoles="ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO">
          <Menu.Item
            name="atendimentos"
            header
            active={activeItem === "atendimentos"}
            onClick={(e, attr) =>
              this.handleItemClick(e, attr, "/atendimentos")
            }
          >
            <Icon name="comments" />
            Atendimentos
            {atendimentos > 0 && <Label color="red">{atendimentos}</Label>}
          </Menu.Item>
        </Security>

        <Security allowedRoles="ADMIN,ATENDIMENTO">
          <Menu.Item
            name="consulta-tratamentos"
            header
            active={activeItem === "consulta-tratamentos"}
            onClick={(e, attr) =>
              this.handleItemClick(e, attr, "/consulta/tratamentos")
            }
          >
            <Icon name="search" />
            Consultar Tratamentos
          </Menu.Item>
        </Security>

        <Security allowedRoles="ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO,ORGANIZACAO,ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO">
          <Dropdown
            item
            icon={
              totalTratamentos > 0 && (
                <Label color="red">{totalTratamentos}</Label>
              )
            }
            text={`Tratamentos de Hoje`}
          >
            <Dropdown.Menu>
              <Dropdown.Item
                text="Espiritual"
                onClick={(e, attr) => {
                  this.listagemTratamentos(e, attr, "espiritual");
                }}
                description={tratamentosEspirituais}
                label={{ color: "blue", empty: true, circular: true }}
              />
              <Dropdown.Item
                text="Saúde"
                onClick={(e, attr) => {
                  this.listagemTratamentos(e, attr, "saude");
                }}
                description={tratamentosSaude}
                label={{ color: "yellow", empty: true, circular: true }}
              />
              <Dropdown.Item
                text="Distância (Espiritual)"
                onClick={(e, attr) => {
                  this.listagemTratamentos(e, attr, "distancia");
                }}
                description={tratamentosDistancia}
                label={{ color: "teal", empty: true, circular: true }}
              />
              <Dropdown.Item
                text="Distância (Saúde)"
                onClick={(e, attr) => {
                  this.listagemTratamentos(e, attr, "distancia_saude");
                }}
                description={tratamentosDistanciaSaude}
                label={{ color: "orange", empty: true, circular: true }}
              />
            </Dropdown.Menu>
          </Dropdown>
        </Security>

        <Security authenticated>
          <Menu.Menu position="right">
            <Menu.Item header>
              <Icon name="user" />
              <Dropdown fluid labeled text={`Olá, ${this.getNomeUsuario()}`}>
                <Dropdown.Menu>
                  <Dropdown.Item
                    text={`Entrou como ${this.getUserName()}`}
                    disabled
                  />
                  <Dropdown.Item
                    text="Sair"
                    icon="sign out"
                    onClick={this.logout}
                  />
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Item>
          </Menu.Menu>
        </Security>

        <Security notAuthenticated>
          <Menu.Menu position="right">
            <Menu.Item header name="login" active={activeItem === "login"}>
              <Login />
            </Menu.Item>
          </Menu.Menu>
        </Security>
      </Menu>
    );
  }
}

AppMenu.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired
  })
};

function mapStateToProps(state) {
  return { autenticacao: state.autenticacao };
}

export default connect(
  mapStateToProps,
  {
    consultarTratamentosVerificadosProcesso,
    consultarTratamentosVerificados,
    logarUsuario,
    deslogarUsuario,
    removerTodosAlertas,
    novoUsuario
  }
)(AppMenu);
