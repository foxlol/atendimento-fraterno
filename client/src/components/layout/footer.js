﻿import React, { Component } from "react";
import { Segment } from "semantic-ui-react";

import "./footer.css";

export default class Footer extends Component {
  render() {
    return (
      <Segment size="tiny" className="bottom" textAlign="center">
        © COPYRIGHT 2017-2023 - Fundação Espírita Bezerra de Menezes -{" "}
        <strong>Versão 08.05.2023</strong>
      </Segment>
    );
  }
}
