import React, {Component} from 'react'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Dimmer, Loader } from 'semantic-ui-react';

import Header from './header'
import Footer from './footer'
import Alerts from './alerts';

import './index.css';

class Layout extends Component {

  componentDidUpdate() {
    this.scrollToMainDiv();
  }

  scrollToMainDiv() {
    const mainDiv = ReactDOM.findDOMNode(this.refs.mainDiv)
    if (mainDiv) {
      mainDiv.scrollIntoView(true);
    }
  }

  render() {
    return (
      <div>
        <Dimmer.Dimmable dimmed={this.props.loader.isLoading}>
          <Dimmer active={this.props.loader.isLoading}>
            <Loader>Carregando ...</Loader>
          </Dimmer>
          <Header />
          <div ref="mainDiv">
            <Alerts />
            {this.props.children}
          </div>
          <Footer />
        </Dimmer.Dimmable>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return { loader: state.loader };
}

export default connect(mapStateToProps)(Layout);
