import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Loadable from 'react-loadable';
import Loading from '../layout/loader';

import Layout from '../layout/index';
import Security from '../security/component';

import withWSConnection from '../websocket/index';

const Home = withWSConnection(Loadable({
  loader: () => import('../home/index'),
  loading: Loading,
}));

const Atendimentos = withWSConnection(Loadable({
  loader: () => import('../atendimento/index'),
  loading: Loading,
}));

const CadastroUsuario = withWSConnection(Loadable({
  loader: () => import('../usuario/cadastro'),
  loading: Loading,
}));

const ListagemTratamento = withWSConnection(Loadable({
  loader: () => import('../tratamento/listagem'),
  loading: Loading,
}));

const ConsultaTratamentos = Loadable({
  loader: () => import('../tratamento/consulta'),
  loading: Loading,
});

class App extends Component {

  render() {
    return (
      <Layout>
        <Security allowedRoles="ADMIN,ATENDIMENTO">
          <Route exact path="/consulta/tratamentos" component={ConsultaTratamentos} />
        </Security>
        <Security allowedRoles="ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO,ORGANIZACAO,ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO">
          <Route path="/tratamentos/:tipo" component={ListagemTratamento} />
        </Security>
        <Security allowedRoles="ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO">
          <Route path="/atendimentos" component={Atendimentos} />
        </Security>
        <Security allowedRoles="ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO">
          <Route path="/usuarios/cadastro" component={CadastroUsuario} />
        </Security>
        <Route exact path="/" component={Home} />
      </Layout>
    );
  }
}

export default App;
