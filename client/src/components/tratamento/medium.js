import React, { Component } from "react";
import { Dropdown, Label } from "semantic-ui-react";

export default class MediumInput extends Component {

  mapMediums = (mediums) => {
    if (!mediums) {
      return [];
    }

    return mediums.map((medium) => {
      return {
        key: medium._id,
        text: medium.nome,
        value: medium.nome
      }
    });
  };

  render() {
    const { medium, handleChange, isAtendente, style, mediums } = this.props;

    return (
      <Label attached="top" style={style}>
        <label>
          <strong>Médium: </strong>
        </label>
        {isAtendente ? (
          <Dropdown
            options={this.mapMediums(mediums)}
            placeholder="Selecione um médium..."
            search
            upward
            selection
            additionLabel="Adicionar "
            value={medium}
            onChange={handleChange}
          />
        ) : (
          <span>
            <strong>{medium}</strong>
          </span>
        )}
      </Label>
    );
  }
}
