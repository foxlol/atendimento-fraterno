import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Card, Divider, Grid, Icon, Accordion, Header, Label, Dropdown, Button } from 'semantic-ui-react';

import {
  consultarTodosTratamentos, adicionarObservacaoMediunica
} from '../../actions/tratamento';

import { pesquisarUsuarios } from '../../actions/pesquisa';

import Tipo from './tipo';
import ObservacoesMediunicas from './observacoes-mediunicas';
import ModalObservacaoMediunica from './modal-obs-mediunica';
import LinkUsuario from '../usuario/link';
import SearchInput from '../pesquisa/input';

const tipos = [
  { key: 'espiritual', text: 'Espiritual', value: 'espiritual' },
  { key: 'saude', text: 'Saúde', value: 'saude' },
  { key: 'distancia', text: 'A Distância (Espiritual)', value: 'distancia' },
  { key: 'distancia_saude', text: 'A Distância (Saúde)', value: 'distancia_saude' }
];

const situacoes = [
  { key: 'todas', text: 'Todas situações', value: undefined },
  { key: 'em_andamento', text: 'Em andamento', value: 'false' },
  { key: 'concluido', text: 'Concluído', value: 'true' }
];

class ConsultaTratamentos extends Component {

  state = {
    tipo: [],
    concluido: undefined,
    paciente: undefined,
    activeIndex: -1
  };

  consultarTodosTratamentos() {
    const { tipo, concluido, paciente } = this.state;
    this.setState({activeIndex: -1});
    this.props.consultarTodosTratamentos({ tipo, concluido, paciente });
  }

  obterDataInicioTratamento(tratamento) {
    const primeiroAgendamento = _.head(tratamento.agendamentos);
    return moment(primeiroAgendamento.data).format('DD/MM/YYYY');
  }

  obterDataFinalTratamento(tratamento) {
    const ultimoAgendamento = _.last(tratamento.agendamentos);
    return moment(ultimoAgendamento.data).format('DD/MM/YYYY');
  }

  adicionarObservacaoMediunica = (observacaoMediunica, idTratamento) => {
    this.props.adicionarObservacaoMediunica(
      idTratamento, observacaoMediunica, () =>  {
        this.consultarTodosTratamentos();
      });
  };

  selecionar = (index) => {
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index
    this.setState({ activeIndex: newIndex })
  }

  handleTipoChange = (e, { value }) => {
    this.setState({ tipo: value });
  };

  handleSituacaoChange = (e, { value }) => {
    this.setState({ concluido: value });
  };

  handleUsuarioSearchSelect = (selectedValue) => {
    this.setState({ paciente: selectedValue ? selectedValue.key : undefined });
  }

  renderTratamentos(listagem) {
    if (_.isEmpty(listagem)) {
      return <div>Nenhum resultado encontrado</div>
    }

    const { activeIndex } = this.state;

    return (
      <div>
        <strong>Total de resultados:</strong> {listagem.length}
        <Divider hidden />
        <Accordion fluid styled>
         {
           _.map(listagem, (tratamento, index) => {
             const dataInicio = this.obterDataInicioTratamento(tratamento);
             const dataFinal = this.obterDataFinalTratamento(tratamento);
             const motivo = tratamento.motivo || 'Não informado';
             const intercessor = tratamento.intercessor;

             return (
               <div key={index}>
                 <Accordion.Title
                   active={activeIndex === index}
                   onClick={() => this.selecionar(index)}
                 >
                   {tratamento.concluido ?
                     <Label size="mini" as='a' color='green' ribbon>Concluído</Label>
                     : <Label size="mini" as='a' color='red' ribbon>Em andamento</Label>
                   }
                   <Icon name='dropdown' />
                   <LinkUsuario usuario={tratamento.paciente} />
                   <Header as='h4'>
                     {dataInicio} a {dataFinal}
                   </Header>
                   <Tipo tipos={tratamento.tipo} />
                 </Accordion.Title>
                 <Accordion.Content active={activeIndex === index}>
                   <strong>Motivo: </strong> {motivo}
                   {
                     intercessor &&
                     <div>
                       <Divider hidden />
                       <strong>A pedido de: </strong> {intercessor}
                     </div>
                   }
                   <Divider hidden />
                   <ObservacoesMediunicas tratamento={tratamento} />
                   <Divider hidden />
                   <ModalObservacaoMediunica
                     onConfirm={
                       (observacaoMediunica) =>
                         this.adicionarObservacaoMediunica(
                           observacaoMediunica, tratamento._id
                         )
                     } />
                 </Accordion.Content>
              </div>
             );
           })
         }
        </Accordion>
      </div>
    );
  }

  renderFiltro() {
    const { tipo, concluido } = this.state;

    return (
      <Grid padded stackable>
        <Grid.Row>
          <Grid.Column width={1}>
            <label>Filtrar</label>
          </Grid.Column>
          <Grid.Column width={6}>
            <Dropdown placeholder='Tipo' fluid multiple selection
                      value={tipo} options={tipos} closeOnChange={true}
                      onChange={this.handleTipoChange} />
          </Grid.Column>
          <Grid.Column width={3}>
            <Dropdown placeholder='Situação' fluid selection
                      value={concluido} options={situacoes}
                      onChange={this.handleSituacaoChange} />
          </Grid.Column>
          <Grid.Column width={4}>
            <SearchInput
              placeholder="Paciente"
              searchAction={this.props.pesquisarUsuarios}
              onSelect={this.handleUsuarioSearchSelect} />
          </Grid.Column>
          <Grid.Column>
            <Button
              primary
              onClick={() => this.consultarTodosTratamentos()}
            >
              Consultar
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }

  render() {
    const { listagem } = this.props.tratamentos;

    return (
      <Grid padded>
        <Grid.Column>
          <Card fluid color='blue'>
            <Card.Content>
              <Card.Header>
                <Icon name='search' /> Consulta de Tratamentos
              </Card.Header>
              <Card.Description>
                {this.renderFiltro()}
                {this.renderTratamentos(listagem)}
              </Card.Description>
            </Card.Content>
          </Card>
        </Grid.Column>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return { tratamentos : state.tratamentos };
}

export default connect(mapStateToProps, { pesquisarUsuarios, consultarTodosTratamentos, adicionarObservacaoMediunica })(ConsultaTratamentos);
