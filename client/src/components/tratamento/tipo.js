import _ from 'lodash';
import React from 'react';
import { Label } from 'semantic-ui-react';

const TIPOS = {
  espiritual: {texto: 'Espiritual', cor: 'blue'},
  saude: {texto: 'Saúde', cor: 'yellow'},
  distancia: {texto: 'A Distancia (Espiritual)', cor: 'teal'},
  distancia_saude: {texto: 'A Distancia (Saúde)', cor: 'orange'}
};

export default (props) => {
  const { tipos } = props;

  return (
    <div>
      {
        _.map(tipos, (tipo) => {
          return (
            <Label key={tipo}
                   horizontal
                   color={TIPOS[tipo].cor}>
              {TIPOS[tipo].texto}
           </Label>
         )
        })
      }
    </div>
  )
};
