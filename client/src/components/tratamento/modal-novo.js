import _ from 'lodash';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import {
  Button, Header, Dropdown, Modal, Form, Input, Message, Transition, Label
} from 'semantic-ui-react';
import Datetime from 'react-datetime';
import moment from 'moment';

import { alertar } from '../../actions/alerta';
import { cadastrarTratamento } from '../../actions/tratamento';

import '../datetime/datetime.css';

const tipos = [
  { key: 'espiritual', text: 'Espiritual', value: 'espiritual' },
  { key: 'saude', text: 'Saúde', value: 'saude' },
  { key: 'distancia', text: 'A Distância (Espiritual)', value: 'distancia' },
  { key: 'distancia_saude', text: 'A Distância (Saúde)', value: 'distancia_saude' }
];

class ModalNovoTratamento extends Component {

  state = {
    open: false, 
    motivo: '',
    intercessor: '', 
    tipo: [],
    dataInicio: moment(), 
    semanas: 4, 
    alerta: undefined,
    loading: false
  };

  reset = () => {
    this.setState({
      open: false, motivo: '', intercessor: '',
      tipo: [], dataInicio: moment(), semanas: 4, alerta: undefined
    });
  }

  open = () => this.setState({ open: true });

  handleDateChange = (date) => {
    this.setState({dataInicio: date});
  };

  handleTipoChange = (e, { value }) => {
    if (_.includes(value, 'distancia') || _.includes(value, 'distancia_saude')) {
        this.exibirAlerta('Lembre-se de verificar o endereço do paciente');
    }

    this.setState({ tipo: value });
  };

  handleSemanasChange = (e, { value }) => {
    this.setState({ semanas: value });
  };

  handleChangeMotivo = (e, { value }) => {
    this.setState({ motivo: value });
  };

  handleChangeIntercessor = (e, { value }) => {
    this.setState({ intercessor: value });
  };

  scrollToInputDuracao = () => {
    const inputDuracao = ReactDOM.findDOMNode(this.refs.inputDuracao)
    if (inputDuracao) {
      setTimeout(() => inputDuracao.scrollIntoView(true), 500);
    }
  };

  confirm = () => {
    const { tipo, dataInicio, semanas, motivo, intercessor } = this.state;
    const { _id: idUsuario } = this.props.usuario;
    const { isAtendente, usuario: usuarioLogado } = this.props.autenticacao;

    if (!moment(dataInicio).isValid()) {
      return this.exibirAlerta('Data de Início inválida');
    }

    if (moment(dataInicio).isBefore(new Date(), 'day')) {
      return this.exibirAlerta('A Data de Início não pode estar no passado');
    }

    if (_.isEmpty(tipo)) {
      return this.exibirAlerta('Tipo inválido');
    }

    if (semanas <= 0) {
      return this.exibirAlerta('Duração inválida');
    }

    this.setState({ loading: true });

    let datasAgendamentos = this.criarDatasAgendamentos(dataInicio, semanas);
    let idAtendente = isAtendente ? usuarioLogado._id : undefined;
    
    this.props.cadastrarTratamento(
      idUsuario, tipo, datasAgendamentos, idAtendente, motivo, intercessor, () => {
        this.setState({ loading: false, open: false });
        this.props.socket.emit('atualizarEstatisticasTratamentos');
      });
  };

  criarDatasAgendamentos = (inicio, semanas) => {
    var dataAgendamento = moment(inicio).startOf('day').utc();
    var datas = [ dataAgendamento.toDate() ];

    for (var semana = 1; semana < semanas; semana++) {
      dataAgendamento = dataAgendamento.add(7, 'days');

      datas.push(dataAgendamento.toDate());
    }

    return datas;
  };

  exibirAlerta = (alerta) => {
    this.modal.ref.scrollIntoView();

    this.setState({alerta});

    setTimeout(() => {
      this.removerAlerta();
    }, 5000);
  };

  removerAlerta = () => {
    this.setState({alerta: undefined});
  };

  hasTipoDistancia = (tipo) => {
    return _.some(tipo, (tipo) => {
      return _.startsWith(tipo, 'distancia');
    });
  };

  render() {
    const { open, motivo, tipo, dataInicio, semanas, alerta, intercessor, loading } = this.state;

    return (
      <Modal trigger={this.props.trigger} size='small'
             ref={(modal) => { this.modal = modal; }}
             open={open} onOpen={this.open} onClose={this.reset}>
        <Modal.Header>Novo Tratamento</Modal.Header>
        <Modal.Content>
          <Transition visible={!_.isEmpty(alerta)} animation='scale' duration={500}>
            <Message header='' warning content={alerta}
                     onDismiss={this.removerAlerta} />
          </Transition>
          <Modal.Description>
            <Header>{this.props.usuario.nome}</Header>

            <Form>
              <Form.Field>
                <label>Tipo</label>
                <Dropdown placeholder='Selecione...'
                          scrolling multiple selection fluid
                          value={tipo} options={tipos} closeOnChange={true}
                          onChange={this.handleTipoChange} />
                <Label as='a' basic pointing color='yellow'>
                  Lembre-se de confirmar o endereço do paciente
                  para tratamentos a distância
                </Label>
              </Form.Field>
              <Form.Field>
                <label>Motivo</label>
                <Form.Input value={motivo} onChange={this.handleChangeMotivo} />
              </Form.Field>
              { this.hasTipoDistancia(tipo) &&
                <Form.Field>
                  <label>A Pedido de:</label>
                  <Form.Input value={intercessor} onChange={this.handleChangeIntercessor} />
                </Form.Field>
              }
              <Form.Field>
                <label>Início</label>
                <Datetime dateFormat="DD/MM/YYYY"
                          timeFormat={false}
                          closeOnSelect={true}
                          value={dataInicio}
                          input={true}
                          onChange={this.handleDateChange} />
              </Form.Field>
              <Form.Field>
                <label>Duração (não inclui retorno)</label>
                <Input
                  label={{ basic: true, content: 'semanas' }}
                  labelPosition='right'
                  type="number"
                  value={semanas}
                  ref="inputDuracao"
                  onFocus={this.scrollToInputDuracao}
                  onChange={this.handleSemanasChange} />
              </Form.Field>
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button color='red' icon='cancel' content='Cancelar' onClick={this.reset} />
          <Button color='green' icon='check' content='Confirmar' onClick={this.confirm} disabled={loading} loading={loading} />
        </Modal.Actions>
      </Modal>
    );
  };
}

function mapStateToProps(state) {
  return { autenticacao : state.autenticacao };
}

export default connect(mapStateToProps, { alertar, cadastrarTratamento })(ModalNovoTratamento);
