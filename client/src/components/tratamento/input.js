import _ from "lodash";
import React, { Component } from "react";
import {
  Grid,
  Button,
  TextArea,
  Header,
  Icon,
  Segment,
  Label,
  Form,
  Divider
} from "semantic-ui-react";
import { connect } from "react-redux";

import MediumInput from "./medium";
import Security from "../security/component";

class InputTratamento extends Component {
  constructor(props) {
    super(props);
    this.state = { tratamento: props.tratamento, observacoesMediunicas: [] };
  }

  componentWillReceiveProps(nextProps) {
    const { tratamento, socket } = nextProps;

    this.setState({ tratamento });

    if (socket) {
      socket.on("sincronizarObservacoesMediunicas", observacoesMediunicas => {
        this.setState({ observacoesMediunicas });
      });

      socket.on(
        "sincronizarObservacaoMediunica",
        observacaoMediunicaAtualizada => {
          this.setState({
            observacoesMediunicas: this.state.observacoesMediunicas.map(
              observacaoMediunica => {
                return observacaoMediunica._id ===
                  observacaoMediunicaAtualizada._id
                  ? observacaoMediunicaAtualizada
                  : observacaoMediunica;
              }
            )
          });
        }
      );
    }
  }

  sincronizarObservacaoMediunica = _.debounce(observacaoMediunicaAtualizada => {
    this.props.socket.emit(
      "atualizarObservacaoMediunicaTratamento",
      observacaoMediunicaAtualizada
    );
  }, 1000);

  atualizarObservacaoMediunica(observacaoMediunicaAtualizada) {
    this.sincronizarObservacaoMediunica(observacaoMediunicaAtualizada);

    this.setState({
      observacoesMediunicas: this.state.observacoesMediunicas.map(
        observacaoMediunica => {
          return observacaoMediunica._id === observacaoMediunicaAtualizada._id
            ? observacaoMediunicaAtualizada
            : observacaoMediunica;
        }
      )
    });
  }

  removerObservacoesMediunicas() {
    this.props.socket.emit("removerObservacoesMediunicas");
  }

  anunciarPaciente(nome) {
    window.speechSynthesis.speak(new SpeechSynthesisUtterance(nome));
  }

  getObservacaoMediunica = id => {
    const { observacoesMediunicas } = this.state;

    return observacoesMediunicas.find(observacaoMediunica => {
      return observacaoMediunica._id === id;
    });
  };

  agruparObservacoesMediunicas = observacoesMediunicas => {
    const { tratamento } = this.state;

    const separator = "[c]";
    let observacoesAgrupadas = "";

    _.forEach(observacoesMediunicas, observacaoMediunica => {
      if (!_.isEmpty(_.trim(observacaoMediunica.conteudo))) {
        observacoesAgrupadas +=
          _.trim(observacaoMediunica.conteudo) + separator;
      }
    });

    observacoesAgrupadas = observacoesAgrupadas.substring(
      0,
      observacoesAgrupadas.length - separator.length
    );

    _.set(tratamento, "observacaoMediunica", observacoesAgrupadas);
  };

  mediumHandler = (value, id) => {
    const observacaoMediunica = this.getObservacaoMediunica(id);

    observacaoMediunica.medium = value;

    this.atualizarObservacaoMediunica(observacaoMediunica);
  };

  observacaoHandler = (id, event, { value }) => {
    const { observacoesMediunicas } = this.state;

    const observacaoMediunica = observacoesMediunicas.filter(
      observacaoMediunica => {
        return observacaoMediunica._id === id;
      }
    )[0];

    observacaoMediunica.conteudo = value;

    this.atualizarObservacaoMediunica(observacaoMediunica);
  };

  adicionarObservacaoMediunica() {
    const { usuarioLogado: atendente } = this.props;

    this.props.socket.emit("adicionarObservacaoMediunicaTratamento", atendente);
  }

  removerObservacaoMediunica(id) {
    let { observacoesMediunicas } = this.state;

    if (_.isEmpty(observacoesMediunicas)) {
      return;
    }

    observacoesMediunicas = observacoesMediunicas.filter(
      observacaoMediunica => {
        return observacaoMediunica._id !== id;
      }
    );

    this.props.socket.emit("removerObservacaoMediunicaTratamento", id);

    this.setState({ observacoesMediunicas });
  }

  concluirTratamento = tratamento => {
    this.agruparObservacoesMediunicas(this.state.observacoesMediunicas);
    this.props.onConcluido(tratamento);
    this.removerObservacoesMediunicas();
  };

  cancelarTratamento = tratamento => {
    this.props.onCancelado(tratamento.id);
    this.removerObservacoesMediunicas();
  };

  renderObservacoesMediunicas() {
    const { observacoesMediunicas } = this.state;
    const { usuarioLogado, mediums } = this.props;

    if (_.isEmpty(observacoesMediunicas)) {
      return null;
    }

    return _.map(observacoesMediunicas, (observacaoMediunica, mapIndex) => {
      const { _id, atendente, medium, conteudo } = observacaoMediunica;
      const isAtendente = usuarioLogado.nome === atendente.nome;
      const style = isAtendente
        ? { backgroundColor: "#ffeead" }
        : { backgroundColor: "#e8e8e8" };

      return (
        <Segment key={_id} padded style={style}>
          <Label attached="bottom" style={style}>
            <p>
              Atendente: <strong>{atendente.nome}</strong>
            </p>
          </Label>
          <MediumInput
            medium={medium}
            isAtendente={isAtendente}
            style={style}
            handleChange={(e, { value }) => this.mediumHandler(value, _id)}
            mediums={mediums ? mediums.usuarios : []}
          />
          <Divider />
          <Form>
            <TextArea
              autoHeight
              value={conteudo}
              onChange={(e, data) =>
                this.observacaoHandler(_id, e, data, observacaoMediunica)
              }
              style={{ width: "100%" }}
            />
          </Form>
          <Divider />
          {isAtendente && (
            <Label
              as="a"
              color={"red"}
              onClick={() => {
                this.removerObservacaoMediunica(_id);
              }}
            >
              Remover
              <Icon name="delete" />
            </Label>
          )}
        </Segment>
      );
    });
  }

  renderEndereco(endereco) {
    if (!endereco) {
      return "Endereço não informado";
    }

    let enderecoCompleto = "";

    const { logradouro, numero, bairro } = endereco;

    if (logradouro) {
      enderecoCompleto += logradouro;
    }

    if (numero) {
      enderecoCompleto += ", " + numero;
    }

    if (bairro) {
      enderecoCompleto += " - " + bairro;
    }

    if (_.isEmpty(enderecoCompleto)) {
      return "Endereço não informado";
    }

    return enderecoCompleto;
  }

  render() {
    const { tratamento } = this.state;

    if (!tratamento) {
      return <div />;
    }

    return (
      <Grid padded>
        <Grid.Row>
          <Grid.Column>
            <Header size="huge">
              {tratamento.nomePaciente}{" "}
              <Button
                icon="announcement"
                content="Anunciar"
                size="small"
                color="orange"
                onClick={() => this.anunciarPaciente(tratamento.nomePaciente)}
              />
            </Header>
            <Header as="h4">
              <Header color="grey" size="medium">
                {this.renderEndereco(tratamento.endereco)}
              </Header>
              <Header.Subheader>
                <Header color="grey" size="small">
                  {tratamento.endereco.cidade || "Cidade não informada"}
                </Header>
              </Header.Subheader>
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Security allowedRoles="SUPORTE_ESCRIVAO">
          <Grid.Row>
            <Grid.Column>{this.renderObservacoesMediunicas()}</Grid.Column>
          </Grid.Row>
        </Security>
        <Security allowedRoles="SUPORTE_ESCRIVAO">
          <Grid.Row>
            <Grid.Column>
              <Button
                compact
                onClick={() => this.adicionarObservacaoMediunica()}
                icon
                labelPosition="right"
              >
                Adicionar observação
                <Icon name="add" />
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Security>
        <Grid.Row>
          <Grid.Column>
            <Button
              compact
              primary
              onClick={() => this.concluirTratamento(tratamento)}
            >
              Concluir
            </Button>
            <Button compact onClick={() => this.cancelarTratamento(tratamento)}>
              Cancelar
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    mediums: state.usuariosPorRole
  };
}

export default connect(mapStateToProps)(InputTratamento);
