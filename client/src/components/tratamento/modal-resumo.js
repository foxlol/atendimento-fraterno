import React, { Component } from 'react';
import { Button, Header, Modal, Icon, Divider } from 'semantic-ui-react';
import ObservacoesMediunicas from './observacoes-mediunicas';
import Tipo from './tipo';
import Security from '../security/component';

class ModalResumoTratamento extends Component {

  componentWillReceiveProps(nextProps) {
    const { paciente: pacienteAtual } = this.props;
    const { paciente: pacienteNovo } = nextProps;

    if (pacienteAtual !== pacienteNovo) {
      this.setState({ modalOpen: true });
    }
  }

  state = { modalOpen: true };

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => {
    this.setState({ modalOpen: false });
  }

  render() {
    const { tratamentoAtual, paciente } = this.props;

    return (
      <Security allowedRoles="ADMIN,ATENDIMENTO">
        <Modal
          open={this.state.modalOpen}
          size='small'
        >
          <Header icon='treatment' content={`Resumo do tratamento atual de ${paciente}`} />
          <Modal.Content scrolling>
            <p><strong>Tipo</strong></p>
            <Tipo tipos={tratamentoAtual.tipo} />
            <Divider section />
            <p><strong>Motivo</strong></p>
            {tratamentoAtual.motivo || 'Não informado'}
            <Divider section />
            <ObservacoesMediunicas tratamento={tratamentoAtual} />
          </Modal.Content>
          <Modal.Actions>
            <Button color='green' onClick={this.handleClose} inverted>
              <Icon name='checkmark' /> OK
            </Button>
          </Modal.Actions>
        </Modal>
      </Security>
    );
  }
}

export default ModalResumoTratamento;
