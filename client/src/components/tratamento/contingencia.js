import React from 'react';
import _ from "lodash";
import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
import moment from 'moment';

const styles = StyleSheet.create({
  page: {
    flexDirection: 'row'
  },
  section: {
    margin: 10
  },
  listing: {
    fontWeight: 'bold',
    margin: 10,
    fontSize: 12
  },
  address: {
    marginLeft: 15,
    fontSize: 8
  }
});

const ContingenciaTratamentos = ({listagem, titulo}) => {

    const renderEndereco = (endereco) => {
        if (!endereco) {
          return "Endereço não informado";
        }
    
        let enderecoCompleto = "";
    
        const { logradouro, numero, bairro, cidade } = endereco;
    
        if (logradouro) {
          enderecoCompleto += logradouro;
        }
    
        if (numero) {
          enderecoCompleto += ", " + numero;
        }
    
        if (bairro) {
          enderecoCompleto += " - " + bairro;
        }

        if (cidade) {
            enderecoCompleto += " - " + cidade;
        }
    
        if (_.isEmpty(enderecoCompleto)) {
          return "Endereço não informado";
        }
    
        return enderecoCompleto;
    }

    const dateFormatter = (date) => {
        const parsed = moment.utc(date);
        return parsed.isValid() ? moment.utc(date).format("DD/MM/YYYY") : date;
    }

    return (
        <Document
         title="Contingencia de Tratamentos"
         author="Associacao Espirita Bezerra de Menezes">
            <Page size="A4" style={styles.page}>
                <View>
                    <Text style={styles.section}>{`${titulo} - ${dateFormatter(moment().startOf("day").utc())}`}</Text>
                    {
                        _.isEmpty(listagem) ?
                            <Text style={styles.listing}>Nao ha tratamentos</Text>
                            :
                            _.map(listagem, tratamento => {
                                return (
                                    <div key={tratamento.paciente._id}>
                                        <Text 
                                            style={styles.listing}
                                        >
                                            {tratamento.paciente.nome}
                                            
                                        </Text>
                                        <Text 
                                            style={styles.address}
                                        >
                                            { renderEndereco(tratamento.paciente.endereco) }
                                        </Text>
                                    </div>
                                )
                            })
                    }
                </View>
            </Page>
        </Document>
    );
}

export default ContingenciaTratamentos;