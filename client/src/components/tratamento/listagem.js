import _ from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Grid,
  Card,
  Icon,
  Label,
  Table,
  Menu,
  Dropdown,
  Radio,
  Button,
  Divider
} from "semantic-ui-react";
import asyncPoll from "react-async-poll";
import io from "socket.io-client";
import moment from "moment";

import Security from "../security/component";
import LinkUsuario from "../usuario/link";
import ProcessoTratamento from "./processo";

import { alertarErro, alertarSucesso } from "../../actions/alerta";

import { consultarUsuariosPorRole } from "../../actions/usuario";

import {
  consultarTratamentosVerificados,
  checkoutTratamento
} from "../../actions/tratamento";
import ContingenciaTratamentos from "./contingencia";
import { PDFDownloadLink } from "@react-pdf/renderer";

const TIPOS = {
  espiritual: {
    titulo: "Tratamentos Espirituais",
    idEstatistica: "tratamentosEspirituais",
    cor: "blue"
  },
  saude: {
    titulo: "Tratamentos de Saúde",
    idEstatistica: "tratamentosSaude",
    cor: "yellow"
  },
  distancia: {
    titulo: "Tratamentos a Distancia (Espiritual)",
    idEstatistica: "tratamentosDistancia",
    cor: "teal"
  },
  distancia_saude: {
    titulo: "Tratamentos a Distancia (Saúde)",
    idEstatistica: "tratamentosDistanciaSaude",
    cor: "orange"
  }
};

const STATUS = {
  nao_iniciado: {
    descricao: "Não iniciado",
    cor: "grey",
    icone: "info",
    acao: "Iniciar Tratamento"
  },
  em_andamento: {
    descricao: "Em andamento",
    cor: "yellow",
    icone: "wait",
    acao: "Concluir Tratamento"
  },
  em_andamento_outra_sala: {
    descricao: "Em outro tratamento",
    cor: "orange",
    icone: "lock",
    acao: "Concluir Tratamento"
  },
  aguardar_prioritario: {
    descricao: "Priorizar tratamento de saúde",
    cor: "orange",
    icone: "lock",
    acao: "Iniciar Tratamento"
  }
};

class ListagemTratamento extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tratamentosEmAndamento: [],
      pacientesEmEspera: []
    };
  }

  componentWillReceiveProps(nextProps) {
    const oldTipo = this.props.match.params.tipo;
    const tipo = nextProps.match.params.tipo;

    if (this.socket && oldTipo === tipo) {
      return;
    }

    this.configureSocket(tipo);
  }

  componentWillUnmount() {
    if (this.socket) {
      this.socket.disconnect();
    }
  }

  configureSocket = tipo => {
    this.socket = io(`${process.env.REACT_APP_API_WS_URL}/tratamentos`, {
      query: { sala: tipo, auth_token: localStorage.getItem('auth-token') },
      "force new connection": true
    });

    this.socket.on("exception", exception => {
      this.props.alertarErro(exception.message);
    });

    this.socket.on("connect_error", error => {
      this.props.alertarErro("Erro ao se conectar ao servidor", error);
    });

    this.socket.on(
      "sincronizarTratamentosEmAndamento",
      tratamentosEmAndamento => {
        this.setState({ tratamentosEmAndamento: tratamentosEmAndamento });
      }
    );

    this.socket.on("sincronizarPacientesEmEspera", pacientesEmEspera => {
      this.setState({ pacientesEmEspera });
    });

    this.socket.on("tratamentoConcluido", id => {
      navigator.vibrate(1000);
      const { listagem } = this.props.tratamentos;
      _.remove(listagem, tratamento => {
        return tratamento._id === id;
      });
    });
  };

  getUserID() {
    const { usuario } = this.props.autenticacao;
    return usuario ? usuario._id : null;
  }

  togglePacienteEmEspera(idPaciente) {
    this.socket.emit("togglePacienteEmEspera", idPaciente);
  }

  iniciarTratamento(id) {
    this.socket.emit("iniciarTratamento", id, this.getUserID(), status => {
      if (status !== "OK") {
        this.props.alertarErro("Erro ao iniciar tratamento");
      } else {
        this.props.consultarUsuariosPorRole('MEDIUM');
      }
    });
  }

  cancelarTratamento(id) {
    this.socket.emit("cancelarTratamento", id, status => {
      if (status !== "OK") {
        this.props.alertarErro("Erro ao cancelar tratamento");
      }
    });
  }

  concluirTratamento(id, tipo) {
    this.socket.emit("concluirTratamento", { id, tipo }, status => {
      if (status !== "OK") {
        this.props.alertarErro("Erro ao concluir tratamento");
      } else {
        this.props.socket.emit("atualizarEstatisticasTratamentos");
      }
    });
  }

  checkoutTratamento(id) {
    this.props.checkoutTratamento(id, () => {
      this.props.socket.emit("atualizarEstatisticasTratamentos");
      const tipo = this.props.match.params.tipo;
      this.props.consultarTratamentosVerificados(tipo);
    });
  }

  isTratamentoEmAndamento(tratamento) {
    const { tratamentosEmAndamento } = this.state;

    const ids = _.mapValues(_.flatten(_.values(tratamentosEmAndamento)), "id");

    return _.includes(ids, tratamento._id);
  }

  obterProcesso(tratamento, tipo) {
    const { agendamentos } = tratamento;

    const agendamentoDoDia = _.find(agendamentos, {
        data: moment().startOf("day").utc().toJSON()
    });

    if (!agendamentoDoDia || typeof agendamentoDoDia == "undefined") {
        return false;
    }

    const { processos } = agendamentoDoDia;

    if (processos.length === 1) {
      return false;
    }

    return _.find(processos, { tipo });
  }

  possuiTratamentoEspiritual(tipo, tratamento) {
    if (tipo !== "saude") {
      return false;
    }

    const processoEspiritual = this.obterProcesso(tratamento, "espiritual");

    if (!processoEspiritual) {
      return false;
    }

    return !processoEspiritual.concluido;
  }

  aguardarTratamentoPrioritario(tipo, tratamento) {
    if (tipo !== "espiritual") {
      return false;
    }

    const processoSaude = this.obterProcesso(tratamento, "saude");

    if (!processoSaude) {
      return false;
    }

    return !processoSaude.concluido;
  }

  renderEndereco(endereco) {
    if (!endereco) {
      return "Endereço não informado";
    }

    let enderecoCompleto = "";

    const { logradouro, numero, bairro } = endereco;

    if (logradouro) {
      enderecoCompleto += logradouro;
    }

    if (numero) {
      enderecoCompleto += ", " + numero;
    }

    if (bairro) {
      enderecoCompleto += " - " + bairro;
    }

    if (_.isEmpty(enderecoCompleto)) {
      return "Endereço não informado";
    }

    return enderecoCompleto;
  }

  renderListagem(tipo, listagem) {
    const { tratamentosEmAndamento, pacientesEmEspera } = this.state;
    const tratamentosEmAndamentoDaSala = tratamentosEmAndamento[tipo];

    return (
      <Table striped>
        <Table.Body>
          {_.isEmpty(listagem) && (
            <Table.Row>
              <Table.Cell>Não há tratamentos verificados até agora</Table.Cell>
            </Table.Row>
          )}
          {_.map(listagem, tratamento => {
            const aguardarTratamentoPrioritario = this.aguardarTratamentoPrioritario(
              tipo,
              tratamento
            );

            const possuiTratamentoEspiritual = this.possuiTratamentoEspiritual(
              tipo,
              tratamento
            );

            const isEmAndamento = this.isTratamentoEmAndamento(tratamento);

            const isEmAndamentoNaSala = _.find(tratamentosEmAndamentoDaSala, {
              id: tratamento._id
            });

            let status = "nao_iniciado";

            if (isEmAndamentoNaSala) {
              status = "em_andamento";
            } else if (isEmAndamento) {
              status = "em_andamento_outra_sala";
            } else if (aguardarTratamentoPrioritario) {
              status = "aguardar_prioritario";
            }

            const isHabilitadoConclusao =
              tipo !== "espiritual" && isEmAndamentoNaSala;

            const isADistancia = _.startsWith(tipo, "distancia");

            const idPaciente = tratamento.paciente._id;

            const pacienteEmEspera = pacientesEmEspera.find(
              pacienteEmEspera => {
                return idPaciente === pacienteEmEspera;
              }
            );

            return (
              <Table.Row warning={isEmAndamento} key={tratamento._id}>
                <Table.Cell>
                  <LinkUsuario usuario={tratamento.paciente} />
                </Table.Cell>
                {!isADistancia && (
                  <Table.Cell>
                    <Security allowedRoles="ADMIN,ORGANIZACAO,ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO">
                      <Label color={STATUS[status].cor} horizontal>
                        <Icon name={STATUS[status].icone} />
                        {STATUS[status].descricao}
                      </Label>
                    </Security>
                    {possuiTratamentoEspiritual && (
                      <Security allowedRoles="ADMIN,ORGANIZACAO,ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO">
                        <Label color={"orange"} horizontal>
                          <Icon name={"warning"} />
                          Passará também por Tratamento Espiritual
                        </Label>
                      </Security>
                    )}
                  </Table.Cell>
                )}
                {!isADistancia && (
                  <Table.Cell>
                    <Radio
                      toggle
                      label="Em sala de espera"
                      checked={pacienteEmEspera !== undefined}
                      onChange={() => this.togglePacienteEmEspera(idPaciente)}
                    />
                  </Table.Cell>
                )}
                {isADistancia && (
                  <Table.Cell>
                    {this.renderEndereco(tratamento.paciente.endereco)}
                  </Table.Cell>
                )}
                {isADistancia && (
                  <Table.Cell>
                    <strong>
                      {tratamento.paciente.endereco.cidade ||
                        "Cidade não informada"}
                    </strong>
                  </Table.Cell>
                )}
                <Table.Cell textAlign="right">
                  <Security allowedRoles="ADMIN,RECEPCAO,SECRETARIA,ORGANIZACAO,ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO">
                    <Menu stackable widths={1}>
                      <Dropdown item text={"Ações"}>
                        <Dropdown.Menu>
                          <Security allowedRoles="ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO">
                            <Dropdown.Item
                              onClick={() => {
                                if (!isEmAndamento) {
                                  return this.iniciarTratamento(tratamento._id);
                                } else {
                                  return this.concluirTratamento(
                                    tratamento._id,
                                    tipo
                                  );
                                }
                              }}
                              disabled={isEmAndamento && !isHabilitadoConclusao}
                            >
                              {STATUS[status].acao}
                            </Dropdown.Item>
                          </Security>
                          <Security allowedRoles="ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO">
                            <Dropdown.Item
                              onClick={() =>
                                this.cancelarTratamento(tratamento._id)
                              }
                              disabled={!isEmAndamentoNaSala}
                            >
                              Cancelar Tratamento
                            </Dropdown.Item>
                          </Security>
                          <Security allowedRoles="ADMIN,RECEPCAO,SECRETARIA,ORGANIZACAO,ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO">
                            <Dropdown.Item
                              disabled={isEmAndamento}
                              onClick={() =>
                                this.checkoutTratamento(tratamento._id)
                              }
                            >
                              Check-out
                            </Dropdown.Item>
                          </Security>
                        </Dropdown.Menu>
                      </Dropdown>
                    </Menu>
                  </Security>
                </Table.Cell>
              </Table.Row>
            );
          })}
        </Table.Body>
      </Table>
    );
  }

  concluirTratamentosEmAndamento = tipo => {
    const { tratamentosEmAndamento } = this.state;
    const tratamentosAConcluir = tratamentosEmAndamento[tipo];

    _.forEach(tratamentosAConcluir, tratamento => {
      this.concluirTratamento(tratamento.id, tratamento.tipo);
    });

    this.props.alertarSucesso("Tratamentos concluídos com sucesso");
  };

  render() {
    const { tipo } = this.props.match.params;

    if (!tipo) {
      return <div>Tipo de tratamento inválido</div>;
    }

    const { listagem } = this.props.tratamentos;
    const estatistica = this.props.estatisticas[TIPOS[tipo].idEstatistica];
    const { tratamentosEmAndamento } = this.state;

    const qtdTratamentosSaudeAndamento = tratamentosEmAndamento.saude
      ? tratamentosEmAndamento.saude.length
      : 0;

    return (
      <Grid padded>
        <Security allowedRoles="ADMIN,ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO">
          <div>
            <Grid.Row columns={2}>
              <PDFDownloadLink 
                 document={<ContingenciaTratamentos listagem={listagem} titulo={TIPOS[tipo].titulo} />} 
                 fileName={`tratamentos_${tipo}_${moment().format("DD/MM/YYYY")}.pdf`}
              >
                    {({ blob, url, loading, error }) => 
                        loading ? 'Gerando arquivo de contingência...' : 'Contingência (off-line)'
                    }
              </PDFDownloadLink>
              {tipo === "saude" && qtdTratamentosSaudeAndamento > 0 && (
                <div>
                  <Divider fitted />
                  <Button
                    compact
                    color={"yellow"}
                    onClick={() => this.concluirTratamentosEmAndamento(tipo)}
                  >
                    Concluir Tratamentos Iniciados (
                    {qtdTratamentosSaudeAndamento})
                  </Button>
                </div>
              )}
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                <ProcessoTratamento
                  tipo={tipo}
                  estatisticas={this.props.estatisticas}
                  socket={this.props.socket}
                />
              </Grid.Column>
            </Grid.Row>
          </div>
        </Security>
        <Grid.Row id="listagem">
          <Grid.Column>
            <Card fluid color="blue">
              <Card.Content>
                <Card.Header>
                  <Icon name="treatment" /> {TIPOS[tipo].titulo}
                  <Label color={TIPOS[tipo].cor}>{estatistica}</Label>
                </Card.Header>
                <Card.Description>
                  {this.renderListagem(tipo, listagem)}
                </Card.Description>
              </Card.Content>
            </Card>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    autenticacao: state.autenticacao,
    tratamentos: state.tratamentos
  };
}

const onPollInterval = props => {
  const tipo = props.match.params.tipo;
  return props.consultarTratamentosVerificados(tipo);
};

export default connect(
  mapStateToProps,
  {
    alertarErro,
    alertarSucesso,
    consultarTratamentosVerificados,
    checkoutTratamento,
    consultarUsuariosPorRole
  }
)(asyncPoll(5 * 1000, onPollInterval)(ListagemTratamento));
