import React, { Component } from 'react';

import { Button, Header, Icon, Modal, TextArea } from 'semantic-ui-react'

import Security from '../security/component';

class ModalObservacaoMediunica extends Component {

  state = { open: false, observacaoMediunica: '' };

  open = () => this.setState({ open: true });
  close = () => this.setState({ open: false });
  change = (e, { value }) => this.setState({ observacaoMediunica: value });

  confirm = () => {
    this.setState({ open: false, observacaoMediunica: '' });
    this.props.onConfirm(this.state.observacaoMediunica);
  }

  render() {
    const { open, observacaoMediunica } = this.state

    return (
      <Security allowedRoles="ADMIN,SUPORTE_ESCRIVAO">
        <Modal
          dimmer={true}
          open={open}
          onOpen={this.open}
          onClose={this.close}
          size='small'
          trigger={
            <Button color='blue' inverted>
              <Icon name='comment' /> Adicionar Observação Mediúnica
            </Button>
          }>

          <Modal.Header>
            <Header>Adicionar Observação Mediúnica</Header>
            <Header sub>
              Insira abaixo a observação mediúnica referente ao tratamento
            </Header>
          </Modal.Header>
          <Modal.Content>
            <TextArea
              autoHeight
              value={observacaoMediunica}
              onChange={this.change}
              style={{ width: '100%' }}
              placeholder='Escreva aqui...' />
          </Modal.Content>
          <Modal.Actions>
            <Button color='red' icon='cancel' content='Cancelar' onClick={this.close} />
            <Button color='green' icon='check' content='Confirmar' onClick={this.confirm} />
          </Modal.Actions>
        </Modal>
      </Security>
    )
  }
}

export default ModalObservacaoMediunica;
