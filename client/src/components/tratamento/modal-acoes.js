import _ from 'lodash';
import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Button, Header, Icon, Modal, Input } from 'semantic-ui-react'

import {
  encerrarRequisicaoAcao, encerrarTratamento,
  estenderTratamento, adicionarObservacaoMediunica,
  converterTratamentoParaDistancia, converterTratamentoParaPresencial
} from '../../actions/tratamento';

import ModalObservacaoMediunica from './modal-obs-mediunica';

class ModalExtensao extends Component {

  state = { open: false, semanas: this.props.semanas };

  open = () => this.setState({ open: true });
  close = () => this.setState({ open: false });
  change = (e, { value }) => this.setState({ semanas: value });
  confirm = () => this.props.onConfirm(this.state.semanas);

  render() {
    const { open, semanas } = this.state;

    return (
      <Modal
        open={open}
        onOpen={this.open}
        onClose={this.close}
        size='small'
        trigger={
          <Button color='green' inverted>
            <Icon name='forward' /> Estender / Reduzir
          </Button>
        }>

        <Modal.Header>
          <Header>Estender / Reduzir Tratamento</Header>
          <Header sub>
            As datas em que o paciente já passou por tratamento não entram no cálculo
          </Header>
        </Modal.Header>
        <Modal.Content>
          <Input
            label={{ basic: true, content: 'semanas' }}
            labelPosition='right'
            type="number"
            value={semanas}
            onChange={this.change}
            placeholder='Digite aqui ...' />
        </Modal.Content>
        <Modal.Actions>
          <Button color='red' icon='cancel' content='Cancelar' onClick={this.close} />
          <Button color='green' icon='check' content='Confirmar' onClick={this.confirm} />
        </Modal.Actions>
      </Modal>
    )
  }
}

class ModalAcoesTratamento extends Component {

  handleClose = (e) => {
    this.props.encerrarRequisicaoAcao();
  };

  adicionarObservacaoMediunica = (observacaoMediunica) => {
    const { id } = this.props.tratamentos;
    this.props.adicionarObservacaoMediunica(id, observacaoMediunica, () => {
      this.props.encerrarRequisicaoAcao();
    });
  };

  encerrar = () => {
    const { id } = this.props.tratamentos;
    this.props.encerrarTratamento(id, () => {
      this.props.socket.emit('atualizarEstatisticasTratamentos');
      this.props.encerrarRequisicaoAcao();
    });
  };

  extender = (semanas) => {
    if (!semanas) {
      semanas = 0;
    }

    const { id } = this.props.tratamentos;
    this.props.estenderTratamento(id, semanas, () => {
      this.props.socket.emit('atualizarEstatisticasTratamentos');
      this.props.encerrarRequisicaoAcao();
    });
  };

  converterParaDistancia = () => {
    const { id } = this.props.tratamentos;
    this.props.converterTratamentoParaDistancia(id, () => {
      this.props.socket.emit('atualizarEstatisticasTratamentos');
      this.props.encerrarRequisicaoAcao();
    });
  };

  converterParaPresencial = () => {
    const { id } = this.props.tratamentos;
    this.props.converterTratamentoParaPresencial(id, () => {
      this.props.socket.emit('atualizarEstatisticasTratamentos');
      this.props.encerrarRequisicaoAcao();
    });
  };

  isApenasDistancia(tipo) {
    return _.every(tipo, (tipo) => {
      return _.startsWith(tipo, 'distancia');
    });
  }

  render() {
    const isTratamentoDistancia = 
        !this.props.tratamentoAtual ? false : this.isApenasDistancia(this.props.tratamentoAtual.tipo);

    return (
      <Modal
        open={!_.isEmpty(this.props.tratamentos.mensagem)}
        onClose={this.handleClose}
        basic
        size='small'>

        <Header icon='attention' content='Atenção' />
        <Modal.Content>
          <h3>{this.props.tratamentos.mensagem}</h3>
          <p>O que deseja fazer com o tratamento?</p>
        </Modal.Content>
        <Modal.Actions>
          <ModalObservacaoMediunica onConfirm={this.adicionarObservacaoMediunica} />
          <ModalExtensao onConfirm={this.extender}
                         semanas={this.props.tratamentos.agendamentosNecessarios} />
          { isTratamentoDistancia ? 
            <Button color='orange' onClick={this.converterParaPresencial} inverted>
                <Icon name='exchange' /> Convertar para Presencial
            </Button>
            :
            <Button color='purple' onClick={this.converterParaDistancia} inverted>
                <Icon name='exchange' /> Convertar para Distancia
            </Button> 
          }
          <Button color='red' onClick={this.encerrar} inverted>
            <Icon name='remove' /> Encerrar
          </Button>
        </Modal.Actions>
      </Modal>
    )
  }
}

function mapStateToProps(state) {
  return { tratamentos : state.tratamentos };
}

export default connect(mapStateToProps, { encerrarRequisicaoAcao, encerrarTratamento, estenderTratamento, adicionarObservacaoMediunica, converterTratamentoParaDistancia, converterTratamentoParaPresencial })(ModalAcoesTratamento);
