import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { Icon, Grid, Step, Button } from 'semantic-ui-react';

import {
  checkinTratamento, checkoutTratamento, requisitarAcao
} from '../../actions/tratamento';

import { agendarAtendimento } from '../../actions/atendimento';

import Tipo from './tipo';
import ObservacoesMediunicas from './observacoes-mediunicas';
import Security from '../security/component';

class TratamentoAtual extends Component {

  dateFormatter(date) {
    const parsed = moment.utc(date);
    return parsed.isValid() ? moment(date).utc().format("DD/MM/YYYY") : date;
  }

  isHoje(data) {
    return moment(data).utc().isSame(moment().startOf('day').utc(), 'day');
  }

  isDataFutura(data) {
    return moment(data).utc().isAfter(moment().startOf('day').utc(), 'day');
  }

  isDataPassada(data) {
    return moment(data).utc().isBefore(moment().startOf('day').utc(), 'day');
  }

  isApenasDistancia(tipo) {
    return _.every(tipo, (tipo) => {
      return _.startsWith(tipo, 'distancia');
    });
  }

  checkinTratamentoAtual = () => {
    const { dadosAdicionais } = this.props.consultaUsuarios;
    this.props.checkinTratamento(dadosAdicionais.tratamentoAtual._id, () => {
      this.props.socket.emit('atualizarEstatisticasTratamentos');
    });
  }

  checkoutTratamentoAtual = () => {
    const { dadosAdicionais } = this.props.consultaUsuarios;
    this.props.checkoutTratamento(dadosAdicionais.tratamentoAtual._id, () => {
      this.props.socket.emit('atualizarEstatisticasTratamentos');
    });
  }

  acao(agendamento) {
    const { dadosAdicionais } = this.props.consultaUsuarios;

    if (this.isHoje(agendamento.data)
          && dadosAdicionais.checkinHabilitado) {
      return this.checkinTratamentoAtual();
    } else if (this.isHoje(agendamento.data)
                  && dadosAdicionais.checkoutHabilitado) {
      return this.checkoutTratamentoAtual();
    }
  }

  exibirAcoes = () => {
    const { dadosAdicionais: { tratamentoAtual } } = this.props.consultaUsuarios;
    this.props.requisitarAcao(tratamentoAtual._id, 'Tratamento Atual');
  }

  agendarRetorno(paciente, atendente) {
    this.props.agendarAtendimento(paciente, true, atendente, () => {
      this.props.socket.emit('atualizarEstatisticasAtendimento');
    });
  }

  renderDescricaoAgendamento(agendamento) {
    const { dadosAdicionais } = this.props.consultaUsuarios;

    if (this.isHoje(agendamento.data)
          && dadosAdicionais.checkinHabilitado) {
      return 'Clique para Check-in';
    } else if (this.isHoje(agendamento.data)
                  && dadosAdicionais.checkoutHabilitado) {
      return 'Clique para Check-out';
    } else if (dadosAdicionais.isApenasDistancia) {
      return 'À Distância';
    } else {
      return this.isDataFutura(agendamento.data) ?
         'Agendado' : agendamento.compareceu ?
         'Compareceu': 'Não compareceu';
    }
  }

  renderAgendamentos(agendamentos, paciente, tipo, atendente) {

    const ultimoAgendamento = _.maxBy(agendamentos, 'data');
    const dataRetorno = moment(ultimoAgendamento.data).add(7, 'days');
    const nomeAtendente = atendente ? atendente.nome : undefined;

    return (
      <Step.Group vertical size={'large'}>
        {
          _.map(agendamentos, (agendamento) => {
            return (
              <Step key={agendamento._id}
                    active={this.isHoje(agendamento.data)}
                    completed={agendamento.compareceu || (this.isApenasDistancia(tipo) && this.isDataPassada(agendamento.data))}
                    onClick={() => this.acao(agendamento)}
                    disabled={!this.isHoje(agendamento.data)}>
                <Icon name={
                  this.isDataPassada(agendamento.data)
                    ? (this.isApenasDistancia(tipo) ? 'check' :
                    !agendamento.compareceu ? 'remove' : 'check') : 'calendar outline'} />
                <Step.Content>
                  <Step.Title>{this.dateFormatter(agendamento.data)}</Step.Title>
                  <Step.Description>
                    {this.renderDescricaoAgendamento(agendamento)}
                  </Step.Description>
                </Step.Content>
              </Step>
            )
        })
      }
      {
        !this.isApenasDistancia(tipo) &&
        <Step key={dataRetorno}
              active={this.isHoje(dataRetorno) || this.isDataPassada(dataRetorno)}
              onClick={() => this.agendarRetorno(paciente, nomeAtendente)}
              disabled={!this.isHoje(dataRetorno)}>
          <Icon name={'repeat'} />
          <Step.Content>
            <Step.Title>{this.dateFormatter(dataRetorno)}</Step.Title>
            <Step.Description>
              <strong>Retorno</strong>
            </Step.Description>
          </Step.Content>
        </Step>
      }
      </Step.Group>
    );
  }

  render() {
    const { dadosAdicionais: { tratamentoAtual } } = this.props.consultaUsuarios;

    if (!tratamentoAtual) {
      return <div>Não há tratamento em andamento</div>
    }

    const {
      tipo, atendente, agendamentos, motivo, intercessor, paciente
    } = tratamentoAtual;

    return (
      <div>
        <Grid>
          {atendente ?
            <Grid.Row>
              <Grid.Column>
                <p><strong>Atendido por:</strong> {atendente.nome}</p>
              </Grid.Column>
            </Grid.Row> : <p>Tratamento não cadastrado por um atendente</p>
          }
          <Grid.Row>
            <Grid.Column>
              <Tipo tipos={tipo} />
            </Grid.Column>
          </Grid.Row>
          <Security allowedRoles="ADMIN,ATENDIMENTO">
            <Grid.Row>
              <Grid.Column>
                <strong>Motivo: </strong> {motivo || 'Não informado'}
              </Grid.Column>
            </Grid.Row>
          </Security>
          <Security allowedRoles="ADMIN,ATENDIMENTO">
            <Grid.Row>
              <Grid.Column>
                <strong>A pedido de: </strong> {intercessor || 'Não informado'}
              </Grid.Column>
            </Grid.Row>
          </Security>
          <Security allowedRoles="ADMIN,ATENDIMENTO">
            <Grid.Row>
              <Grid.Column>
                <ObservacoesMediunicas tratamento={tratamentoAtual} />
              </Grid.Column>
            </Grid.Row>
          </Security>
          <Grid.Row>
            <Grid.Column>
              {this.renderAgendamentos(agendamentos, paciente, tipo, atendente)}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Button fluid onClick={this.exibirAcoes}>Ações sobre o tratamento</Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { consultaUsuarios : state.consultaUsuarios };
}

export default connect(mapStateToProps, { agendarAtendimento, checkinTratamento, checkoutTratamento, requisitarAcao })(TratamentoAtual);
