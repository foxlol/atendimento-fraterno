import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid, Progress, Icon, Card } from "semantic-ui-react";
import io from "socket.io-client";
import asyncPoll from "react-async-poll";

import { alertar, alertarSucesso, alertarErro } from "../../actions/alerta";

import { consultarTratamentosVerificadosProcesso } from "../../actions/tratamento";

import InputTratamento from "./input";

const TIPO = {
  espiritual: {
    descricao: "Espiritual",
    contagem: "tratamentosEspirituais"
  },
  saude: {
    descricao: "de Saúde",
    contagem: "tratamentosSaude"
  },
  distancia: {
    descricao: "a Distancia (Espiritual)",
    contagem: "tratamentosDistancia"
  },
  distancia_saude: {
    descricao: "a Distancia (Saúde)",
    contagem: "tratamentosDistanciaSaude"
  }
};

class ProcessoTratamento extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tratamento: undefined,
      isLoading: true,
      isProcessoFinalizado: false,
      tratamentosEmAndamento: []
    };
  }

  componentWillReceiveProps(nextProps) {
    const { tipo } = nextProps;
    const oldTipo = this.props.tipo;

    if (!this.socket || oldTipo !== tipo) {
      this.setState({ tratamento: undefined });
      this.configureSocket(tipo);
    }

    const contagem = nextProps.estatisticas[TIPO[tipo].contagem];
    this.setState({ isProcessoFinalizado: !contagem || contagem === 0 });
  }

  componentWillUnmount() {
    if (this.socket) {
      this.socket.disconnect();
    }
  }

  configureSocket = tipo => {
    if (this.socket) {
      this.socket.disconnect();
    }

    this.socket = io(`${process.env.REACT_APP_API_WS_URL}/tratamentos`, {
      query: { sala: tipo, auth_token: localStorage.getItem('auth-token') },
      "force new connection": true
    });

    this.socket.on("exception", exception => {
      this.props.alertarErro(exception.message);
    });

    this.socket.on("connect_error", error => {
      this.props.alertarErro("Erro ao se conectar ao servidor", error);
    });

    this.socket.on(
      "sincronizarTratamentosEmAndamento",
      tratamentosEmAndamento => {
        this.setState({ tratamentosEmAndamento: tratamentosEmAndamento[tipo] });
      }
    );

    this.socket.on("tratamentoCancelado", tratamento => {
      this.props.alertar("Tratamento cancelado");
      this.setState({ tratamento: undefined, isLoading: true });
    });

    this.socket.on("tratamentoConcluido", tratamento => {
      this.setState({ tratamento: undefined, isLoading: true });
    });

    this.socket.on("carregarTratamento", tratamento => {
      navigator.vibrate(1000);
      this.setState({ tratamento, isLoading: false });
    });
  };

  cancelarTratamento = id => {
    this.socket.emit("cancelarTratamento", id, status => {
      if (status === "OK") {
        this.setState({ tratamento: undefined, isLoading: true });
      } else {
        this.props.alertarErro("Erro ao cancelar tratamento");
      }
    });
  };

  concluirTratamento = tratamento => {
    this.socket.emit("concluirTratamento", tratamento, status => {
      if (status === "OK") {
        this.props.alertarSucesso("Tratamento concluído com sucesso");
        this.props.socket.emit("atualizarEstatisticasTratamentos");
        this.atualizarListagem();
        this.setState({ tratamento: undefined, isLoading: true });
      } else {
        this.props.alertarErro("Erro ao concluir tratamento");
      }
    });
  };

  atualizarListagem() {
    const { tipo } = this.props;
    this.props.consultarTratamentosVerificadosProcesso(tipo);
  }

  renderLoading() {
    const { isLoading } = this.state;

    if (!isLoading) {
      return <div />;
    }

    return (
      <Progress
        label="Aguardando início de tratamento ..."
        percent={100}
        indicating
      />
    );
  }

  render() {
    const { tratamento, isProcessoFinalizado } = this.state;
    const { tipo, autenticacao } = this.props;

    if (!tipo || tipo === "saude") {
      return <div />;
    }

    return (
      <Grid padded>
        <Grid.Row>
          <Grid.Column>
            <Card fluid color="blue">
              <Card.Content>
                <Card.Header>
                  <Icon name="treatment" />
                  {`Processo de Tratamento ${TIPO[tipo].descricao}`}
                </Card.Header>
                <Card.Description>
                  {isProcessoFinalizado ? (
                    <p>Não há tratamentos</p>
                  ) : (
                    <div>
                      {this.renderLoading()}
                      <InputTratamento
                        tratamento={tratamento}
                        socket={this.socket}
                        onConcluido={this.concluirTratamento}
                        onCancelado={this.cancelarTratamento}
                        usuarioLogado={autenticacao.usuario}
                      />
                    </div>
                  )}
                </Card.Description>
              </Card.Content>
            </Card>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    autenticacao: state.autenticacao,
    tratamentosProcesso: state.tratamentosProcesso
  };
}

const onPollInterval = props => {
  const { tipo } = props;
  props.consultarTratamentosVerificadosProcesso(tipo);
};

export default connect(
  mapStateToProps,
  {
    consultarTratamentosVerificadosProcesso,
    alertar,
    alertarSucesso,
    alertarErro
  }
)(asyncPoll(15 * 1000, onPollInterval)(ProcessoTratamento));
