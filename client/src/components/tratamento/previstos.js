import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Statistic, Grid, Header, Button } from 'semantic-ui-react';
import asyncPoll from 'react-async-poll';

import { 
  consultarTratamentosPrevistosDia, 
  consultarContagemTratamentosPrevistosDia 
} from '../../actions/tratamento';

import {
  checkinTratamento
} from '../../actions/tratamento';

import { agendarAtendimento } from '../../actions/atendimento';

import LinkUsuario from '../usuario/link';
import ModalAcoesTratamento from '../tratamento/modal-acoes';

class TratamentosPrevistos extends Component {

  componentWillMount() {
    this.props.consultarContagemTratamentosPrevistosDia();
    this.props.consultarTratamentosPrevistosDia();
  }

  checkinTratamento = (id) => {
    this.props.checkinTratamento(id, () => {
      this.props.socket.emit('atualizarEstatisticasTratamentos');
    });
  }

  agendarRetorno(paciente, atendente) {
    this.props.agendarAtendimento(paciente, true, atendente, () => {
      this.props.socket.emit('atualizarEstatisticasAtendimento');
    });
  }

  render() {
    const { contagemPrevistos, previstos } = this.props.tratamentos;

    if (!contagemPrevistos || !previstos) {
      return null;
    }

    const { espiritual, saude, distancia, distancia_saude } = contagemPrevistos;

    return (
      <div>
        <ModalAcoesTratamento socket={this.props.socket} />
        <Grid centered columns={4} padded>
          <Grid.Row>
            <Header>Tratamentos Previstos para Hoje</Header>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Statistic size={'tiny'}>
                <Statistic.Value>{espiritual ? espiritual.contagem : 0}</Statistic.Value>
                <Statistic.Label>Espirituais</Statistic.Label>
              </Statistic>
            </Grid.Column>
            <Grid.Column>
              <Statistic size={'tiny'}>
                <Statistic.Value>{saude ? saude.contagem : 0}</Statistic.Value>
                <Statistic.Label>Saúde</Statistic.Label>
              </Statistic>
            </Grid.Column>
            <Grid.Column>
              <Statistic size={'tiny'}>
                <Statistic.Value>{distancia ? distancia.contagem : 0}</Statistic.Value>
                <Statistic.Label>Distância (Espiritual)</Statistic.Label>
              </Statistic>
            </Grid.Column>
            <Grid.Column>
              <Statistic size={'tiny'}>
                <Statistic.Value>{distancia_saude ? distancia_saude.contagem : 0}</Statistic.Value>
                <Statistic.Label>Distância (Saúde)</Statistic.Label>
              </Statistic>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Grid centered columns={2} padded>
          <Grid.Row>
            <Header>Pacientes</Header>
          </Grid.Row>
            {
              previstos.length > 0 ? previstos.map(previsto => {
                return (
                  <Grid.Row key={previsto.idTratamento} columns={2} textAlign={'left'}>
                    <Grid.Column textAlign={'left'}> 
                      <LinkUsuario usuario={{_id: previsto.idPaciente, nome: previsto.nomePaciente}} />
                    </Grid.Column>
                    <Grid.Column>
                      {
                        previsto.retorno ? 
                        <Button basic color='red' onClick={() => this.agendarRetorno(previsto.idPaciente, previsto.nomeAtendente)}>Retorno</Button> :
                        <Button basic color='green' onClick={() => this.checkinTratamento(previsto.idTratamento)}>Check-in</Button>
                      }
                    </Grid.Column>
                  </Grid.Row>
                );
              }) : <p>Nenhum paciente previsto para hoje</p>
            }
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { tratamentos: state.tratamentos };
}

const onPollInterval = (props) => {
  props.consultarContagemTratamentosPrevistosDia();
	return props.consultarTratamentosPrevistosDia();
};

export default connect(mapStateToProps, { agendarAtendimento, consultarTratamentosPrevistosDia, consultarContagemTratamentosPrevistosDia, checkinTratamento })(asyncPoll(5 * 1000, onPollInterval)(TratamentosPrevistos));
