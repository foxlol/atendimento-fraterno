import _ from 'lodash';
import React, { Component } from 'react';
import { Accordion, List, Icon } from 'semantic-ui-react';

export default class ObservacaoMdiunica extends Component {

  state = { activeIndex: -1 }

  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index

    this.setState({ activeIndex: newIndex })
  }

  render() {
    const { observacoesMediunicas } = this.props.tratamento;

    if (_.isEmpty(observacoesMediunicas)) {
      return <div>Sem observações mediúnicas</div>
    }

    const dataSeparator = ' - ';
    const comunicacaoSeparator = '[c]';
    const { activeIndex } = this.state;

    return (
      <Accordion fluid>
        <Accordion.Title active={activeIndex === 0} index={0} onClick={this.handleClick}>
          <Icon name='dropdown' />
          <strong>Exibir Observações Mediúnicas</strong>
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 0}>
          <List divided relaxed>
            {
              _.map(observacoesMediunicas, (observacaoMediunica, index) => {

                const data =
                  observacaoMediunica.substring(
                    0, observacaoMediunica.indexOf(dataSeparator));

                observacaoMediunica =
                  observacaoMediunica.substring(
                    observacaoMediunica.indexOf(dataSeparator) + dataSeparator.length);

                const observacaoMediunicaDesagrupada =
                  _.split(observacaoMediunica, comunicacaoSeparator);

                  return (
                    <List.Item key={index}>
                      <List.Icon name='comments' size='large' verticalAlign='middle' />
                      <List.Content>
                        <List.Header>{data}</List.Header>
                      </List.Content>
                      <List.List>
                        {
                          _.map(observacaoMediunicaDesagrupada, (omd, index) => {
                            return (
                              <List.Item key={index}>
                                <List.Icon name='comment' size='small' verticalAlign='middle' />
                                <List.Content>
                                  <List.Description>{omd}</List.Description>
                                </List.Content>
                              </List.Item>
                            )
                          })
                        }
                      </List.List>
                    </List.Item>
                  )
              })
            }
          </List>
      </Accordion.Content>
      </Accordion>
    );
  }
};
