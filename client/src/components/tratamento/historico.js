import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Card, Divider } from 'semantic-ui-react';

import {
  adicionarObservacaoMediunica
} from '../../actions/tratamento';

import Tipo from './tipo';
import ObservacoesMediunicas from './observacoes-mediunicas';
import ModalObservacaoMediunica from './modal-obs-mediunica';

class HistoricoTratamentos extends Component {

  obterDataInicioTratamento(tratamento) {
    const primeiroAgendamento = _.head(tratamento.agendamentos);
    return moment(primeiroAgendamento.data).format('DD/MM/YYYY');
  }

  obterDataFinalTratamento(tratamento) {
    const ultimoAgendamento = _.last(tratamento.agendamentos);
    return moment(ultimoAgendamento.data).format('DD/MM/YYYY');
  }

  adicionarObservacaoMediunica = (observacaoMediunica, idTratamento) => {
    this.props.adicionarObservacaoMediunica(idTratamento, observacaoMediunica);
  };

  renderHistoricos(historicoTratamentos) {
    return _.map(historicoTratamentos, (tratamento) => {
      const dataInicio = this.obterDataInicioTratamento(tratamento);
      const dataFinal = this.obterDataFinalTratamento(tratamento);
      const motivo = tratamento.motivo || 'Não informado';
      const intercessor = tratamento.intercessor;

      return (
        <Card
          key={tratamento._id} fluid centered raised
          header={`${dataInicio} a ${dataFinal}`}
          meta={<Tipo tipos={tratamento.tipo} />}
          description={
            <div>
              <Divider hidden />
              <strong>Motivo: </strong> {motivo}
              {
                intercessor &&
                <div>
                  <Divider hidden />
                  <strong>A pedido de: </strong> {intercessor}
                </div>
              }
              <Divider hidden />
              <ObservacoesMediunicas tratamento={tratamento} />
              <Divider hidden />
              <ModalObservacaoMediunica
                onConfirm={
                  (observacaoMediunica) =>
                    this.adicionarObservacaoMediunica(
                      observacaoMediunica, tratamento._id
                    )
                } />
            </div>
          } />
      );
    });
  }

  render() {
    const { dadosAdicionais: { historicoTratamentos } } = this.props.consultaUsuarios;

    if (_.isEmpty(historicoTratamentos)) {
      return <div>Não há histórico de tratamentos</div>
    }

    return (
      <div>
        {this.renderHistoricos(historicoTratamentos)}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { consultaUsuarios : state.consultaUsuarios };
}

export default connect(mapStateToProps, { adicionarObservacaoMediunica })(HistoricoTratamentos);
