import React from 'react';

import Security from './component';

export default (allowedRoles) => {
  return WrappedComponent => {
    return (
      <Security {...allowedRoles} />
    );
  }
}
