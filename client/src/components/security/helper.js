import _ from 'lodash';

export function isUserAllowed(userRoles, allowedRoles) {
  if (_.isEmpty(allowedRoles) || _.isEmpty(userRoles)) {
    return false;
  }

  const allowedRolesArray =
    _.split(allowedRoles.replace(/ /g, '').toUpperCase(), ',');

  return (_.intersection(userRoles, allowedRolesArray).length > 0);
}
