import React, { Component } from 'react';
import { connect } from 'react-redux';

import { isUserAllowed } from './helper';

class ComponentSecurity extends Component {
    render() {
      const { roles, isAutenticado } = this.props.autenticacao;
      const allowedRoles = this.props.allowedRoles;
      const authenticated = this.props.authenticated;
      const notAuthenticated = this.props.notAuthenticated;

      if (notAuthenticated) {
        if (!isAutenticado) {
          return {...this.props.children};
        } else {
          return <div></div>
        }
      }

      if (!isAutenticado) {
        return <div></div>
      }

      if (authenticated) {
        return {...this.props.children};
      }

      if (isUserAllowed(roles, allowedRoles)) {
        return {...this.props.children}
      } else {
        return <div></div>
      }
    }
}

function mapStateToProps(state) {
  return { autenticacao: state.autenticacao }
};

export default connect(mapStateToProps)(ComponentSecurity);
