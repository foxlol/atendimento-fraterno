import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import asyncPoll from 'react-async-poll';
import PropTypes from 'prop-types';

import {
  Grid, Card, Icon, Table, Menu, Label, Dropdown
} from 'semantic-ui-react';

import LinkUsuario from '../usuario/link';

import {
  consultarAtendimentos, iniciarAtendimento, removerAtendimento
} from '../../actions/atendimento';

import {
  consultarUsuario
} from '../../actions/usuario';

class Atendimentos extends Component {

  iniciarAtendimento(idAtendimento, idUsuario) {
    const { usuario: usuarioLogado } = this.props.autenticacao;

    this.props.iniciarAtendimento(idAtendimento, usuarioLogado._id, () => {
      this.props.consultarUsuario(idUsuario, () => {
        this.context.router.history.push('/usuarios/cadastro');
      });
    });
  }

  continuarAtendimento(idUsuario) {
    this.props.consultarUsuario(idUsuario, () => {
      this.context.router.history.push('/usuarios/cadastro');
    });
  }

  concluirAtendimento(id) {
    this.props.removerAtendimento(id, () => {
      this.props.socket.emit('atualizarEstatisticasAtendimento');
    });
  }

  renderAtendimentos(atendimentos) {
    const { isAtendente, usuario: usuarioLogado } = this.props.autenticacao;

    return (
      <Table striped>
        <Table.Body>
          {
            _.isEmpty(atendimentos) &&
            <Table.Row>
              <Table.Cell>
                Não há atendimentos agendados
              </Table.Cell>
            </Table.Row>
          }
          {
            _.map(atendimentos, (atendimento) => {
              const emAtendimento = !_.isEmpty(atendimento.atendente);

              const continuarAtendimento =
                emAtendimento
                  && isAtendente
                  && (usuarioLogado._id === atendimento.atendente._id);

              let nomeAtendente = '';

              if (emAtendimento) {
                const { nome: nomeCompletoAtendente } = atendimento.atendente;

                if (nomeCompletoAtendente) {
                  nomeAtendente = _.words(nomeCompletoAtendente)[0];
                }
              }

              let mensagemPreferenciaRetorno;

              if (atendimento.isRetorno
                    && atendimento.atendentePreferencial) {

                const nomeAtendentePreferencial =
                  _.words(atendimento.atendentePreferencial)[0];

                mensagemPreferenciaRetorno =
                  `(preferencialmente com ${nomeAtendentePreferencial})`;
              }

              return (
                <Table.Row key={atendimento._id}>
                  <Table.Cell>
                    <LinkUsuario usuario={atendimento.usuario} />
                  </Table.Cell>
                  <Table.Cell>
                    {atendimento.isRetorno &&
                      <Label color='orange' horizontal>
                        Retorno {mensagemPreferenciaRetorno}
                      </Label>
                    }
                  </Table.Cell>
                  <Table.Cell textAlign='right'>
                    <Menu stackable>
                      <Dropdown
                        item
                        text={
                          emAtendimento ?
                            `Em atendimento com ${nomeAtendente}` : 'Aguardando'
                        }>
                        <Dropdown.Menu>
                          {!emAtendimento && isAtendente &&
                            <Dropdown.Item
                              text='Iniciar'
                              onClick={
                                () => this.iniciarAtendimento(
                                  atendimento._id, atendimento.usuario._id
                                )} />
                          }
                          {continuarAtendimento &&
                            <Dropdown.Item
                              text='Continuar'
                              onClick={
                                () => this.continuarAtendimento(
                                  atendimento.usuario._id
                                )} />
                          }
                          <Dropdown.Item
                            text='Concluir'
                            onClick={() => this.concluirAtendimento(atendimento._id)} />
                        </Dropdown.Menu>
                      </Dropdown>
                    </Menu>
                  </Table.Cell>
                </Table.Row>
              )
            })
          }
        </Table.Body>
      </Table>
    );
  }

  render() {
    const { resultado } = this.props.atendimentos;
    const { atendimentos } = this.props.estatisticas;

    return (
      <Grid padded>
        <Grid.Column>
          <Card fluid color='blue'>
            <Card.Content>
              <Card.Header>
                <Icon name='comments' /> Atendimentos
                <Label color="red">{atendimentos}</Label>
              </Card.Header>
              <Card.Description>
                { this.renderAtendimentos(resultado) }
              </Card.Description>
            </Card.Content>
          </Card>
        </Grid.Column>
      </Grid>
    );
  }
}

Atendimentos.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.object.isRequired,
  })
};

function mapStateToProps(state) {
  return {
    atendimentos : state.atendimentos,
    autenticacao: state.autenticacao
  };
}

const onPollInterval = (props) => {
	return props.consultarAtendimentos();
};

export default connect(mapStateToProps, { consultarAtendimentos, iniciarAtendimento, removerAtendimento, consultarUsuario })(asyncPoll(10 * 1000, onPollInterval)(Atendimentos));
