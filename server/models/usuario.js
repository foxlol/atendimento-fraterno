const _ = require('lodash');
const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const capitalize = require('capitalize');

const generoEnum = {
  values: ['M', 'F'], message: '`{VALUE}` não é um gênero válido'
}

const roleEnum = {
  values: [
    'ADMIN', 'RECEPCAO', 'SECRETARIA', 'ATENDIMENTO', 'ORGANIZACAO',
    'ENCAMINHAMENTO', 'SUPORTE_ESCRIVAO', 'SUPORTE', 'PACIENTE', 'MEDIUM'
  ], message: '`{VALUE}` não é uma permissão válida'
}

const usuarioSchema = new mongoose.Schema({
  username: {
    type: String,
    lowercase: true,
    required: true,
    trim: true,
    minlength: 4,
    unique: true
  },
  senha: {
    type: String,
    required: true,
    minlength: 6
  },
  telefone: {
    type: String,
    minlength: 7
  },
  email: {
    type: String,
    trim: true,
    unique: true,
    minlength: 6,
    sparse: true,
    validate: {
      validator: value => validator.isEmail(value),
      message: '{VALUE} não é um e-mail válido'
    }
  },
  nome: {
    type: String,
    required: true,
    trim: true
  },
  endereco: {
    logradouro: {
      type: String,
      minlength: 3,
      trim: true
    },
    numero: {
      type: String,
      minlength: 1,
      trim: true
    },
    bairro: {
      type: String,
      minlength: 1,
      trim: true
    },
    cidade: {
      type: String,
      minlength: 3,
      trim: true
    }
  },
  dataNascimento: {
    type: Date
  },
  genero: {
    type: String,
    enum: generoEnum
  },
  deficiente: {
    type: Boolean,
    default: false
  },
  roles: {
      type: [{type: String}],
      enum: roleEnum
  },
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }]
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

usuarioSchema.virtual('endereco.completo').get(function () {
  let enderecoCompleto = '';

  if (!this.endereco) {
    return enderecoCompleto;
  }

  let { logradouro, numero, bairro, cidade } = this.endereco;

  if (logradouro) {
    enderecoCompleto += logradouro;
  }

  if (numero) {
    enderecoCompleto += ', ' + numero;
  }

  if (bairro) {
    enderecoCompleto += ' - ' + bairro;
  }

  if (cidade) {
    enderecoCompleto += ', ' + cidade;
  }

  return enderecoCompleto;
});

usuarioSchema.methods.toJSON = function() {
  const usuario = this;
  const usuarioObject = usuario.toObject();

  return _.pick(usuarioObject,
    [
      '_id', 'username', 'nome', 'genero', 'telefone',
      'deficiente', 'endereco', 'dataNascimento', 'email', 'roles'
    ]
  );
};

usuarioSchema.methods.generateAuthToken = function() {
  const usuario = this;
  const access = 'auth';

  const token = jwt.sign({
    _id: usuario._id.toHexString(),
    roles: usuario.roles,
    access
  }, process.env.JWT_SECRET).toString();

  usuario.tokens.push({access, token});

  return usuario.save().then(() => { return token });
};

usuarioSchema.methods.removeToken = function(token) {
  const usuario = this;

  return usuario.updateOne({
    $pull: {
      tokens: {token}
    }
  });
};

usuarioSchema.statics.hasRoleAtendimento = function(idUsuario) {
  const Usuario = this;

  return Usuario.countDocuments({_id: idUsuario, roles: 'ATENDIMENTO'}).then((count) => {
    return count > 0;
  });
};

usuarioSchema.statics.findByToken = function(token) {
  const Usuario = this;
  var decoded = undefined;

  try {
    decoded = jwt.verify(token, process.env.JWT_SECRET);
  } catch (e) {
    return Promise.reject(e);
  }

  return Usuario.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  });
};

usuarioSchema.statics.findByCredentials = function(username, senha) {
  const Usuario = this;

  return Usuario.findOne({username}).then((usuario) => {
    if (!usuario) {
      return Promise.reject('Usuário ou senha inválida');
    }

    return new Promise((resolve, reject) => {
      bcrypt.compare(senha, usuario.senha, (err, result) => {
        if (result) {
          resolve(usuario);
        } else {
          reject('Usuário ou senha inválida');
        }
      });
    });
  });
};

usuarioSchema.statics.hashPassword = function(password) {
  return bcrypt.hashSync(password, 10);
}

usuarioSchema.pre('save', function(next) {
  const usuario = this;

  if (usuario.isNew && !_.includes(usuario.roles, 'PACIENTE')) {
    usuario.roles.push('PACIENTE');
  }

  usuario.nome = capitalize.words(_.toLower(usuario.nome));

  if (usuario.endereco) {
    if (usuario.endereco.logradouro) {
      usuario.endereco.logradouro =
        capitalize.words(_.toLower(usuario.endereco.logradouro));
    }

    if (usuario.endereco.bairro) {
      usuario.endereco.bairro =
        capitalize.words(_.toLower(usuario.endereco.bairro));
    }
  }

  if (usuario.isModified('senha')) {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(usuario.senha, salt, (err, hash) => {
        usuario.senha = hash;
        next();
      });
    });
  } else {
    next();
  }
});

const Usuario = mongoose.model('Usuario', usuarioSchema);

module.exports = { Usuario };
