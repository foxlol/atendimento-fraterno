const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require('moment');
const idvalidator = require('mongoose-id-validator');
const { Atendimento } = require('./atendimento');

const tipoEnum = {
  values: ['espiritual', 'saude', 'distancia', 'distancia_saude'],
  message: '`{VALUE}` não é um tipo de tratamento válido'
};

const tratamentoSchema = new mongoose.Schema({
  paciente: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Usuario',
    required: true
  },
  atendente: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Usuario'
  },
  tipo: {
    type: [{type:String}],
    enum: tipoEnum,
    required: true
  },
  agendamentos: [{
    data: {
      type: Date,
      required: true
    },
    compareceu: {
      type: Boolean,
      default: false
    },
    dataCheckIn: {
      type: Date,
      required: false
    },
    processos: [{
      tipo: {
        type: String,
        enum: tipoEnum,
        required: true
      },
      concluido: {
        type: Boolean,
        required: true
      }
    }]
  }],
  motivo: {
    type: String
  },
  intercessor: {
    type: String
  },
  observacoesMediunicas: {
    type: [{type:String}]
  },
  concluido: {
    type: Boolean,
    default: false
  }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

tratamentoSchema.plugin(idvalidator, {
  message : 'ID de {PATH} inexistente'
});

tratamentoSchema.methods.encerrar = function(tipo) {
  const tratamento = this;

  const agendamento = _.find(tratamento.agendamentos, (agendamento) => {
    return moment(agendamento.data).startOf("day").utc().isSame(moment().startOf("day").utc(), 'day');
  });

  const processo = _.find(agendamento.processos, (processo) => {
    return processo.tipo === tipo;
  });

  processo.concluido = true;

  const isApenasDistancia = _.every(tratamento.tipo, (tipoTratamento) => {
    return _.startsWith(tipoTratamento, 'distancia');
  });

  if (isApenasDistancia) {
    agendamento.compareceu = true;
  }

  const haProcessosPendentes =
    _.some(agendamento.processos, ['concluido', false]);

  if (!haProcessosPendentes
        && tratamento.constructor.isUltimoDia(tratamento.agendamentos)
        && isApenasDistancia) {

    tratamento.concluido = true;
  }

  return tratamento.save().then((tratamento) => tratamento);
};

tratamentoSchema.statics.obterTodos = function(filtro) {
  const Tratamento = this;
  let query = {};

  if (filtro) {
    const { tipo, paciente, concluido } = filtro;

    if (!_.isEmpty(tipo)) {
      query['tipo'] = {'$in': tipo};
    }

    if (paciente) {
      query['paciente'] = paciente;
    }

    if (!_.isUndefined(concluido)) {
      query['concluido'] = concluido;
    }
  }

  return Tratamento.find(query)
                   .sort({'concluido': 1, 'agendamentos.data': -1})
                   .populate('paciente');
}

tratamentoSchema.statics.checkin = function(id) {
  const Tratamento = this;

  return Tratamento.findOneAndUpdate({
    _id: id,
    concluido: false,
    agendamentos: {
      $elemMatch: { 'compareceu': false, 'data': moment().startOf("day").utc() }
    }
  }, {
  'agendamentos.$.compareceu' : true,
  'agendamentos.$.dataCheckIn' : moment().utc().toDate()
  }, {
    new: true,
    runValidators: true
  }).then((tratamento) => {
    if (!tratamento) {
      return Promise.reject(
        'Check-in já efetuado ou tratamento fora de período de check-in'
      );
    }

    return tratamento;
  });
};

tratamentoSchema.statics.cadastrarObservacaoMediunica = function(id, observacaoMediunica) {
  const Tratamento = this;

  if (observacaoMediunica) {
    const data = moment().format('DD/MM/YYYY');
    observacaoMediunica = `${data} - ${observacaoMediunica}`;
  }

  return Tratamento.findByIdAndUpdate(
    { _id: id },
    { $push: { observacoesMediunicas: observacaoMediunica } },
    { new: true, runValidators: true });
};

tratamentoSchema.statics.isUltimoDia = function(agendamentos) {
  const ultimoAgendamento = _.maxBy(agendamentos, 'data');
  return moment(ultimoAgendamento.data).startOf("day").utc().isSame(moment().startOf("day").utc(), 'day');
};

tratamentoSchema.statics.obterPorPaciente = function(idUsuario) {
  const Tratamento = this;
  return Tratamento.find({ 'paciente': idUsuario });
};

tratamentoSchema.statics.obterHistoricoPaciente = function(idUsuario) {
  const Tratamento = this;
  return Tratamento.find(
    { 'paciente': idUsuario, 'concluido': true }
  ).sort({'agendamentos.data': -1});
};

tratamentoSchema.statics.obterAtual = function(idUsuario) {
  const Tratamento = this;
  return Tratamento.findOne(
    { 'paciente': idUsuario, 'concluido': false }
  ).populate('atendente');
};

tratamentoSchema.statics.paraVerificacaoHoje = function(idUsuario) {
  const Tratamento = this;

  return Tratamento.findOne({
    paciente: idUsuario,
    concluido: false,
    agendamentos: {
      $elemMatch: {
        'data': moment().startOf('day').utc(), 'processos.concluido': false
      }
    }
  });
};

tratamentoSchema.statics.validarTipo = function(tipo) {
  const Tratamento = this;

  return _.includes(tipoEnum.values, tipo);
};

tratamentoSchema.statics.obterVerificadosDoDia = function(tipo) {
  const Tratamento = this;
  const data = moment().startOf("day").utc().toDate();
  const isTratamentoDistancia = _.startsWith(tipo, 'distancia');

  var queryProcessos = {
    $elemMatch: {
      'tipo': tipo, concluido: false
    }
  }

  var queryAgendamentos = {
    $elemMatch: {
      'compareceu': true, 'data': data, 'processos': queryProcessos
    }
  };

  if (isTratamentoDistancia) {
    queryAgendamentos = {
      $elemMatch: { 'data': data, 'processos': queryProcessos }
    };
  }

  return Tratamento.find({
    'concluido': false,
    'tipo': tipo,
    agendamentos: queryAgendamentos
  }).populate('paciente');
};

tratamentoSchema.statics.obterContagemVerificadosDoDia = function(tipo) {
  const Tratamento = this;
  const data = moment().startOf("day").utc().toDate();
  const isTratamentoDistancia = _.startsWith(tipo, 'distancia');

  var queryProcessos = {
    $elemMatch: {
      'tipo': tipo, concluido: false
    }
  }

  var queryAgendamentos = {
    $elemMatch: { 'compareceu': true, 'data': data, 'processos': queryProcessos }
  };

  if (isTratamentoDistancia) {
    queryAgendamentos = {
      $elemMatch: { 'data': data, 'processos': queryProcessos }
    };
  }

  return Tratamento.countDocuments({
    'concluido': false,
    'tipo': tipo,
    agendamentos: queryAgendamentos
  });
};

tratamentoSchema.statics.obterRetornosPrevistosDoDia = function() {
  const Tratamento = this;
  const dataUltimoAgendamento = moment().startOf("day").utc().subtract(7, 'days').toDate();

  const queryProcessos = {
    $elemMatch: { 
      'concluido': true
    }
  }

  const queryAgendamentos = {
    $elemMatch: {
      'compareceu': true, 'data': dataUltimoAgendamento, 'processos': queryProcessos
    }
  };

  const query = {
    'concluido': false,
    'tipo': { $in: ['espiritual', 'saude'] },
    agendamentos: queryAgendamentos
  }

  return Tratamento.aggregate([
    {
      $match: {
        'concluido': false,
        'tipo': { $in: ['espiritual', 'saude'] },
        agendamentos: queryAgendamentos
      }
    },
    { $unwind: '$agendamentos' },
    { 
      $group: {
        _id: "$_id",
        dataUltimoAgendamento: { $max: "$agendamentos.data" }
      }
    },
    {
      $match: {
        'dataUltimoAgendamento': dataUltimoAgendamento
      }
    },
    {
      $project: { _id: 1, dataUltimoAgendamento: 1 }
    }
  ]).then(result => {
    return Tratamento.find({_id: { $in: result }}).populate("paciente atendente");
  });
}

tratamentoSchema.statics.obterPrevistosDoDia = function() {
  const Tratamento = this;
  const data = moment().startOf("day").utc().toDate();

  const queryProcessos = {
    $elemMatch: { 
      'concluido': false,
      'tipo': { $in: ['espiritual', 'saude'] }
    }
  }

  const queryAgendamentos = {
    $elemMatch: {
      'compareceu': false, 'data': data, 'processos': queryProcessos
    }
  };

  const query = {
    'concluido': false,
    agendamentos: queryAgendamentos
  }

  return Tratamento.find(query).populate('paciente');
};

tratamentoSchema.statics.obterContagemPrevistosDoDia = function() {
  const Tratamento = this;

  const data = moment().startOf("day").utc().toDate();

  var queryProcessos = {
    $elemMatch: {
      concluido: false
    }
  }

  var queryAgendamentos = {
    $elemMatch: { 'data': data, 'processos': queryProcessos }
  };

  return Tratamento.aggregate([
    {
      $match: {
        concluido: false,
        agendamentos: queryAgendamentos
      }
    },
    { $unwind:"$tipo" },
    { $group: { _id: "$tipo", count: { $sum: 1 } } },
    { $sort: { count: -1 } },
    { $project : { _id: 0, tipo: '$_id', contagem: '$count' } }
  ]);
};

tratamentoSchema.statics.extrairTratamentosMultiplos = function(tratamentos) {
  if (!tratamentos) {
    return undefined;
  }

  return _.remove(tratamentos, (tratamento) => {
    const { agendamentos } = tratamento;

    const agendamentoDoDia = _.find(
      agendamentos, { 'data': moment().startOf("day").utc().toDate() }
    );

    const { processos } = agendamentoDoDia;

    return processos.length > 1;
  });
}

tratamentoSchema.statics.ordenarTratamentosDistancia = function(tratamentos) {
  if (!tratamentos) {
    return undefined;
  }

  return _.orderBy(tratamentos, 'paciente.nome', 'asc');
};

tratamentoSchema.statics.ordenarTratamentos = function(tratamentos) {
  if (!tratamentos) {
    return undefined;
  }

  const diaAtual = moment().startOf('day').utc().toDate();

  const tratamentosOrdenados = _.orderBy(tratamentos, (tratamento) => {
    const agendamento = _.filter(tratamento.agendamentos, (agendamento) => {
      return moment(agendamento.data).toDate().getTime() === diaAtual.getTime();
    })[0];
    
    return _.get(agendamento, 'dataCheckIn');
  }, ['asc']);

  return tratamentosOrdenados;

  // const deficientes = _.remove(tratamentos, (tratamento) => {
  //   return tratamento.paciente.deficiente;
  // });

  // const semDataNascimento = _.remove(tratamentos, (tratamento) => {
  //   return !tratamento.paciente.dataNascimento;
  // });

  // const idosos = _.remove(tratamentos, (tratamento) => {
  //   const idade =  moment().diff(tratamento.paciente.dataNascimento, 'years');
  //   return idade >= 60;
  // });

  // const criancas = _.remove(tratamentos, (tratamento) => {
  //   const idade =  moment().diff(tratamento.paciente.dataNascimento, 'years');
  //   return idade < 12;
  // });

  // const moramFora = _.remove(tratamentos, (tratamento) => {
  //   if (!tratamento.paciente.endereco) {
  //     return false;
  //   }

  //   const { cidade } = tratamento.paciente.endereco;

  //   return cidade !== "São José do Rio Pardo - SP";
  // });

  // return _.concat(
  //   deficientes,
  //   _.orderBy(idosos, 'paciente.dataNascimento', 'asc'),
  //   _.orderBy(criancas, 'paciente.dataNascimento', 'asc'),
  //   _.orderBy(moramFora, 'paciente.dataNascimento', 'asc'),
  //   _.orderBy(tratamentos, 'paciente.dataNascimento', 'asc'),
  //   semDataNascimento
  // );
};

tratamentoSchema.statics.converterParaDistancia = async function(id) {
    const tratamento = await Tratamento.findById(id);
 
    const tiposConvertidos = [];

    const proximosAgendamentos = _.filter(tratamento.agendamentos, (agendamento) => {
        return moment(agendamento.data).startOf("day").utc().isSameOrAfter(moment().startOf("day").utc(), 'day');
    });

    if (_.includes(tratamento.tipo, 'espiritual')) {
        tiposConvertidos.push('distancia');
    }

    if (_.includes(tratamento.tipo, 'saude')) {
        tiposConvertidos.push('distancia_saude');
    }

    if (tiposConvertidos.length > 0) {
        tratamento.tipo = tiposConvertidos;

        _.forEach(proximosAgendamentos, (agendamento) => {
            _.forEach(agendamento.processos, (processo) => {
                if (processo.tipo == 'espiritual') {
                    processo.tipo = 'distancia';
                } else if (processo.tipo == 'saude') {
                    processo.tipo = 'distancia_saude';
                } 
            });
        });
    }

    return tratamento.save();
};

tratamentoSchema.statics.converterParaPresencial = async function(id) {
    const tratamento = await Tratamento.findById(id);
 
    const tiposConvertidos = [];

    const proximosAgendamentos = _.filter(tratamento.agendamentos, (agendamento) => {
        return moment(agendamento.data).startOf("day").utc().isSameOrAfter(moment().startOf("day").utc(), 'day');
    });

    if (_.includes(tratamento.tipo, 'distancia')) {
        tiposConvertidos.push('espiritual');
    }

    if (_.includes(tratamento.tipo, 'distancia_saude')) {
        tiposConvertidos.push('saude');
    }

    if (tiposConvertidos.length > 0) {
        tratamento.tipo = tiposConvertidos;

        _.forEach(proximosAgendamentos, (agendamento) => {
            _.forEach(agendamento.processos, (processo) => {
                if (processo.tipo == 'distancia') {
                    processo.tipo = 'espiritual';
                } else if (processo.tipo == 'distancia_saude') {
                    processo.tipo = 'saude';
                } 
            });
        });
    }

    return tratamento.save();
};

tratamentoSchema.pre('save', function(next) {
  const tratamento = this;

  if (tratamento.isModified('agendamentos')) {
    _.forEach(tratamento.agendamentos, (agendamento) => {
      agendamento.data = moment(agendamento.data).startOf('day').utc();
    });
  }

  next();
});

const Tratamento = mongoose.model('Tratamento', tratamentoSchema);

module.exports = { Tratamento };
