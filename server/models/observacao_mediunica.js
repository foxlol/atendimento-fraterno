const mongoose = require("mongoose");
const idvalidator = require("mongoose-id-validator");

const observacaoMediunicaSchema = new mongoose.Schema({
  sala: {
    type: String,
    required: true
  },
  conteudo: {
    type: String,
    default: ""
  },
  medium: {
    type: String,
    default: ""
  },
  atendente: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Usuario"
  }
});

observacaoMediunicaSchema.plugin(idvalidator, {
  message: "ID de {PATH} inexistente"
});

observacaoMediunicaSchema.statics.obterTodas = function(sala) {
  const ObservacaoMediunica = this;

  return ObservacaoMediunica.find({ sala })
    .sort("index")
    .populate("atendente");
};

observacaoMediunicaSchema.statics.removerTodas = function(sala) {
  const ObservacaoMediunica = this;

  return ObservacaoMediunica.deleteMany({ sala });
};

observacaoMediunicaSchema.statics.atualizar = function(id, conteudo, medium) {
  const ObservacaoMediunica = this;

  return ObservacaoMediunica.findOneAndUpdate(
    {
      _id: id
    },
    {
      conteudo,
      medium
    }
  );
};

const ObservacaoMediunica = mongoose.model(
  "ObservacaoMediunica",
  observacaoMediunicaSchema
);

module.exports = { ObservacaoMediunica };
