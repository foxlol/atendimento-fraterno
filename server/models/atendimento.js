const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const { Usuario } = require('./usuario');

const atendimentoSchema = new mongoose.Schema({
  usuario: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Usuario',
    required: true
  },
  atendente: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Usuario',
    validate: {
      isAsync: true,
      validator: function(idUsuario, callback) {
        Usuario.hasRoleAtendimento(idUsuario).then((result) => {
          callback(result, 'Você não possui permissão para atendimento');
        }).catch((error) => callback(false, 'Você não possui permissão para atendimento'));
      }
    }
  },
  isRetorno: {
    type: Boolean,
    default: false
  },
  atendentePreferencial: {
    type: String
  }
}, {
  timestamps: true
});

atendimentoSchema.index({usuario: 1}, {unique: true});
// atendimentoSchema.index({atendente: 1}, {unique: true, sparse: true});

atendimentoSchema.statics.obterTodos = function() {
  const Atendimento = this;

  return Atendimento.find({}).sort('createdAt').populate('usuario').populate('atendente');
}

atendimentoSchema.statics.obterQuantidadePendentes = function() {
  const Atendimento = this;

  return Atendimento.estimatedDocumentCount({atendente: { $exists: false }});
}

atendimentoSchema.statics.obterContagem = function() {
  const Atendimento = this;

  return Atendimento.estimatedDocumentCount();
}

atendimentoSchema.statics.obterDoUsuario = function(usuario) {
  const Atendimento = this;

  return Atendimento.findOne({usuario}).then((atendimento) => {
    return atendimento;
  });
};

atendimentoSchema.plugin(idvalidator, {
  message : 'ID de {PATH} inexistente'
});

atendimentoSchema.post('save', function(error, doc, next) {
  if (error.name === 'MongoError' && error.code === 11000) {
    next(new Error('Já existe um atendimento agendado para este paciente ou atendente'));
  } else {
    next(error);
  }
});

const Atendimento = mongoose.model('Atendimento', atendimentoSchema);

module.exports = { Atendimento };
