const handleDuplicateKey = function(error, res, next) {
  if (error.name === 'MongoError' && error.code === 11000) {
    next(new Error(error.errmsg));
  } else {
    next();
  }
};

module.exports = { handleDuplicateKey };
