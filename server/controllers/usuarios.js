const _ = require("lodash");
const express = require("express");
const passwordGenerator = require("generate-password");
const capitalize = require("capitalize");
const { ObjectID } = require("mongodb");
const moment = require("moment");

const { autenticar, autorizar } = require("../middlewares/seguranca");
const { Usuario } = require("../models/usuario");
const { Tratamento } = require("../models/tratamento");
const { Atendimento } = require("../models/atendimento");
const {
  makeCaseAndAccentInsensitiveRegExpPattern
} = require("../helpers/string");

const router = express.Router();

router.get(
  "/",
  [autenticar, autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO")],
  (req, res) => {
    var query = {};
    const nome = req.query.nome;
    const role = req.query.role;

    if (nome) {
      _.set(
        query,
        "nome",
        makeCaseAndAccentInsensitiveRegExpPattern(nome)
      );
    }

    if (role) {
      _.set(
        query,
        "roles",
        role
      );
    }

    Usuario.find(query)
      .sort("nome")
      .then(usuarios => {
        if (_.isEmpty(usuarios)) {
          return res.status(404).send();
        }

        res.send({ usuarios });
      })
      .catch(error => res.status(400).send(error));
  }
);

router.get(
  "/:id",
  [autenticar, autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO")],
  (req, res) => {
    const id = req.params.id;

    if (!ObjectID.isValid(id)) {
      return res.status(400).send({ error: "ID de usuário inválido" });
    }

    Usuario.findById(id)
      .then(usuario => {
        if (!usuario) {
          return res.status(404).send({ error: "Usuário não encontrado" });
        }

        res.send({ usuario });
      })
      .catch(error => res.status(400).send(error));
  }
);

router.get(
  "/:id/dados-adicionais",
  [
    autenticar,
    autorizar(
      "ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO,ORGANIZACAO,ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO"
    )
  ],
  async (req, res) => {
    const idUsuario = req.params.id;

    let dadosAdicionais = {
      checkinHabilitado: false,
      checkoutHabilitado: false,
      atendimentoAtual: undefined,
      tratamentoAtual: undefined,
      historicoTratamentos: undefined,
      isApenasDistancia: false
    };

    try {
      const tratamentoParaVerificacaoHoje = await Tratamento.paraVerificacaoHoje(
        idUsuario
      );

      if (tratamentoParaVerificacaoHoje) {
        const agendamentoDeHoje = _.find(
          tratamentoParaVerificacaoHoje.agendamentos,
          agendamento => {
            return moment(agendamento.data).startOf('day').utc().isSame(moment().startOf('day').utc(), "day");
          }
        );

        const isApenasDistancia = _.every(
          tratamentoParaVerificacaoHoje.tipo,
          tipo => {
            return _.startsWith(tipo, "distancia");
          }
        );

        if (!isApenasDistancia) {
          dadosAdicionais.checkinHabilitado = !agendamentoDeHoje.compareceu;
          dadosAdicionais.checkoutHabilitado = agendamentoDeHoje.compareceu;
        }
      }

      const tratamentoAtual = await Tratamento.obterAtual(idUsuario);

      if (tratamentoAtual) {
        dadosAdicionais.tratamentoAtual = tratamentoAtual;

        dadosAdicionais.isApenasDistancia = _.every(
          tratamentoAtual.tipo,
          tipo => {
            return _.startsWith(tipo, "distancia");
          }
        );
      }

      const historicoTratamentos = await Tratamento.obterHistoricoPaciente(
        idUsuario
      );

      if (historicoTratamentos) {
        dadosAdicionais.historicoTratamentos = historicoTratamentos;
      }

      const atendimento = await Atendimento.obterDoUsuario(idUsuario);

      if (atendimento) {
        dadosAdicionais.atendimentoAtual = { ...atendimento._doc };
      }

      return res.send({ dadosAdicionais });
    } catch (error) {
      res.status(400).send({ error: error.message });
    }
  }
);

router.post("/", (req, res) => {
  const body = _.pick(req.body, [
    "username",
    "nome",
    "endereco",
    "genero",
    "telefone",
    "dataNascimento",
    "senha",
    "deficiente",
    "email",
    "roles"
  ]);

  if (!body.username) {
    body.username = generateUsernameByName(body.nome);
  }

  if (!body.senha) {
    body.senha = passwordGenerator.generate({
      length: 6,
      numbers: true
    });
  }

  const usuario = new Usuario(body);

  usuario
    .save()
    .then(() => {
      return usuario.generateAuthToken();
    })
    .then(token => {
      res.header("x-auth", token).send({ usuario });
    })
    .catch(error => {
      if (error.code === 11000) {
        error.message = "Já existe um usuário cadastrado com este nome";
      }
      res.status(400).send({ error: error.message });
    });
});

router.patch(
  "/:id",
  [autenticar, autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO")],
  (req, res) => {
    const id = req.params.id;

    if (!ObjectID.isValid(id)) {
      return res.status(400).send({ error: "ID de usuário inválido" });
    }

    const body = _.pick(req.body, [
      "nome",
      "username",
      "endereco",
      "dataNascimento",
      "deficiente",
      "email",
      "genero",
      "roles",
      "telefone",
      "senha"
    ]);

    if (!body.senha) {
      _.unset(body, "senha");
    } else {
      body.senha = Usuario.hashPassword(body.senha);
    }

    if (body.nome) {
      body.nome = capitalize.words(_.toLower(body.nome));
    }

    if (body.username) {
      body.username = generateUsernameByName(body.username);
    } else {
      body.username = generateUsernameByName(body.nome);
    }

    if (body.endereco) {
      if (body.endereco.logradouro) {
        body.endereco.logradouro = capitalize.words(
          _.toLower(body.endereco.logradouro)
        );
      }

      if (body.endereco.bairro) {
        body.endereco.bairro = capitalize.words(
          _.toLower(body.endereco.bairro)
        );
      }
    }

    Usuario.findByIdAndUpdate(
      id,
      { $set: body },
      { new: true, runValidators: true }
    )
      .then(usuario => {
        if (!usuario) {
          return res.status(404).send({ error: "Usuário não encontrado" });
        }

        res.send({ usuario });
      })
      .catch(error => res.status(400).send({ error: error.message }));
  }
);

router.post("/login", (req, res) => {
  const body = _.pick(req.body, ["username", "senha"]);

  Usuario.findByCredentials(body.username, body.senha)
    .then(usuario => {
      return usuario.generateAuthToken().then(token => {
        res.header("x-auth", token).send({ usuario });
      });
    })
    .catch(error => res.status(400).send({ error }));
});

router.delete("/token", autenticar, (req, res) => {
  req.usuario.removeToken(req.token).then(
    () => {
      res.status(200).send();
    },
    () => {
      res.status(400).send();
    }
  );
});

router.delete("/:id", [autenticar, autorizar("ADMIN")], async (req, res) => {
  const id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(400).send({ error: "ID de usuário inválido" });
  }

  try {
    await Atendimento.deleteMany({ $or: [{ usuario: id }, { atendente: id }] });
    await Tratamento.deleteMany({ paciente: id });
    await Usuario.deleteOne({ _id: id });

    return res.status(200).send();
  } catch (error) {
    res.status(400).send({ error });
  }
});

const generateUsernameByName = name => {
  if (name) {
    return _.lowerCase(_.deburr(_.trim(name))).replace(/ /g, "");
  }
};

module.exports = router;
