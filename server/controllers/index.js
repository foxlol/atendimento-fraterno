const express = require('express');

const router = express.Router();

router.use('/usuarios', require('./usuarios'));
router.use('/atendimentos', require('./atendimentos'));
router.use('/tratamentos', require('./tratamentos'));
router.use('/localidades', require('./localidades'));
router.use('/estatisticas', require('./estatisticas'));

module.exports = router;
