const _ = require("lodash");
const express = require("express");
var moment = require("moment");
const { ObjectID } = require("mongodb");

const { Tratamento } = require("../models/tratamento");
const { Atendimento } = require("../models/atendimento");
const { autenticar, autorizar } = require("../middlewares/seguranca");

const {
  validarCheckin,
  validarCheckout,
  validarCadastro
} = require("../middlewares/tratamento");

const router = express.Router();

router.post(
  "/todos",
  [autenticar, autorizar("ADMIN,ATENDIMENTO")],
  (req, res) => {
    return Tratamento.obterTodos(req.body)
      .then(tratamentos => {
        res.send({ tratamentos });
      })
      .catch(error => res.status(400).send({ error: error.message }));
  }
);

router.get(
  "/contagem/previstos/dia",
  [autenticar, autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO")],
  (req, res) => {
    return Tratamento.obterContagemPrevistosDoDia()
      .then(result => {
        res.send({ previstos: _.keyBy(result, "tipo") });
      })
      .catch(error => res.status(400).send({ error: error.message }));
  }
);

router.get(
  "/previstos/dia",
  [autenticar, autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO")],
  async (req, res) => {
    try {
      let tratamentos = await Tratamento.obterPrevistosDoDia();
      let retornos = await Tratamento.obterRetornosPrevistosDoDia();

      tratamentos = tratamentos.map(tratamento => {
        return {
          idTratamento: tratamento._id,
          idPaciente: tratamento.paciente._id,
          nomePaciente: tratamento.paciente.nome
        };
      });

      retornos = retornos.map(async tratamento => {
        const atendimento = await Atendimento.findOne({
          usuario: tratamento.paciente._id
        });

        if (atendimento) {
          return undefined;
        }

        return {
          idTratamento: tratamento._id,
          idPaciente: tratamento.paciente._id,
          nomePaciente: tratamento.paciente.nome,
          retorno: true,
          nomeAtendente: tratamento.atendente
            ? tratamento.atendente.nome
            : undefined
        };
      });

      retornos = await Promise.all(retornos);

      const resultados = retornos
        .filter(tratamento => {
          return tratamento != null;
        })
        .concat(tratamentos)
        .sort((a, b) => {
          if (a.nomePaciente > b.nomePaciente) {
            return 1;
          }
          if (a.nomePaciente < b.nomePaciente) {
            return -1;
          }

          return 0;
        });

      res.send(resultados);
    } catch (error) {
      res.status(400).send({ error: error.message });
    }
  }
);

router.post(
  "/",
  [
    autenticar,
    autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO"),
    validarCadastro
  ],
  (req, res) => {
    const tratamento = new Tratamento({
      paciente: req.tratamento.paciente,
      atendente: req.tratamento.atendente,
      motivo: req.tratamento.motivo,
      intercessor: req.tratamento.intercessor,
      tipo: req.tratamento.tipo,
      agendamentos: criarAgendamentos(
        req.tratamento.datasAgendamentos,
        req.tratamento.tipo
      )
    });

    tratamento
      .save()
      .then(tratamento => {
        res.send({ tratamento });
      })
      .catch(error => res.status(400).send({ error: error.message }));
  }
);

router.patch(
  "/:id/observacao-mediunica",
  [autenticar, autorizar("ADMIN,SUPORTE_ESCRIVAO")],
  (req, res) => {
    const id = req.params.id;
    const observacaoMediunica = _.trim(req.body.observacaoMediunica);

    if (!ObjectID.isValid(id)) {
      return res.status(400).send({ error: "ID de tratamento inválido" });
    }

    if (_.isEmpty(observacaoMediunica)) {
      return res.status(400).send({ error: "Observação Mediúnica inválida" });
    }

    Tratamento.cadastrarObservacaoMediunica(id, observacaoMediunica)
      .then(tratamento => {
        if (!tratamento) {
          return res.status(404).send({ error: "Tratamento não encontrado" });
        }

        return res.send({ tratamento });
      })
      .catch(error => {
        res.status(400).send({ error });
      });
  }
);

router.patch(
  "/:id/checkin",
  [
    autenticar,
    autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO"),
    validarCheckin
  ],
  (req, res) => {
    const id = req.params.id;

    if (!ObjectID.isValid(id)) {
      return res.status(400).send({ error: "ID de tratamento inválido" });
    }

    Tratamento.checkin(id)
      .then(tratamento => {
        res.send({ tratamento });
      })
      .catch(error => res.status(400).send({ error }));
  }
);

router.patch(
  "/:id/checkout",
  [
    autenticar,
    autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO,ORGANIZACAO"),
    validarCheckout
  ],
  (req, res) => {
    const id = req.params.id;

    if (!ObjectID.isValid(id)) {
      return res.status(400).send({ error: "ID de tratamento inválido" });
    }

    Tratamento.findOneAndUpdate(
      {
        _id: id,
        concluido: false,
        agendamentos: {
          $elemMatch: { compareceu: true, data: moment().startOf("day").utc() }
        }
      },
      {
        "agendamentos.$.compareceu": false
      },
      {
        new: true,
        runValidators: true
      }
    )
      .then(tratamento => {
        if (!tratamento) {
          return res
            .status(404)
            .send({ error: "Tratamento fora de período de check-out" });
        }

        return res.send({ tratamento });
      })
      .catch(error => res.status(400).send({ error }));
  }
);

router.patch(
  "/:id/encerrar",
  [autenticar, autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO")],
  (req, res) => {
    const id = req.params.id;

    if (!ObjectID.isValid(id)) {
      return res.status(400).send({ error: "ID de tratamento inválido" });
    }

    return Tratamento.findOneAndUpdate(
      {
        _id: id,
        concluido: false
      },
      { concluido: true },
      {
        new: true,
        runValidators: true
      }
    )
      .then(tratamento => {
        if (!tratamento) {
          return res.status(404).send({
            error: "Tratamento inexistente ou já encerrado"
          });
        }

        return res.send({ tratamento });
      })
      .catch(error => res.status(400).send({ error }));
  }
);

router.patch(
  "/:id/estender/semanas/:semanas",
  [autenticar, autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO")],
  (req, res) => {
    const id = req.params.id;
    var semanas = Math.trunc(req.params.semanas);

    if (!ObjectID.isValid(id)) {
      return res.status(400).send({ error: "ID de tratamento inválido" });
    }

    if (_.isUndefined(semanas) || _.isNaN(semanas) || semanas <= 0) {
      return res.status(400).send({ error: "Número de semanas inválido" });
    }

    Tratamento.findById(id)
      .then(tratamento => {
        if (!tratamento) {
          return res.status(404).send({ error: "Tratamento não encontrado" });
        }

        processarExtensaoAgendamentos(tratamento, semanas);

        return tratamento.save().then(tratamento => {
          res.send({ tratamento });
        });
      })
      .catch(error => res.status(400).send({ error }));
  }
);

router.get(
  "/:tipo/verificados",
  [
    autenticar,
    autorizar(
      "ADMIN,SECRETARIA,RECEPCAO,ATENDIMENTO,ORGANIZACAO,ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO"
    )
  ],
  (req, res) => {
    const tipo = req.params.tipo;

    if (!Tratamento.validarTipo(tipo)) {
      return res.status(400).send({ error: "Tipo de tratamento inválido" });
    }

    return Tratamento.obterVerificadosDoDia(tipo)
      .then(tratamentos => {
        if (_.isEmpty(tratamentos)) {
          return res.send({ tratamentos: [] });
        }

        const isApenasDistancia = _.startsWith(tipo, "distancia");

        if (isApenasDistancia) {
          return res.send({
            tratamentos: Tratamento.ordenarTratamentosDistancia(tratamentos)
          });
        }

        const tratamentosMultiplosOrdenados = Tratamento.ordenarTratamentos(
          Tratamento.extrairTratamentosMultiplos(tratamentos)
        );

        const tratamentosSingularesOrdenados = Tratamento.ordenarTratamentos(
          tratamentos
        );

        let tratamentosOrdenados;

        if (tipo === "saude") {
          tratamentosOrdenados = _.concat(
            tratamentosMultiplosOrdenados,
            tratamentosSingularesOrdenados
          );
        } else {
          tratamentosOrdenados = _.concat(
            tratamentosSingularesOrdenados,
            tratamentosMultiplosOrdenados
          );
        }

        return res.send({ tratamentos: tratamentosOrdenados });
      })
      .catch(error => res.status(400).send({ error }));
  }
);

router.get(
  "/paciente/:id",
  [autenticar, autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO")],
  (req, res) => {
    const id = req.params.id;

    if (!ObjectID.isValid(id)) {
      return res.status(400).send({ error: "ID de paciente inválido" });
    }

    Tratamento.obterPorPaciente(id)
      .then(tratamentos => {
        if (_.isEmpty(tratamentos)) {
          return res
            .status(404)
            .send({ error: "Nenhum tratamento encontrado" });
        }

        res.send({ tratamentos });
      })
      .catch(error => res.status(400).send({ error }));
  }
);

router.patch(
    "/:id/converter-distancia",
    [autenticar, autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO")],
    (req, res) => {
      const id = req.params.id;
  
      if (!ObjectID.isValid(id)) {
        return res.status(400).send({ error: "ID de tratamento inválido" });
      }
  
      return Tratamento.converterParaDistancia(id)
        .then(tratamento => {  
          return res.send({ tratamento });
        }).catch(error => res.status(400).send({ error }));
    }
);

router.patch(
    "/:id/converter-presencial",
    [autenticar, autorizar("ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO")],
    (req, res) => {
      const id = req.params.id;
  
      if (!ObjectID.isValid(id)) {
        return res.status(400).send({ error: "ID de tratamento inválido" });
      }
  
      return Tratamento.converterParaPresencial(id)
        .then(tratamento => {  
          return res.send({ tratamento });
        }).catch(error => res.status(400).send({ error }));
    }
);

const processarExtensaoAgendamentos = (tratamento, semanas) => {
  var agendamentosComparecidos = removerIdDeAgendamentosParaComparacao(
    filtrarAgendamentosComparecidos(tratamento.agendamentos)
  );

  var novosAgendamentos = criarAgendamentos(
    criarDatasNovosAgendamentos(semanas),
    tratamento.tipo
  );

  tratamento.agendamentos = mesclarAgendamentos(
    agendamentosComparecidos,
    novosAgendamentos
  );
};

const mesclarAgendamentos = (agendamentosComparecidos, novosAgendamentos) => {
  return _.unionWith(agendamentosComparecidos, novosAgendamentos, (a, b) => {
    return moment(a.data)
      .startOf("day")
      .utc()
      .isSame(b.data, "day");
  });
};

const criarDatasNovosAgendamentos = semanas => {
  var dataAgendamento = moment().startOf("day").utc();
  var datas = [dataAgendamento.toDate()];

  for (var semana = 1; semana < semanas; semana++) {
    dataAgendamento = dataAgendamento.add(7, "days");

    datas.push(dataAgendamento.toDate());
  }

  return datas;
};

const removerIdDeAgendamentosParaComparacao = agendamentos => {
  return _.map(agendamentos, agendamento => {
    return _.pick(agendamento, ["data", "compareceu", "processos"]);
  });
};

const filtrarAgendamentosComparecidos = agendamentos => {
  return _.filter(agendamentos, agendamento => {
    return agendamento.compareceu;
  });
};

const criarAgendamentos = (datas, tipos) => {
  return _.map(datas, data => {
    var compareceu = moment()
      .startOf("day")
      .utc()
      .isSame(data, "day");
    return { data, compareceu, processos: criarProcessos(tipos) };
  });
};

const criarProcessos = tipos => {
  return _.map(tipos, tipo => {
    return { tipo, concluido: false };
  });
};

module.exports = router;
