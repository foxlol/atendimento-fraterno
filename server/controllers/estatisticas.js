const _ = require('lodash');
const express = require('express');

const { Atendimento } = require('../models/atendimento');
const { Tratamento } = require('../models/tratamento');

const { autenticar, autorizar } = require('../middlewares/seguranca');

const router = express.Router();

router.get('/',
  [autenticar, autorizar('ADMIN,RECEPCAO,SECRETARIA,ORGANIZACAO,ATENDIMENTO,ENCAMINHAMENTO,SUPORTE,SUPORTE_ESCRIVAO')], async (req, res) => {

  try {
    const atendimentos = await Atendimento.obterContagem();
    const tratamentosEspirituais = await Tratamento.obterContagemVerificadosDoDia('espiritual');
    const tratamentosSaude = await Tratamento.obterContagemVerificadosDoDia('saude');
    const tratamentosDistancia = await Tratamento.obterContagemVerificadosDoDia('distancia');
    const tratamentosDistanciaSaude = await Tratamento.obterContagemVerificadosDoDia('distancia_saude');

    return res.send({
      estatisticas: {
        atendimentos,
        tratamentosEspirituais,
        tratamentosSaude,
        tratamentosDistancia,
        tratamentosDistanciaSaude
      }
    }); 
  } catch (error) {
    res.status(400).send({error});
  }
});

module.exports = router;
