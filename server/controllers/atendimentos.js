const _ = require('lodash');
const express = require('express');
const { ObjectID } = require('mongodb');

const { autenticar, autorizar } = require('../middlewares/seguranca');
const { Atendimento } = require('../models/atendimento');
const { Usuario } = require('../models/usuario');

const router = express.Router();

router.post('/',
  [autenticar, autorizar('ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO')], (req, res) => {

    const {
      idUsuario, idAtendente, isRetorno, atendentePreferencial
     } = req.body;

    if (!ObjectID.isValid(idUsuario)) {
      return res.status(400).send({error: 'ID de usuário inválido'});
    }

    if (idAtendente && !ObjectID.isValid(idUsuario)) {
      return res.status(400).send({error: 'ID de atendente inválido'});
    }

    return Usuario.findById(idUsuario).then((usuario) => {
      if (!usuario) {
        return res.status(404).send({error: 'Usuário não encontrado'});
      }

      const atendimento = new Atendimento({
        usuario: usuario._id,
        atendente: idAtendente,
        isRetorno,
        atendentePreferencial
      });

      return atendimento.save().then((atendimento) => {
        return res.send({atendimento});
      }, (error) => {
        return res.status(400).send({error: error.message});
      });
    }).catch((error) => res.status(400).send({error}));
});

router.patch('/:id/iniciar',
  [autenticar, autorizar('ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO')], (req, res) => {

    const id = req.params.id;
    const idAtendente = req.body.idAtendente;

    if (!ObjectID.isValid(id)) {
      return res.status(404).send({error: 'ID de atendimento inválido'})
    }

    if (!ObjectID.isValid(idAtendente)) {
      return res.status(400).send({error: 'ID de atendente inválido'});
    }

    return Atendimento.findByIdAndUpdate({
      _id: id,
    }, {
    'atendente': idAtendente
    }, {
      new: true,
      runValidators: true
    }).then((atendimento) => {
      return res.send({atendimento});
    }).catch((error) => res.status(400).send({error}));
});

router.get('/',
  [autenticar, autorizar('ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO')], (req, res) => {

    return Atendimento.obterTodos().then((atendimentos) => {
      return res.send({atendimentos});
    }).catch((error) => res.status(400).send({error}));
});

router.delete('/:id',
  [autenticar, autorizar('ADMIN,RECEPCAO,SECRETARIA,ATENDIMENTO')], (req, res) => {

    const id = req.params.id;

    if (!ObjectID.isValid(id)) {
      return res.status(404).send({error: 'ID de atendimento inválido'})
    }

    return Atendimento.findByIdAndRemove(id).then((atendimento) => {
      if (!atendimento) {
        return res.status(404).send({error: 'ID de atendimento não encontrado'});
      }

      return res.send({atendimento});
    }).catch((error) => res.status(400).send({error}));
});

module.exports = router;
