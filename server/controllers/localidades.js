const _ = require('lodash');
const express = require('express');

const { makeCaseAndAccentInsensitiveStartsWithRegExpPattern } = require('../helpers/string');

const router = express.Router();

router.get('/estados', (req, res) => {
  const localidades = getLocalidades();

  let estados = _.map(localidades.estados, (estado) => {
    return estado.sigla;
  });

  res.send({estados});
});

router.get('/estados/:estado/cidades', (req, res) => {
  const localidades = getLocalidades();
  const sigla = _.toUpper(req.params.estado);

  let estado = _.filter(localidades.estados, {sigla});
  let cidades = _.isEmpty(estado) ? [] : estado[0].cidades

  res.send({cidades});
});

router.get('/cidades', (req, res) => {
  const localidades = getLocalidades();
  const nome = _.trim(req.query.nome);

  let cidades = [];
  let expr = makeCaseAndAccentInsensitiveStartsWithRegExpPattern(nome);

  _.forEach(localidades.estados, (estado) => {
    _.forEach(estado.cidades, (cidade) => {
      if (cidade.match(expr)) {
        cidades.push({ estado: estado.sigla, nome: cidade});
      }
    });
  });

  res.send({cidades});
});

const getLocalidades = () => {
  try {
    return require('../data/localidades.json');
  } catch (e) {
    return [];
  }
};

module.exports = router;
