const socketIO = require('socket.io');
const socketIOJwtAuth = require('socketio-jwt-auth');

const { Usuario } = require('../models/usuario');

const tratamentoHandler = require('./tratamento-handler');
const estatisticasHandler = require('./estatisticas-handler');

module.exports = (server) => {

  const io = socketIO(server, {
    pingInterval: 10000,
    pingTimeout: 5000
  });

  io.use(socketIOJwtAuth.authenticate({
    secret: process.env.JWT_SECRET
  }, function(payload, done) {

    Usuario.findById(payload._id).then((usuario) => {
      if (!usuario) {
        return done(null, false, 'Usuario nao encontrado. Token invalido.');
      }
  
      return done(null, usuario);
    }).catch((err) => {
      return done(err);
    });
  }));

  io.of('/ws/tratamentos').on('connection', tratamentoHandler);
  io.of('/ws/estatisticas').on('connection', estatisticasHandler);

  return io;
};
