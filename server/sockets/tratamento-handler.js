const _ = require("lodash");
const moment = require("moment");

const { Tratamento } = require("../models/tratamento");
const { ObservacaoMediunica } = require("../models/observacao_mediunica");

const emitException = (socket, message) => {
  socket.emit("exception", { message });
};

const montarTratamento = (id, tipo, nomePaciente, endereco, idUsuario) => {
  return { id, tipo, nomePaciente, endereco, idUsuario };
};

const sincronizarObservacoesMediunicas = (socket, sala) => {
  ObservacaoMediunica.obterTodas(sala).then(
    observacoesMediunicasEmAndamento => {
      socket.emit(
        "sincronizarObservacoesMediunicas",
        observacoesMediunicasEmAndamento
      );

      socket.broadcast
        .to(sala)
        .emit(
          "sincronizarObservacoesMediunicas",
          observacoesMediunicasEmAndamento
        );
    }
  );
};

let tratamentosEmAndamento = {
  espiritual: [],
  saude: [],
  distancia: [],
  distancia_saude: []
};

let pacientesEmEspera = [];

module.exports = socket => {
  const sala = socket.request._query.sala;

  socket.join(sala);

  socket.emit("sincronizarPacientesEmEspera", pacientesEmEspera);
  socket.emit("sincronizarTratamentosEmAndamento", tratamentosEmAndamento);

  sincronizarObservacoesMediunicas(socket, sala);

  const isTratamentoEmAndamento =
    sala !== "saude" && tratamentosEmAndamento[sala].length > 0;

  if (isTratamentoEmAndamento) {
    socket.emit("carregarTratamento", tratamentosEmAndamento[sala][0]);
  }

  socket.on("iniciarTratamento", (id, idUsuario, callback) => {
    return Tratamento.findById(id)
      .populate("paciente")
      .then(tratamento => {
        if (!tratamento) {
          return emitException(socket, "Tratamento não encontrado");
        }

        if (sala !== "saude") {
          if (tratamentosEmAndamento[sala].length > 0) {
            return emitException(
              socket,
              "Já existe um tratamento em andamento"
            );
          }
        }

        const dadosTratamento = montarTratamento(
          tratamento._id,
          sala,
          tratamento.paciente.nome,
          tratamento.paciente.endereco,
          idUsuario
        );

        tratamentosEmAndamento[sala].push(dadosTratamento);

        socket.emit("carregarTratamento", dadosTratamento);
        socket.broadcast.to(sala).emit("carregarTratamento", dadosTratamento);

        socket.broadcast.emit(
          "sincronizarTratamentosEmAndamento",
          tratamentosEmAndamento
        );

        socket.emit(
          "sincronizarTratamentosEmAndamento",
          tratamentosEmAndamento
        );

        callback("OK");
      })
      .catch(error => emitException(socket, error.message));
  });

  socket.on("cancelarTratamento", (id, callback) => {
    return Tratamento.findById(id)
      .then(tratamento => {
        if (!tratamento) {
          return emitException(socket, "Tratamento não encontrado");
        }

        tratamentoCancelado = _.remove(
          tratamentosEmAndamento[sala],
          tratamentoEmAndamento => {
            return _.isEqual(tratamentoEmAndamento.id, tratamento._id);
          }
        );

        ObservacaoMediunica.removerTodas(sala);

        socket.broadcast
          .to(sala)
          .emit("tratamentoCancelado", tratamentoCancelado);

        socket.broadcast.emit(
          "sincronizarTratamentosEmAndamento",
          tratamentosEmAndamento
        );

        socket.emit(
          "sincronizarTratamentosEmAndamento",
          tratamentosEmAndamento
        );

        callback("OK");
      })
      .catch(error => emitException(socket, error.message));
  });

  socket.on("concluirTratamento", (dados, callback) => {
    if (!dados) {
      return emitException(socket, "Dados de tratamento inválidos");
    }

    if (!dados.tipo) {
      return emitException(socket, "Tipo de tratamento inválido");
    }

    return Tratamento.findById(dados.id)
      .then(tratamento => {
        if (!tratamento) {
          return emitException(socket, "Tratamento não encontrado");
        }

        if (!tratamento.observacoesMediunicas) {
          tratamento.observacoesMediunicas = [];
        }

        if (dados.tipo === "espiritual" || dados.tipo === "distancia") {
          const data = moment().format("DD/MM/YYYY");

          const observacaoMediunica = dados.observacaoMediunica
            ? dados.observacaoMediunica
            : "Sem comunicações";

          tratamento.observacoesMediunicas.push(
            `${data} - ${observacaoMediunica}`
          );
        }

        return tratamento.encerrar(dados.tipo).then(tratamento => {
          _.remove(tratamentosEmAndamento[sala], tratamentoEmAndamento => {
            return _.isEqual(tratamentoEmAndamento.id, tratamento._id);
          });

          return ObservacaoMediunica.removerTodas(sala).then(() => {
            socket.emit(
              "sincronizarTratamentosEmAndamento",
              tratamentosEmAndamento
            );

            socket.broadcast.emit(
              "sincronizarTratamentosEmAndamento",
              tratamentosEmAndamento
            );

            socket.emit("tratamentoConcluido", tratamento._id);

            socket.broadcast
              .to(sala)
              .emit("tratamentoConcluido", tratamento._id);

            callback("OK");
          });
        });
      })
      .catch(error => emitException(socket, error.message));
  });

  socket.on("togglePacienteEmEspera", (id, callback) => {
    const pacienteEmEspera = _.remove(pacientesEmEspera, pacienteEmEspera => {
      return _.isEqual(pacienteEmEspera, id);
    });

    if (_.isEmpty(pacienteEmEspera)) {
      pacientesEmEspera.push(id);
    }

    socket.emit("sincronizarPacientesEmEspera", pacientesEmEspera);
    socket.broadcast.emit("sincronizarPacientesEmEspera", pacientesEmEspera);
  });

  socket.on("removerObservacoesMediunicas", () => {
    ObservacaoMediunica.removerTodas(sala).then(() => {
      socket.emit("sincronizarObservacoesMediunicas", []);
      socket.broadcast.to(sala).emit("sincronizarObservacoesMediunicas", []);
    });
  });

  socket.on(
    "atualizarObservacaoMediunicaTratamento",
    observacaoMediunicaAtualizada => {
      ObservacaoMediunica.atualizar(
        observacaoMediunicaAtualizada._id,
        observacaoMediunicaAtualizada.conteudo,
        observacaoMediunicaAtualizada.medium
      ).then(() => {
        socket.broadcast
          .to(sala)
          .emit(
            "sincronizarObservacaoMediunica",
            observacaoMediunicaAtualizada
          );
      });
    }
  );

  socket.on("adicionarObservacaoMediunicaTratamento", atendente => {
    const obs = new ObservacaoMediunica({ atendente, sala }).save().then(() => {
      sincronizarObservacoesMediunicas(socket, sala);
    });
  });

  socket.on("removerObservacaoMediunicaTratamento", id => {
    ObservacaoMediunica.deleteOne({ _id: id }).then(() => {
      sincronizarObservacoesMediunicas(socket, sala);
    });
  });
};
