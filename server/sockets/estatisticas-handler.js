const { Tratamento } = require('../models/tratamento');
const { Atendimento } = require('../models/atendimento');

const obterTodasEstatisticas = async () => {
  try {
    const atendimentos = await Atendimento.obterContagem();
    const tratamentosEspirituais = await Tratamento.obterContagemVerificadosDoDia('espiritual');
    const tratamentosSaude = await Tratamento.obterContagemVerificadosDoDia('saude');
    const tratamentosDistancia = await Tratamento.obterContagemVerificadosDoDia('distancia');
    const tratamentosDistanciaSaude = await Tratamento.obterContagemVerificadosDoDia('distancia_saude');

    return {
      atendimentos,
      tratamentosEspirituais,
      tratamentosSaude,
      tratamentosDistancia,
      tratamentosDistanciaSaude
    }; 
  } catch (error) {
    throw new Error('Erro ao obter estatísticas');
  }
}

let estatisticas;

obterTodasEstatisticas().then(resultado => {
  estatisticas = resultado;
});

const obterEstatisticasAtendimento = async () => {
  try {
    return await Atendimento.obterContagem();
  } catch (error) {
    throw new Error('Erro ao obter estatísticas');
  }
}

const obterEstatisticasTratamentos = async () => {
  try {
    const tratamentosEspirituais = await Tratamento.obterContagemVerificadosDoDia('espiritual');
    const tratamentosSaude = await Tratamento.obterContagemVerificadosDoDia('saude');
    const tratamentosDistancia = await Tratamento.obterContagemVerificadosDoDia('distancia');
    const tratamentosDistanciaSaude = await Tratamento.obterContagemVerificadosDoDia('distancia_saude');

    return {
      tratamentosEspirituais,
      tratamentosSaude,
      tratamentosDistancia,
      tratamentosDistanciaSaude
    };  
  } catch (error) {
    throw new Error('Erro ao obter estatísticas');
  }
}

const emitException = (socket, message) => {
  socket.emit('exception', { message });
};

module.exports = async (socket) => {

  socket.emit('sincronizarEstatisticas', await obterTodasEstatisticas());

  socket.on('atualizarTodasEstatisticas', async () => {
    try {
      estatisticas = await obterTodasEstatisticas();
      socket.emit('sincronizarEstatisticas', estatisticas);
      socket.broadcast.emit('sincronizarEstatisticas', estatisticas); 
    } catch (error) {
      return emitException(socket, 'Erro ao atualizar estatísticas');
    }
  });

  socket.on('atualizarEstatisticasAtendimento', async () => {
    try {
      estatisticas.atendimentos = await obterEstatisticasAtendimento();
      socket.emit('sincronizarEstatisticas', estatisticas);
      socket.broadcast.emit('sincronizarEstatisticas', estatisticas); 
    } catch (error) {
      return emitException(socket, 'Erro ao atualizar estatísticas');
    }
  });

  socket.on('atualizarEstatisticasTratamentos', async () => {
    try {
      estatisticasTratamentos = await obterEstatisticasTratamentos();
      estatisticas = { ...estatisticas, ...estatisticasTratamentos };
      socket.emit('sincronizarEstatisticas', estatisticas);
      socket.broadcast.emit('sincronizarEstatisticas', estatisticas); 
    } catch (error) {
      return emitException(socket, 'Erro ao atualizar estatísticas');
    }
  });

};