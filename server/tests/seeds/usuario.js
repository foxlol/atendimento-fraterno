const _ = require('lodash');
const { ObjectID } = require('mongodb');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const { Usuario } = require('../../models/usuario');

const idUsuarioAdmin = new ObjectID();
const idUsuarioRecepcao = new ObjectID();
const idUsuarioSecretaria = new ObjectID();
const idUsuarioAtendimento = new ObjectID();
const idUsuarioOrganizacao = new ObjectID();
const idUsuarioEncaminhamento = new ObjectID();
const idUsuarioEscrivao = new ObjectID();
const idUsuarioPaciente = new ObjectID();
const idUsuarioDeficiente = new ObjectID();

const tokenUsuarioAdmin = jwt.sign({
  _id: idUsuarioAdmin.toHexString(), access: 'auth'
}, process.env.JWT_SECRET).toString();

const tokenUsuarioRecepcao = jwt.sign({
  _id: idUsuarioRecepcao.toHexString(), access: 'auth'
}, process.env.JWT_SECRET).toString();

const tokenUsuarioSecretaria = jwt.sign({
  _id: idUsuarioSecretaria.toHexString(), access: 'auth'
}, process.env.JWT_SECRET).toString();

const tokenUsuarioAtendimento = jwt.sign({
  _id: idUsuarioAtendimento.toHexString(), access: 'auth'
}, process.env.JWT_SECRET).toString();

const tokenUsuarioOrganizacao = jwt.sign({
  _id: idUsuarioOrganizacao.toHexString(), access: 'auth'
}, process.env.JWT_SECRET).toString();

const tokenUsuarioEncaminhamento = jwt.sign({
  _id: idUsuarioEncaminhamento.toHexString(), access: 'auth'
}, process.env.JWT_SECRET).toString();

const tokenUsuarioEscrivao = jwt.sign({
  _id: idUsuarioEscrivao.toHexString(), access: 'auth'
}, process.env.JWT_SECRET).toString();

const tokenUsuarioPaciente = jwt.sign({
  _id: idUsuarioPaciente.toHexString(), access: 'auth'
}, process.env.JWT_SECRET).toString();

const tokenUsuarioDeficiente = jwt.sign({
  _id: idUsuarioDeficiente.toHexString(), access: 'auth'
}, process.env.JWT_SECRET).toString();

const USUARIOS = [{
  _id: idUsuarioAdmin,
  username: 'admin',
  senha: '111111',
  nome: 'Diógenes Dassan Feijó',
  email: 'foxlol@gmail.com',
  dataNascimento: moment().subtract(31, 'years').toDate(),
  endereco: {
    logradouro: 'Rua Júlio de Almeida',
    numero: '86',
    bairro: 'Portal Buenos Aires',
    cidade: 'São José do Rio Pardo - SP'
  },
  roles: ['ADMIN'],
  tokens: [{
    access: 'auth',
    token: tokenUsuarioAdmin
  }]
}, {
  _id: idUsuarioRecepcao,
  username: 'recepcao',
  senha: '222222',
  nome: 'Recepção Souza',
  dataNascimento: moment().subtract(11, 'years').toDate(),
  roles: ['RECEPCAO'],
  tokens: [{
    access: 'auth',
    token: tokenUsuarioRecepcao
  }]
}, {
  _id: idUsuarioSecretaria,
  username: 'secretaria',
  senha: '333333',
  nome: 'Secretaria Martins',
  roles: ['SECRETARIA'],
  tokens: [{
    access: 'auth',
    token: tokenUsuarioSecretaria
  }]
}, {
  _id: idUsuarioAtendimento,
  username: 'atendimento',
  senha: '444444',
  nome: 'Atendimento Oliveira',
  roles: ['ATENDIMENTO'],
  tokens: [{
    access: 'auth',
    token: tokenUsuarioAtendimento
  }]
}, {
  _id: idUsuarioOrganizacao,
  username: 'organizacao',
  senha: '555555',
  nome: 'Organização Figueiredo',
  roles: ['ORGANIZACAO'],
  tokens: [{
    access: 'auth',
    token: tokenUsuarioOrganizacao
  }]
}, {
  _id: idUsuarioEncaminhamento,
  username: 'encaminhamento',
  senha: '666666',
  nome: 'Encaminhamento Boaro',
  dataNascimento: moment().subtract(60, 'years').toDate(),
  roles: ['ENCAMINHAMENTO'],
  tokens: [{
    access: 'auth',
    token: tokenUsuarioEncaminhamento
  }]
}, {
  _id: idUsuarioEscrivao,
  username: 'suporteescrivao',
  senha: '777777',
  nome: 'Escrivão Nogueira',
  roles: ['SUPORTE_ESCRIVAO'],
  tokens: [{
    access: 'auth',
    token: tokenUsuarioEscrivao
  }]
}, {
  _id: idUsuarioPaciente,
  username: 'paciente',
  senha: '888888',
  nome: 'Paciente Terminal',
  dataNascimento: moment().subtract(65, 'years').toDate(),
  roles: ['PACIENTE'],
  tokens: [{
    access: 'auth',
    token: tokenUsuarioPaciente
  }]
}, {
  _id: idUsuarioDeficiente,
  username: 'deficiente',
  senha: '999999',
  nome: 'Paciente Deficiente',
  dataNascimento: moment().subtract(6, 'years').toDate(),
  deficiente: true,
  roles: ['PACIENTE'],
  tokens: [{
    access: 'auth',
    token: tokenUsuarioDeficiente
  }]
}];

const popularUsuarios = (done) => {
  Usuario.remove({}).then(() => {
    const usuariosSalvos = [];

    _.forEach(USUARIOS, (usuario) => {
      usuariosSalvos.push(new Usuario(usuario).save());
    });

    return Promise.all(usuariosSalvos);
  }).then(() => done()).catch((error) => console.log(error));
};

module.exports = { USUARIOS, popularUsuarios };
