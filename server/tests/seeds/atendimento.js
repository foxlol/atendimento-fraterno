const _ = require('lodash');
const { ObjectID } = require('mongodb');

const { USUARIOS, popularUsuarios } = require('./usuario');
const { Usuario } = require('../../models/usuario');
const { Atendimento } = require('../../models/atendimento');

const ATENDIMENTOS = [{
  _id: new ObjectID(),
  usuario: USUARIOS[7]._id
},
{
  _id: new ObjectID(),
  usuario: USUARIOS[6]._id
},
{
  _id: new ObjectID(),
  usuario: USUARIOS[5]._id
}];

const popularAtendimentos = (done) => {
  Atendimento.remove({}).then(() => {
    const atendimentosSalvos = [];

    _.forEach(ATENDIMENTOS, (atendimento) => {
      atendimentosSalvos.push(new Atendimento(atendimento).save());
    });

    return Promise.all(atendimentosSalvos);
  }).then(() => done()).catch((error) => console.log(error));
};

module.exports = { USUARIOS, popularUsuarios, ATENDIMENTOS, popularAtendimentos };
