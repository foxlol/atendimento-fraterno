const _ = require('lodash');
const { ObjectID } = require('mongodb');
const moment = require('moment');

const { Tratamento } = require('../../models/tratamento');
const { Atendimento } = require('../../models/atendimento');
const { USUARIOS, popularUsuarios } = require('./usuario');

const TRATAMENTOS = [{
  _id: new ObjectID(),
  paciente: USUARIOS[0]._id,
  tipo: ['espiritual'],
  concluido: false,
  agendamentos: [{
    data: moment().add(21, 'days'),
    compareceu: false
  }, {
    data: moment().add(14, 'days'),
    compareceu: false
  }, {
    data: moment().add(7, 'days'),
    compareceu: false
  }, {
    data: moment(),
    compareceu: false,
    processos: [{
      tipo: 'espiritual',
      concluido: false
    }]
  }]
}, {
  _id: new ObjectID(),
  paciente: USUARIOS[7]._id,
  concluido: false,
  tipo: ['espiritual', 'distancia'],
  agendamentos: [{
    data: moment().subtract(21, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(14, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(7, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment(),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    },
    {
      tipo: 'espiritual',
      concluido: false
    }]
  }]
}, {
  _id: new ObjectID(),
  paciente: USUARIOS[6]._id,
  concluido: false,
  tipo: ['espiritual'],
  agendamentos: [{
    data: moment().subtract(21, 'days'),
    compareceu: true
  }, {
    data: moment().subtract(14, 'days'),
    compareceu: true
  }, {
    data: moment().subtract(7, 'days'),
    compareceu: true
  }, {
    data: moment(),
    compareceu: false
  }]
}, {
  _id: new ObjectID(),
  paciente: USUARIOS[5]._id,
  tipo: ['distancia'],
  agendamentos: [{
    data: moment().subtract(21, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(14, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(7, 'days'),
    compareceu: false,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment(),
    compareceu: false,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }]
}, {
  _id: new ObjectID(),
  paciente: USUARIOS[6]._id,
  tipo: ['distancia'],
  concluido: true,
  agendamentos: [{
    data: moment().subtract(21, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(14, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(7, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment(),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }]
}, {
  _id: new ObjectID(),
  paciente: USUARIOS[5]._id,
  tipo: ['distancia'],
  agendamentos: [{
    data: moment().subtract(21, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(14, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment(),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().add(7, 'days'),
    compareceu: false,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().add(14, 'days'),
    compareceu: false,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().add(21, 'days'),
    compareceu: false,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }]
}, {
  _id: new ObjectID(),
  paciente: USUARIOS[7]._id,
  concluido: false,
  tipo: ['saude'],
  agendamentos: [{
    data: moment().subtract(21, 'days'),
    compareceu: true
  }, {
    data: moment().subtract(14, 'days'),
    compareceu: true
  }, {
    data: moment().subtract(7, 'days'),
    compareceu: true
  }, {
    data: moment(),
    compareceu: false
  }]
}, {
  _id: new ObjectID(),
  paciente: USUARIOS[0]._id,
  concluido: false,
  tipo: ['distancia'],
  agendamentos: [{
    data: moment().subtract(21, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(14, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(7, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment(),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }]
}, {
  _id: new ObjectID(),
  paciente: USUARIOS[1]._id,
  concluido: false,
  tipo: ['distancia'],
  agendamentos: [{
    data: moment().subtract(21, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(14, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(7, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment(),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }]
}, {
  _id: new ObjectID(),
  paciente: USUARIOS[8]._id,
  concluido: false,
  tipo: ['distancia'],
  agendamentos: [{
    data: moment().subtract(21, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(14, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment().subtract(7, 'days'),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }, {
    data: moment(),
    compareceu: true,
    processos: [{
      tipo: 'distancia',
      concluido: false
    }]
  }]
}, {
  _id: new ObjectID(),
  paciente: USUARIOS[6]._id,
  concluido: false,
  tipo: ['espiritual'],
  agendamentos: [{
    data: moment().add(21, 'days'),
    compareceu: true
  }, {
    data: moment().add(14, 'days'),
    compareceu: true
  }, {
    data: moment().add(7, 'days'),
    compareceu: true
  }, {
    data: moment(),
    compareceu: true,
    processos: [{
      tipo: 'espiritual',
      concluido: false
    }]
  }]
}];

const ATENDIMENTOS = [{
  _id: new ObjectID(),
  usuario: USUARIOS[0]._id
}, {
  _id: new ObjectID(),
  usuario: USUARIOS[7]._id
}];

const popularTratamentos = (done) => {
  Tratamento.remove({}).then(() => {
    const tratamentosSalvos = [];

    _.forEach(TRATAMENTOS, (tratamento) => {
      tratamentosSalvos.push(new Tratamento(tratamento).save());
    });

    return Promise.all(tratamentosSalvos);
  }).then(() => {
    return Atendimento.remove({}).then(() => {
      const atendimentosSalvos = [];

      _.forEach(ATENDIMENTOS, (atendimento) => {
        atendimentosSalvos.push(new Atendimento(atendimento).save());
      });

      return Promise.all(atendimentosSalvos);
    });
  }).then(() => done()).catch((error) => console.log(error));
};

module.exports = { TRATAMENTOS, popularTratamentos, USUARIOS, popularUsuarios };
