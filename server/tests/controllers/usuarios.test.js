const expect = require('expect');
const request = require('supertest');
const { ObjectID } = require('mongodb');
const moment = require('moment');

const { app } = require('../../server');
const { Usuario } = require('../../models/usuario');
const { USUARIOS, popularUsuarios } = require('../seeds/usuario');

describe('USUÁRIO', () => {

  before(popularUsuarios);

  describe('Consulta - GET /usuarios', () => {
    it('deveria obter todos os usuários', (done) => {
      request(app)
        .get('/usuarios')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.usuarios.length).toBe(9);
        })
        .end(done);
    });

    it('deveria obter único usuário por nome', (done) => {
      const nome = 'diogenes';

      request(app)
        .get('/usuarios')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .query({nome})
        .expect(200)
        .expect((res) => {
          expect(res.body.usuarios.length).toBe(1);
        })
        .end(done);
    });

    it('deveria retornar 404 se usuários não forem encontrados', (done) => {
      request(app)
        .get('/usuarios')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .query({nome: 'nome inexistente'})
        .expect(404)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      request(app)
        .get('/usuarios')
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      request(app)
        .get('/usuarios')
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

  describe('Cadastro - POST /usuarios', () => {
    it('deveria cadastrar um usuário pelo nome', (done) => {
      const nome = 'teSte da SiLva ângelo';

      request(app)
        .post('/usuarios')
        .send({nome})
        .expect(200)
        .expect((res) => {
          expect(res.headers['x-auth']).toExist();
          expect(res.body.usuario._id).toExist();
          expect(res.body.usuario.nome).toBe('Teste Da Silva Ângelo');
          expect(res.body.usuario.username).toBe('testedasilvaangelo');
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Usuario.findById(res.body.usuario._id).then((usuario) => {
            expect(usuario.tokens[0]).toInclude({
              access: 'auth',
              token: res.headers['x-auth']
            });
            expect(usuario.nome).toBe('Teste Da Silva Ângelo');
            expect(usuario.username).toBe('testedasilvaangelo')
            expect(usuario.roles).toInclude('PACIENTE');
            expect(usuario.senha).toExist();
            expect(usuario.email).toNotExist();
            expect(usuario.endereco.logradouro).toNotExist();
            expect(usuario.endereco.cidade).toNotExist();
            expect(usuario.dataNascimento).toNotExist();
            done();
          }).catch((error) => done(error));
        });
    });

    it('deveria cadastrar um usuário com roles', (done) => {
      const nome = 'Master Admin';
      const roles = ['RECEPCAO', 'ATENDIMENTO'];

      request(app)
        .post('/usuarios')
        .send({nome, roles})
        .expect(200)
        .expect((res) => {
          expect(res.headers['x-auth']).toExist();
          expect(res.body.usuario._id).toExist();
          expect(res.body.usuario.nome).toBe(nome);
          expect(res.body.usuario.username).toBe('masteradmin');
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Usuario.findById(res.body.usuario._id).then((usuario) => {
            expect(usuario.tokens[0]).toInclude({
              access: 'auth',
              token: res.headers['x-auth']
            });
            expect(usuario.nome).toBe(nome);
            expect(usuario.username).toBe('masteradmin')
            expect(usuario.roles).toInclude('PACIENTE');
            expect(usuario.roles).toInclude('ATENDIMENTO');
            expect(usuario.roles).toInclude('RECEPCAO');
            expect(usuario.senha).toExist();
            expect(usuario.email).toNotExist();
            expect(usuario.endereco.logradouro).toNotExist();
            expect(usuario.endereco.cidade).toNotExist();
            expect(usuario.dataNascimento).toNotExist();
            done();
          }).catch((error) => done(error));
        });
    });

    it('deveria retornar 400 para cadastro sem nome', (done) => {
      request(app)
        .post('/usuarios')
        .expect(400)
        .end(done);
    });

    it('deveria retornar 400 para cadastro com nome invalido', (done) => {
      request(app)
        .post('/usuarios')
        .send({nome: 'aaa'})
        .expect(400)
        .end(done);
    });
  });

  describe('Atualização - PATCH /usuarios/:id', () => {
    it('deveria atualizar os dados do usuário', (done) => {
      const id = USUARIOS[1]._id;

      const payload = {
        nome: 'Sebastian Alfredo',
        endereco: {
          logradouro: 'Avenida Silas Malafaia',
          numero: '201',
          bairro: 'Jardim Santana',
          cidade: 'São José do Rio Preto - SP'
        },
        email: 'aaa@bbb.com',
        telefone: '19981371993',
        dataNascimento: moment().subtract(25, 'years').toDate(),
        deficiente: true,
        roles: ['ADMIN', 'ATENDIMENTO']
      };

      request(app)
        .patch(`/usuarios/${id}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .send(payload)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Usuario.findById(id).then((usuario) => {
            expect(usuario).toInclude(payload);
            done();
          }).catch((error) => done(error));
        });
    });

    it('deveria retornar 404 para usuário não encontrado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/usuarios/${id}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(404)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/usuarios/${id}`)
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/usuarios/${id}`)
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

  describe('Login - POST /usuarios/login', () => {
    it('deveria efetuar o login e retornar um auth token', (done) => {
      request(app)
        .post('/usuarios/login')
        .send({
          username: USUARIOS[0].username,
          senha: USUARIOS[0].senha
        })
        .expect(200)
        .expect((res) => {
          expect(res.headers['x-auth']).toExist();
          expect(res.body.usuario.username).toBe(USUARIOS[0].username);
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Usuario.findById(USUARIOS[0]._id).then((usuario) => {
            expect(usuario.tokens[1]).toInclude({
              access: 'auth',
              token: res.headers['x-auth']
            });
            done();
          }).catch((error) => done(error));
        });
    });

    it('deveria retornar 400 para login com credenciais invalidas', (done) => {
      request(app)
        .post('/usuarios/login')
        .send({
          username: USUARIOS[0].username,
          senha: 'errada'
        })
        .expect(400)
        .expect((res) => {
          expect(res.headers['x-auth']).toNotExist();
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Usuario.findById(USUARIOS[0]._id).then((usuario) => {
            expect(usuario.tokens.length).toBe(2);
            done();
          }).catch((error) => done(error));
        });
    });
  });

  describe('Logout - DELETE /usuarios/token', () => {
    it('deveria remover o token do usuario', (done) => {
      request(app)
        .delete('/usuarios/token')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.headers['x-auth']).toNotExist();
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Usuario.findById(USUARIOS[0]._id).then((usuario) => {
            expect(usuario.tokens.length).toBe(1);
            done();
          }).catch((error) => done(error));
        });
    });

    it('deveria retornar 401 ao remover token sem estar autenticado', (done) => {
      request(app)
        .delete('/usuarios/token')
        .expect(401)
        .end(done);
    });
  });

});
