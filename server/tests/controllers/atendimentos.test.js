const expect = require('expect');
const request = require('supertest');
const { ObjectID } = require('mongodb');

const { app } = require('../../server');
const { Atendimento } = require('../../models/atendimento');

const {
  USUARIOS, popularUsuarios,
  ATENDIMENTOS, popularAtendimentos
} = require('../seeds/atendimento');

describe('ATENDIMENTO', () => {

  before(popularUsuarios);
  before(popularAtendimentos);

  describe('Agendamento - POST /atendimentos', () => {

    it('deveria agendar um atendimento sem atendente', (done) => {
      request(app)
        .post('/atendimentos')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .send({idUsuario: USUARIOS[0]._id})
        .expect(200)
        .expect((res) => {
          expect(res.body.atendimento._id).toExist();
          expect(res.body.atendimento.usuario).toExist();
          expect(res.body.atendimento.atendente).toNotExist();
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          return Atendimento.findById(res.body.atendimento._id).then((atendimento) => {
            expect(atendimento.usuario).toEqual(USUARIOS[0]._id);
            expect(atendimento.atendente).toNotExist();
            done();
          }).catch((error) => done(error));
        });
    });

    it('deveria agendar um atendimento com atendente', (done) => {
      request(app)
        .post('/atendimentos')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .send({idUsuario: USUARIOS[1]._id, idAtendente: USUARIOS[3]._id})
        .expect(200)
        .expect((res) => {
          expect(res.body.atendimento._id).toExist();
          expect(res.body.atendimento.usuario).toExist();
          expect(res.body.atendimento.atendente).toExist();
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          return Atendimento.findById(res.body.atendimento._id).then((atendimento) => {
            expect(atendimento.usuario).toEqual(USUARIOS[1]._id);
            expect(atendimento.atendente).toEqual(USUARIOS[3]._id);
            done();
          }).catch((error) => done(error));
        });
    });

    it('deveria retornar 400 para atendimento com usuario sem permissão de atendimento', (done) => {
      request(app)
        .post('/atendimentos')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .send({idUsuario: USUARIOS[1]._id, idAtendente: USUARIOS[0]._id})
        .expect(400)
        .end(done);
    });

    it('deveria retornar 400 para agendamento de atendimento duplicado', (done) => {
      const idUsuario = USUARIOS[0]._id;
      request(app)
        .post('/atendimentos')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .send({idUsuario})
        .expect(400)
        .end(done);
    });

    it('deveria retornar 400 para ID inválido', (done) => {
      request(app)
        .post('/atendimentos')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .send({idUsuario: 'aaa'})
        .expect(400)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      request(app)
        .post('/atendimentos')
        .send({idUsuario: USUARIOS[0]._id})
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      request(app)
        .post('/atendimentos')
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .send({idUsuario: USUARIOS[0]._id})
        .expect(401)
        .end(done);
    });

    it('deveria retornar 404 para usuário não encontrado', (done) => {
      request(app)
        .post('/atendimentos')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .send({idUsuario: new ObjectID()})
        .expect(404)
        .end(done);
    });
  });

  describe('Consulta - GET /atendimentos', () => {

    before(popularAtendimentos);

    it('deveria retornar todos atendimentos agendados', (done) => {
      request(app)
        .get('/atendimentos')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.atendimentos.length).toBe(3);
          expect(res.body.atendimentos[0].usuario).toInclude({
            _id: USUARIOS[7]._id
          });
          expect(res.body.atendimentos[1].usuario).toInclude({
            _id: USUARIOS[6]._id
          });
          expect(res.body.atendimentos[2].usuario).toInclude({
            _id: USUARIOS[5]._id
          });
        })
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      request(app)
        .get('/atendimentos')
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      request(app)
        .get('/atendimentos')
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

  describe('Remoção - DELETE /atendimentos', () => {

    before(popularAtendimentos);

    it('deveria remover um atendimento', (done) => {
      const id = ATENDIMENTOS[0]._id.toHexString();
      request(app)
        .delete(`/atendimentos/${id}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.atendimento._id).toBe(id);
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          return Atendimento.findById(id).then((atendimento) => {
            expect(atendimento).toNotExist();
            return Atendimento.count().then((count) => {
              expect(count).toBe(2);
              done();
            });
          }).catch((error) => done(error));
        });
    });

    it('deveria retornar 404 para atendimento inexistente', (done) => {
      const id = new ObjectID().toHexString();
      request(app)
        .delete(`/atendimentos/${id}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(404)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      const id = ATENDIMENTOS[0]._id.toHexString();
      request(app)
        .delete(`/atendimentos/${id}`)
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      const id = ATENDIMENTOS[0]._id.toHexString();
      request(app)
        .delete(`/atendimentos/${id}`)
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

});
