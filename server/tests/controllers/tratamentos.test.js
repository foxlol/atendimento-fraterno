const expect = require('expect');
const request = require('supertest');
const moment = require('moment');
const { ObjectID } = require('mongodb');

const { app } = require('../../server');
const { Tratamento } = require('../../models/tratamento');
const { Atendimento } = require('../../models/atendimento');

const {
  USUARIOS, popularUsuarios,
  TRATAMENTOS, popularTratamentos
} = require('../seeds/tratamento');

describe('TRATAMENTO', () => {

  before(popularUsuarios);
  beforeEach(popularTratamentos);

  describe('Cadastro - POST /tratamentos', () => {
    it('deveria cadastrar um tratamento', (done) => {
      const usuario = USUARIOS[2];
      const motivo = 'Paciente se sente enraivecido';
      const tipo = ['saude', 'espiritual'];
      const datasAgendamentos = [
        moment().toDate(),
        moment().add(7, 'days').toDate(),
        moment().add(14, 'days').toDate(),
        moment().add(21, 'days').toDate()
      ];

      request(app)
        .post('/tratamentos')
        .send({
          idUsuario: usuario._id,
  	      tipo,
  	      datasAgendamentos,
          motivo
        })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamento.paciente).toEqual(usuario._id);
          expect(res.body.tratamento.concluido).toBe(false);
          expect(res.body.tratamento.motivo).toBe(motivo);
          expect(res.body.tratamento.agendamentos.length).toBe(4);
          expect(res.body.tratamento.agendamentos[0]).toInclude({
            data: moment().startOf('day').toISOString(),
            compareceu: true
          });
          expect(res.body.tratamento.agendamentos[1]).toInclude({
            data: moment().startOf('day').add(7, 'days').toISOString(),
            compareceu: false
          });
          expect(res.body.tratamento.agendamentos[2]).toInclude({
            data: moment().startOf('day').add(14, 'days').toISOString(),
            compareceu: false
          });
          expect(res.body.tratamento.agendamentos[3]).toInclude({
            data: moment().startOf('day').add(21, 'days').toISOString(),
            compareceu: false
          });
          expect(res.body.tratamento.tipo).toEqual(tipo);
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(res.body.tratamento._id).then((tratamento) => {
            expect(tratamento.paciente).toEqual(usuario._id);
            expect(tratamento.concluido).toBe(false);
            expect(tratamento.agendamentos.length).toBe(4);
            expect(tratamento.agendamentos[0]).toInclude({
              data: moment().startOf('day').toDate(),
              compareceu: true
            });
            expect(tratamento.agendamentos[1]).toInclude({
              data: moment().startOf('day').add(7, 'days').toDate(),
              compareceu: false
            });
            expect(tratamento.agendamentos[2]).toInclude({
              data: moment().startOf('day').add(14, 'days').toDate(),
              compareceu: false
            });
            expect(tratamento.agendamentos[3]).toInclude({
              data: moment().startOf('day').add(21, 'days').toDate(),
              compareceu: false
            });
            expect(tratamento.tipo).toEqual(tipo);
            done();
          }).catch((e) => done(e));
        });
    });

    it('deveria retornar 400 para ID de usuário inválido', (done) => {
      request(app)
        .post('/tratamentos')
        .send({idUsuario: 'aaa'})
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 400 para tipo de tratamento inválido', (done) => {
      request(app)
        .post('/tratamentos')
        .send({
          idUsuario: new ObjectID(),
          tipo: ['errado']
        })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 400 para datas de agendamentos inválidas', (done) => {
      request(app)
        .post('/tratamentos')
        .send({
          idUsuario: new ObjectID(),
          tipo: ['espiritual'],
          datasAgendamentos: []
        })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 403 se já houver tratamento em andamento', (done) => {
      request(app)
        .post('/tratamentos')
        .send({
          idUsuario: TRATAMENTOS[0].paciente,
          tipo: ['espiritual'],
          datasAgendamentos: [moment().toDate()]
        })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(403)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      request(app)
        .post('/tratamentos')
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      request(app)
        .post('/tratamentos')
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

  describe('Check-in - PATCH /tratamentos/:id/checkin', () => {
    it('deveria efetuar o checkin do paciente na data atual', (done) => {
      const id = TRATAMENTOS[0]._id;
      request(app)
        .patch(`/tratamentos/${id}/checkin`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.paciente).toEqual(USUARIOS[0]._id);
            expect(tratamento.agendamentos.length).toBe(4);
            expect(tratamento.agendamentos[0]).toInclude({
              compareceu: false
            });
            expect(tratamento.agendamentos[1]).toInclude({
              compareceu: false
            });
            expect(tratamento.agendamentos[2]).toInclude({
              compareceu: false
            });
            expect(tratamento.agendamentos[3]).toInclude({
              data: moment().startOf('day').toDate(),
              compareceu: true
            });

            done();
          }).catch((e) => done(e));
        });
    });

    it('deveria retornar 400 para checkin já realizado na data atual', (done) => {
      const id = TRATAMENTOS[1]._id;
      request(app)
        .patch(`/tratamentos/${id}/checkin`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.paciente).toEqual(USUARIOS[7]._id);
            expect(tratamento.agendamentos.length).toBe(4);
            expect(tratamento.agendamentos[0]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[1]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[2]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[3]).toInclude({
              data: moment().startOf('day').toDate(),
              compareceu: true
            });

            done();
          }).catch((e) => done(e));
        });
    });

    it('deveria efetuar o checkin e marcar atendimento de retorno', (done) => {
      const id = TRATAMENTOS[2]._id;
      request(app)
        .patch(`/tratamentos/${id}/checkin`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.paciente).toEqual(USUARIOS[6]._id);
            expect(tratamento.agendamentos.length).toBe(4);
            expect(tratamento.agendamentos[0]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[1]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[2]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[3]).toInclude({
              data: moment().startOf('day').toDate(),
              compareceu: true
            });

            return Atendimento.findOne({usuario: tratamento.paciente}).then((atendimento) => {
              expect(atendimento.usuario).toEqual(tratamento.paciente);
              done();
            })
          }).catch((e) => done(e));
        });
    });

    it('deveria negar o checkin retornando 403 devido a faltas no período', (done) => {
      const id = TRATAMENTOS[3]._id;
      request(app)
        .patch(`/tratamentos/${id}/checkin`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(403)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.paciente).toEqual(USUARIOS[5]._id);
            expect(tratamento.agendamentos.length).toBe(4);
            expect(tratamento.agendamentos[0]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[1]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[2]).toInclude({
              compareceu: false
            });
            expect(tratamento.agendamentos[3]).toInclude({
              data: moment().startOf('day').toDate(),
              compareceu: false
            });

            done();
          }).catch((e) => done(e));
        });
    });

    it('deveria retornar 403 para tratamento já concluído', (done) => {
      const id = TRATAMENTOS[4]._id;
      request(app)
        .patch(`/tratamentos/${id}/checkin`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(403)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.paciente).toEqual(USUARIOS[6]._id);
            expect(tratamento.agendamentos.length).toBe(4);
            expect(tratamento.concluido).toBe(true);
            expect(tratamento.agendamentos[0]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[1]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[2]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[3]).toInclude({
              compareceu: true
            });

            done();
          }).catch((e) => done(e));
        });
    });

    it('deveria retornar 400 para ID inválido', (done) => {
      const id = 'aaa';
      request(app)
        .patch(`/tratamentos/${id}/checkin`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 404 para ID não encontrado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/tratamentos/${id}/checkin`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(404)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/tratamentos/${id}/checkin`)
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/tratamentos/${id}/checkin`)
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

  describe('Check-out - PATCH /tratamentos/:id/checkout', () => {
    it('deveria efetuar o checkout do paciente na data atual', (done) => {
      const id = TRATAMENTOS[1]._id;
      request(app)
        .patch(`/tratamentos/${id}/checkout`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.paciente).toEqual(USUARIOS[7]._id);
            expect(tratamento.agendamentos.length).toBe(4);
            expect(tratamento.agendamentos[0]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[1]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[2]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[3]).toInclude({
              data: moment().startOf('day').toDate(),
              compareceu: false
            });

            Atendimento.count({paciente: tratamento.paciente}).then((count) => {
              expect(count).toBe(0);
              done();
            }).catch((e) => done(e));
          }).catch((e) => done(e));
        });
    });

    it('deveria retornar 404 para checkout já realizado na data atual', (done) => {
      const id = TRATAMENTOS[0]._id;
      request(app)
        .patch(`/tratamentos/${id}/checkout`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.paciente).toEqual(USUARIOS[0]._id);
            expect(tratamento.agendamentos.length).toBe(4);
            expect(tratamento.agendamentos[0]).toInclude({
              compareceu: false
            });
            expect(tratamento.agendamentos[1]).toInclude({
              compareceu: false
            });
            expect(tratamento.agendamentos[2]).toInclude({
              compareceu: false
            });
            expect(tratamento.agendamentos[3]).toInclude({
              data: moment().startOf('day').toDate(),
              compareceu: false
            });

            done();
          }).catch((e) => done(e));
        });
    });

    it('deveria retornar 403 para tratamento já concluído', (done) => {
      const id = TRATAMENTOS[4]._id;
      request(app)
        .patch(`/tratamentos/${id}/checkout`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(403)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.paciente).toEqual(USUARIOS[6]._id);
            expect(tratamento.agendamentos.length).toBe(4);
            expect(tratamento.concluido).toBe(true);
            expect(tratamento.agendamentos[0]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[1]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[2]).toInclude({
              compareceu: true
            });
            expect(tratamento.agendamentos[3]).toInclude({
              compareceu: true
            });

            done();
          }).catch((e) => done(e));
        });
    });

    it('deveria retornar 400 para ID inválido', (done) => {
      const id = 'aaa';
      request(app)
        .patch(`/tratamentos/${id}/checkout`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 404 para ID não encontrado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/tratamentos/${id}/checkout`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(404)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/tratamentos/${id}/checkout`)
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/tratamentos/${id}/checkout`)
        .set('x-auth', USUARIOS[5].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

  describe('Encerramento - PATCH /tratamentos/:id/encerrar', () => {
    it('deveria encerrar um tratamento', (done) => {
      const id = TRATAMENTOS[0]._id;
      request(app)
        .patch(`/tratamentos/${id}/encerrar`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamento.concluido).toBe(true);
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.concluido).toBe(true);

            Atendimento.count({usuario: tratamento.paciente}).then((count) => {
              expect(count).toBe(1);
              done();
            }).catch((error) => done(error));
          }).catch((error) => done(error));
        });
    });

    it('deveria retornar 404 para tratamento já encerrado', (done) => {
      const id = TRATAMENTOS[4]._id;
      request(app)
        .patch(`/tratamentos/${id}/encerrar`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(404)
        .end(done);
    });

    it('deveria retornar 400 para ID inválido', (done) => {
      const id = 'aaa';
      request(app)
        .patch(`/tratamentos/${id}/encerrar`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 404 para ID não encontrado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/tratamentos/${id}/encerrar`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(404)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/tratamentos/${id}/encerrar`)
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/tratamentos/${id}/encerrar`)
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

  describe('Extensão - PATCH /tratamentos/:id/estender/:semanas', () => {
    it('deveria estender o tratamento por 4 semanas', (done) => {
      const id = TRATAMENTOS[3]._id;
      const semanas = 4;
      request(app)
        .patch(`/tratamentos/${id}/estender/semanas/${semanas}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.concluido).toBe(false);
            expect(tratamento.agendamentos.length).toBe(6);
            expect(tratamento.agendamentos[0]).toInclude({
              data: moment().subtract(21, 'days').toDate(),
              compareceu: true
            });
            expect(tratamento.agendamentos[1]).toInclude({
              data: moment().subtract(14, 'days').toDate(),
              compareceu: true
            });
            expect(tratamento.agendamentos[2]).toInclude({
              data: moment().startOf('day').toDate(),
              compareceu: true
            });
            expect(tratamento.agendamentos[3]).toInclude({
              data: moment().add(7, 'days').toDate(),
              compareceu: false
            });
            expect(tratamento.agendamentos[4]).toInclude({
              data: moment().add(14, 'days').toDate(),
              compareceu: false
            });
            expect(tratamento.agendamentos[5]).toInclude({
              data: moment().add(21, 'days').toDate(),
              compareceu: false
            });
            done();
          }).catch((error) => done(error));
        });
    });

    it('deveria manter o tratamento estendido como está', (done) => {
      const id = TRATAMENTOS[5]._id;
      const semanas = 4;
      request(app)
        .patch(`/tratamentos/${id}/estender/semanas/${semanas}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.concluido).toBe(false);
            expect(tratamento.agendamentos.length).toBe(6);
            expect(tratamento.agendamentos[0]).toInclude({
              data: moment().subtract(21, 'days').toDate(),
              compareceu: true
            });
            expect(tratamento.agendamentos[1]).toInclude({
              data: moment().subtract(14, 'days').toDate(),
              compareceu: true
            });
            expect(tratamento.agendamentos[2]).toInclude({
              data: moment().startOf('day').toDate(),
              compareceu: true
            });
            expect(tratamento.agendamentos[3]).toInclude({
              data: moment().add(7, 'days').toDate(),
              compareceu: false
            });
            expect(tratamento.agendamentos[4]).toInclude({
              data: moment().add(14, 'days').toDate(),
              compareceu: false
            });
            expect(tratamento.agendamentos[5]).toInclude({
              data: moment().add(21, 'days').toDate(),
              compareceu: false
            });
            done();
          }).catch((error) => done(error));
        });
    });

    it('deveria retornar 400 para ID inválido', (done) => {
      const id = 'aaa';
      const semanas = 4;
      request(app)
        .patch(`/tratamentos/${id}/estender/semanas/${semanas}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 404 para ID não encontrado', (done) => {
      const id = new ObjectID();
      const semanas = 4;
      request(app)
        .patch(`/tratamentos/${id}/estender/semanas/${semanas}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(404)
        .end(done);
    });

    it('deveria retornar 400 para número de semanas inválido (NaN)', (done) => {
      const id = new ObjectID();
      const semanas = 'a';
      request(app)
        .patch(`/tratamentos/${id}/estender/semanas/${semanas}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 400 para número de semanas inválido (negativo)', (done) => {
      const id = new ObjectID();
      const semanas = -1;
      request(app)
        .patch(`/tratamentos/${id}/estender/semanas/${semanas}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 400 para número de semanas inválido (zero)', (done) => {
      const id = new ObjectID();
      const semanas = 0;
      request(app)
        .patch(`/tratamentos/${id}/estender/semanas/${semanas}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      const id = new ObjectID();
      const semanas = 4;
      request(app)
        .patch(`/tratamentos/${id}/estender/semanas/${semanas}`)
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      const id = new ObjectID();
      const semanas = 4;
      request(app)
        .patch(`/tratamentos/${id}/estender/semanas/${semanas}`)
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

  describe('Consulta de Tratamentos Verificados do Dia - GET /tratamentos/:tipo/verificados', () => {

    it('deveria retornar os tratamentos verificados do dia', (done) => {
      const tipo = 'distancia';
      request(app)
        .get(`/tratamentos/${tipo}/verificados`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(6);
        })
        .end(done);
    });

    it('deveria retornar vazio quando não há tratamentos', (done) => {
      const tipo = 'saude';
      request(app)
        .get(`/tratamentos/${tipo}/verificados`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(0);
        })
        .end(done);
    });

    it('deveria retornar 400 para tipo inválido', (done) => {
      const tipo = 'medico';
      request(app)
        .get(`/tratamentos/${tipo}/verificados`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      const tipo = 'espiritual';
      request(app)
        .get(`/tratamentos/${tipo}/verificados`)
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      const tipo = 'espiritual';
      request(app)
        .get(`/tratamentos/${tipo}/verificados`)
        .set('x-auth', USUARIOS[7].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

  describe('Consulta de Tratamentos por Paciente - GET /tratamentos/paciente/:id', () => {
    it('deveria retornar tratamentos do paciente', (done) => {
      const id = USUARIOS[6]._id;
      request(app)
        .get(`/tratamentos/paciente/${id}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(3);
          expect(res.body.tratamentos[0]).toInclude({paciente: id});
          expect(res.body.tratamentos[1]).toInclude({paciente: id});
          expect(res.body.tratamentos[2]).toInclude({paciente: id});
        })
        .end(done);
    });

    it('deveria retornar 404 quando não há tratamentos', (done) => {
      const id = new ObjectID();
      request(app)
        .get(`/tratamentos/paciente/${id}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(404)
        .end(done);
    });

    it('deveria retornar 400 para tipo inválido', (done) => {
      const id = 'aaa';
      request(app)
        .get(`/tratamentos/paciente/${id}`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      const id = new ObjectID();
      request(app)
        .get(`/tratamentos/paciente/${id}`)
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      const id = new ObjectID();
      request(app)
        .get(`/tratamentos/paciente/${id}`)
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

  describe('Cadastro de Observação Mediúnica - PATCH /tratamentos/:id/observacao-mediunica', () => {
    it('deveria cadastrar uma observação mediúnica e encerrar o tratamento', (done) => {
      const id = TRATAMENTOS[1]._id;
      const observacaoMediunica = 'ceh loko';
      request(app)
        .patch(`/tratamentos/${id}/observacao-mediunica`)
        .send({observacaoMediunica})
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.concluido).toBe(false);
            expect(tratamento.observacoesMediunicas.length).toBe(1);
            expect(tratamento.observacoesMediunicas[0]).toBe(
              `${moment().format('DD/MM/YYYY')} - ${observacaoMediunica}`
            );
            done();
          }).catch((error) => done(error));
        });
    });

    it('deveria cadastrar uma observação mediúnica e não encerrar o tratamento', (done) => {
      const id = TRATAMENTOS[10]._id;
      const observacaoMediunica = 'ceh loko 2';
      request(app)
        .patch(`/tratamentos/${id}/observacao-mediunica`)
        .send({observacaoMediunica})
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Tratamento.findById(id).then((tratamento) => {
            expect(tratamento.concluido).toBe(false);
            expect(tratamento.observacoesMediunicas.length).toBe(1);
            expect(tratamento.observacoesMediunicas[0]).toBe(
              `${moment().format('DD/MM/YYYY')} - ${observacaoMediunica}`
            );
            done();
          }).catch((error) => done(error));
        });
    });

    it('deveria retornar 404 quando não há o tratamento', (done) => {
      const id = new ObjectID();
      const observacaoMediunica = 'ceh loko';
      request(app)
        .patch(`/tratamentos/${id}/observacao-mediunica`)
        .send({observacaoMediunica})
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(404)
        .end(done);
    });

    it('deveria retornar 400 para id inválido', (done) => {
      const id = 'aaa';
      request(app)
        .patch(`/tratamentos/${id}/observacao-mediunica`)
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 400 para observação mediúnica inválida', (done) => {
      const id = TRATAMENTOS[0]._id;
      const observacaoMediunica = ' ';
      request(app)
        .patch(`/tratamentos/${id}/observacao-mediunica`)
        .send({observacaoMediunica})
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(400)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/tratamentos/${id}/observacao-mediunica`)
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      const id = new ObjectID();
      request(app)
        .patch(`/tratamentos/${id}/observacao-mediunica`)
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

  describe('Obter Todos Tratamentos - GET /tratamentos/todos', () => {
    it('deveria obter todos tratamentos', (done) => {
      request(app)
        .post('/tratamentos/todos')
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(11);
        })
        .end(done);
    });

    it('deveria obter todos tratamentos espirituais', (done) => {
      request(app)
        .post('/tratamentos/todos')
        .send({ tipo: ['espiritual'] })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(4);
        })
        .end(done);
    });

    it('deveria obter todos tratamentos de saúde', (done) => {
      request(app)
        .post('/tratamentos/todos')
        .send({ tipo: ['saude'] })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(1);
        })
        .end(done);
    });

    it('deveria obter todos tratamentos a distancia', (done) => {
      request(app)
        .post('/tratamentos/todos')
        .send({ tipo: ['distancia'] })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(7);
        })
        .end(done);
    });

    it('deveria obter todos tratamentos espirituais e distancia', (done) => {
      request(app)
        .post('/tratamentos/todos')
        .send({ tipo: ['espiritual', 'distancia'] })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(10);
        })
        .end(done);
    });

    it('deveria obter todos tratamentos de um paciente', (done) => {
      request(app)
        .post('/tratamentos/todos')
        .send({ paciente: USUARIOS[0]._id })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(2);
        })
        .end(done);
    });

    it('deveria obter todos tratamentos espirituais de um paciente', (done) => {
      request(app)
        .post('/tratamentos/todos')
        .send({ tipo: ['espiritual'], paciente: USUARIOS[0]._id })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(1);
        })
        .end(done);
    });

    it('deveria obter todos tratamentos concluidos', (done) => {
      request(app)
        .post('/tratamentos/todos')
        .send({ concluido: true })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(1);
        })
        .end(done);
    });

    it('deveria obter todos tratamentos em andamento', (done) => {
      request(app)
        .post('/tratamentos/todos')
        .send({ concluido: false })
        .set('x-auth', USUARIOS[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body.tratamentos.length).toBe(10);
        })
        .end(done);
    });

    it('deveria retornar 401 para acesso não autenticado', (done) => {
      const id = new ObjectID();
      request(app)
        .post('/tratamentos/todos')
        .expect(401)
        .end(done);
    });

    it('deveria retornar 401 para acesso não autorizado', (done) => {
      const id = new ObjectID();
      request(app)
        .post('/tratamentos/todos')
        .set('x-auth', USUARIOS[4].tokens[0].token)
        .expect(401)
        .end(done);
    });
  });

});
