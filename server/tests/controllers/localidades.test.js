const expect = require('expect');
const request = require('supertest');

const { app } = require('../../server');

describe('LOCALIDADE', () => {
  describe('Estados - GET /localidades/estados', () => {
    it('deveria obter todos os estados', (done) => {
      request(app)
        .get('/localidades/estados')
        .expect(200)
        .expect((res) => {
          expect(res.body.estados.length).toBe(27);
          expect(res.body.estados).toInclude('SP', 'MG');
        })
        .end(done);
    });
  });

  describe('Cidades por Estado - GET /localidades/estados/:estado/cidades', () => {
    it('deveria obter todas as cidades de um estado', (done) => {
      const estado = 'SP';
      request(app)
        .get(`/localidades/estados/${estado}/cidades`)
        .expect(200)
        .expect((res) => {
          expect(res.body.cidades.length).toBe(645);
          expect(res.body.cidades).toInclude('São José do Rio Pardo');
        })
        .end(done);
    });
  });

  describe('Cidades por Nome - GET /localidades/cidades?nome=:nome', () => {
    it('deveria obter as cidades pelo nome', (done) => {
      const nome = 'sao jose do rio';
      request(app)
        .get(`/localidades/cidades?nome=${nome}`)
        .expect(200)
        .expect((res) => {
          expect(res.body.cidades.length).toBe(3);
          expect(res.body.cidades).toInclude({
            estado: 'SP',
            nome: 'São José do Rio Pardo'
          });
        })
        .end(done);
    });
  });
});
