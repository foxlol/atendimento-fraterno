const io = require('socket.io-client');
const expect = require('expect');
const { ObjectID } = require('mongodb');

const {
  USUARIOS, popularUsuarios,
  TRATAMENTOS, popularTratamentos
} = require('../seeds/tratamento');

const socketURL = `ws://localhost:${process.env.PORT}/ws/tratamentos`;

describe("PROCESSOS DE TRATAMENTO", () => {

  before(popularUsuarios);
  beforeEach(popularTratamentos);

  it('deveria iniciar, carregar e concluir um tratamento', (done) => {
    const sala = 'espiritual';
    const idTratamento = TRATAMENTOS[0]._id;
    const observacaoMediunica = 'ave maria!!';

    const clientEncaminhamento = io.connect(socketURL, {query: { sala }});
    const clientEscrivao = io.connect(socketURL, {query: { sala }});

    clientEncaminhamento.emit('iniciarTratamento', idTratamento, (status) => {
      expect(status).toBe('OK');
    });

    clientEncaminhamento.on('exception', (exception) => {
      done(new Error(exception.message));
    });

    clientEncaminhamento.on('connect_error', () => {
      done(new Error('Erro ao se conectar ao servidor'));
    });

    clientEncaminhamento.on('tratamentoConcluido', (id) => {
      expect(id).toEqual(idTratamento);
      clientEncaminhamento.disconnect();
      clientEscrivao.disconnect();
      done();
    });

    clientEscrivao.on('carregarTratamento', (tratamento) => {
      expect(tratamento).toExist();
      expect(tratamento.id).toEqual(idTratamento);
      expect(tratamento.tipo).toEqual(sala);

      clientEscrivao.emit('concluirTratamento', {
        id: idTratamento,
        tipo: 'espiritual',
        observacaoMediunica: 'ave maria!!'
      }, (status) => {
        expect(status).toBe('OK');
      });
    });

    clientEscrivao.on('exception', (exception) => {
      done(new Error(exception.message));
    });

    clientEscrivao.on('connect_error', () => {
      done(new Error('Erro ao se conectar ao servidor'));
    });
  });

  it('deveria cancelar um tratamento iniciado', (done) => {
    const sala = 'espiritual';
    const idTratamento = TRATAMENTOS[0]._id;

    const clientEncaminhamento = io.connect(socketURL, {query: { sala }});

    clientEncaminhamento.emit('iniciarTratamento', idTratamento, (status) => {
      expect(status).toBe('OK');

      clientEncaminhamento.emit('cancelarTratamento', idTratamento, (status) => {
        expect(status).toBe('OK');
        done();
      });
    });

    clientEncaminhamento.on('exception', (exception) => {
      done(new Error(exception.message));
    });

    clientEncaminhamento.on('connect_error', () => {
      done(new Error('Erro ao se conectar ao servidor'));
    });
  });
});
