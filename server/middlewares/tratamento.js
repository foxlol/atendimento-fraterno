const _ = require('lodash');
var moment = require('moment');
const { ObjectID } = require('mongodb');

const { Tratamento } = require('../models/tratamento');

const validarCheckin = (req, res, next) => {
  const id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(400).send({error: 'ID de tratamento inválido'});
  }

  Tratamento.findById(id).then((tratamento) => {
    if (!tratamento) {
      return res.status(404).send({error: 'Tratamento não encontrado'});
    }

    if (tratamento.concluido) {
      return res.status(403).send({error: 'Tratamento já concluído'});
    }

    if (verificarExistenciaFaltas(tratamento)) {
      return res.status(403).send({
        error: 'Houve falta no período de tratamento',
        requireAction: true,
        agendamentosNecessarios: calcularAgendamentosNecessarios(tratamento)
      });
    }

    next();
  }).catch((error) => res.status(400).send({error}));
};

const validarCheckout = (req, res, next) => {
  const id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(400).send({error: 'ID de tratamento inválido'});
  }

  Tratamento.findById(id).then((tratamento) => {
    if (!tratamento) {
      return res.status(404).send({error: 'Tratamento não encontrado'});
    }

    if (tratamento.concluido) {
      return res.status(403).send({error: 'Tratamento já concluído'});
    }

    next();
  }).catch((error) => res.status(400).send({error}));
};

const validarCadastro = (req, res, next) => {
  const body = _.pick(
    req.body, [
      'idUsuario', 'tipo', 'datasAgendamentos',
      'idAtendente', 'motivo', 'intercessor'
    ]
  );

  if (!ObjectID.isValid(body.idUsuario)) {
    return res.status(400).send({error: 'ID de usuário inválido'});
  }

  if (!_.isEmpty(body.idAtendente) && !ObjectID.isValid(body.idAtendente)) {
    return res.status(400).send({error: 'ID do usuário atendente inválido'});
  }

  if (_.isEmpty(body.tipo)) {
    return res.status(400).send({error: 'Tipo de tratamento inválido'});
  }

  if (_.isEmpty(body.datasAgendamentos)) {
    return res.status(400).send({error: 'Datas para tratamento inválidas'});
  }

  Tratamento.obterAtual(body.idUsuario).then((tratamento) => {
      if (tratamento) {
          return res.status(403).send(
            {error: 'Já existe um tratamento em andamento'});
      }

      req.tratamento = {
        paciente: body.idUsuario,
        tipo: body.tipo,
        datasAgendamentos: body.datasAgendamentos,
        atendente: body.idAtendente,
        motivo: body.motivo,
        intercessor: body.intercessor
      };

      next();
  }).catch((error) => res.status(400).send({error}));
};

const verificarExistenciaFaltas = (tratamento) => {
  const agendamentosNaoComparecidos =
    _.filter(tratamento.agendamentos, (agendamento) => {
      return moment(agendamento.data).startOf("day").utc().isSameOrBefore(moment().startOf("day").utc(), 'day')
              && !agendamento.compareceu;
    });

  if (!agendamentosNaoComparecidos) {
    return false;
  }

  return agendamentosNaoComparecidos.length > 1;
};

const calcularAgendamentosNecessarios = (tratamento) => {
  const agendamentosNaoComparecidos =
    _.filter(tratamento.agendamentos, (agendamento) => {
      return moment(agendamento.data).startOf("day").utc().isSameOrBefore(moment().startOf("day").utc(), 'day')
              && !agendamento.compareceu;
    });

  const naoComparecidos =
    agendamentosNaoComparecidos ? agendamentosNaoComparecidos.length : 0;

  const agendamentosRestantes =
    _.filter(tratamento.agendamentos, (agendamento) => {
      return moment(agendamento.data).startOf("day").utc().isAfter(moment().startOf("day").utc(), 'day')
              && !agendamento.compareceu;
    });

  const restantes =
    agendamentosRestantes ? agendamentosRestantes.length : 0;

  return naoComparecidos + restantes;
};

module.exports = {
  validarCheckin, validarCheckout, validarCadastro
};
