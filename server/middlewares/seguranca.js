const _ = require('lodash');

const { Usuario } = require('../models/usuario');

const autenticar = (req, res, next) => {
  const token = req.header('x-auth');

  Usuario.findByToken(token).then((usuario) => {
    if (!usuario) {
      return Promise.reject();
    }

    req.usuario = usuario;
    req.token = token;

    next();
  }).catch((e) => {
    res.status(401).send();
  });
};

const autorizar = (roles) => {
  return (req, res, next) => {
    if (_.intersection(req.usuario.roles, _.split(roles, ',')).length > 0) {
      next();
    } else {
      res.status(401).send();
    }
  };
};


module.exports = { autenticar, autorizar };
