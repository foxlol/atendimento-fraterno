require('./configurations/app');
require('./configurations/mongoose');

const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const cors = require('cors');
const compression = require('compression');

const controllers = require('./controllers/index');
const sockets = require('./sockets/index');

const app = express();

app.use(compression());

app.options('*', cors({
  exposedHeaders: ['x-auth']
}));

app.use(cors({
  exposedHeaders: ['x-auth']
}));

app.use(bodyParser.json());
app.use(controllers);

const server = http.createServer(app);
const io = sockets(server);

server.listen(process.env.PORT, () => {
  console.log(`Server started on port ${process.env.PORT}`);
});

module.exports = { app };
