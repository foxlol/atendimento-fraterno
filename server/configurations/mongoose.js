const mongoose = require('mongoose');

const retryInterval = 15000;

const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  autoReconnect: true,
  keepAlive: 1,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 5000,
  connectTimeoutMS: 5000,
  ha: true,
  haInterval: 5000
}

const connect = () => {
  mongoose.connect(process.env.MONGODB_URI, options).then(() => {
    console.log('Connected to database');
  }).catch((error) => console.log(error.message));
};

mongoose.Promise =  global.Promise;

const connection = mongoose.connection;

connection.on('error', function (err) {
  if (err) {
    console.log(`Database disconnected: Retrying connection in ${retryInterval / 1000} seconds`);

    if (connection.db) {
      connection.db.close();
    }

    setTimeout(connect, retryInterval);
  }
});

connect();

module.exports = { mongoose };
