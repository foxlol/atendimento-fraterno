const env = process.env.NODE_ENV || 'development';

console.log('Server mode:', env);

if (env === 'development' || env === 'test' || env === 'production') {
  const config = require('./app.json');
  const envConfig = config[env];

  Object.keys(envConfig).forEach((key) => {
    process.env[key] = envConfig[key];
  });
}
